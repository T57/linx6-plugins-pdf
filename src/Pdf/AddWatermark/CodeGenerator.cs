﻿using System.Security.Cryptography.X509Certificates;
using iText.IO.Source;
using iText.Kernel.Pdf;
using Twenty57.Linx.Interfaces.Plugin.FunctionProvider;
using Twenty57.Linx.Plugins.Pdf.Runtime.AddWatermark;
using Twenty57.Linx.Plugins.Pdf.Shared.AddWatermark;

namespace Twenty57.Linx.Plugins.Pdf.AddWatermark;

internal class CodeGenerator : IFunctionCodeGenerator
{
    public void GenerateCode(IFunctionBuilder functionBuilder)
    {
        var template = new MethodBody(functionBuilder);
        functionBuilder.AddCode(template.TransformText());
        functionBuilder.AddAssemblyReference(typeof(PdfReader));
        functionBuilder.AddAssemblyReference(typeof(OutputStream<>));
        functionBuilder.AddAssemblyReference(typeof(X509Certificate2));
        functionBuilder.AddAssemblyReference(typeof(Executor));
        functionBuilder.AddAssemblyReference(typeof(WatermarkPosition));
    }
}
