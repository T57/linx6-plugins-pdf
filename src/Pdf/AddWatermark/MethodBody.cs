﻿using Twenty57.Linx.Interfaces.Plugin.FunctionProvider;
using Twenty57.Linx.Plugins.Pdf.Common.CodeGeneration;
using Twenty57.Linx.Plugins.Pdf.Runtime;
using Twenty57.Linx.Plugins.Pdf.Shared.AddWatermark;
using Twenty57.Linx.Plugins.Pdf.Shared.AddWatermark.Constants;
using Twenty57.Linx.Sdk.Core.LinxProperties;

namespace Twenty57.Linx.Plugins.Pdf.AddWatermark;

internal partial class MethodBody : CustomT4RuntimeBase
{
    public MethodBody(IFunctionBuilder functionBuilder)
        : base(functionBuilder)
    {
        var functionData = functionBuilder.Data;

        InputPdfFileInfo = new PdfFileInfo(Shared.Common.Constants.CategoryNames.Input, functionBuilder);
        WatermarkPdfFileInfo = new PdfFileInfo(CategoryNames.Watermark, functionBuilder);

        WatermarkPosition = functionData.Properties[PropertyIds.Position].GetValue<WatermarkPosition>();
        WatermarkPagesParameterName = functionBuilder.GetParamName(PropertyIds.Pages);

        OutputFilePathParameterName = functionBuilder.GetParamName(Shared.Common.Constants.PropertyIds.OutputFilePath);
        ContextParameterName = functionBuilder.ContextParamName;
        ExecutorName = typeof(Pdf.Runtime.AddWatermark.Executor).FullName;
    }

    public PdfFileInfo InputPdfFileInfo { get; }
    public PdfFileInfo WatermarkPdfFileInfo { get; }

    public WatermarkPosition WatermarkPosition { get; }
    public string WatermarkPagesParameterName { get; }

    public string OutputFilePathParameterName { get; }
    public string ContextParameterName { get; }
    public string ExecutorName { get; }
}
