﻿using System.Text.RegularExpressions;
using Twenty57.Linx.Interfaces.Plugin.Types;
using Twenty57.Linx.Plugins.Pdf.Shared.AddWatermark;
using Twenty57.Linx.Plugins.Pdf.Shared.ChangeProtection;
using Twenty57.Linx.Plugins.Pdf.Shared.Common;
using Twenty57.Linx.Plugins.Pdf.Shared.Obsolete;
using Twenty57.Linx.Plugins.Pdf.Shared.Read;
using Twenty57.Linx.Plugins.Pdf.Shared.Sign;

namespace Twenty57.Linx.Plugins.Pdf;

public class TypeResolver : ITypeResolver
{
    private static readonly Regex PluginTypeRegex;

    static TypeResolver()
    {
        string namespaceRegex = Regex.Escape(typeof(TypeResolver).Namespace);
        PluginTypeRegex = new Regex($@"^{namespaceRegex}\.([\w\.]+), {namespaceRegex}(\.\w+)?, Version=\d+.\d+.\d+.\d+", RegexOptions.Compiled);
    }

    public bool TryGetResolvedTypeName(string typeName, out string resolvedTypeName)
    {
        var typeNameMatch = PluginTypeRegex.Match(typeName);
        if (!typeNameMatch.Success)
        {
            resolvedTypeName = null;
            return false;
        }

        Type resolvedType = typeNameMatch.Groups[1].Value switch
        {
            "AddWatermark.WatermarkPosition" => typeof(WatermarkPosition),
            "AuthenticationType" => typeof(AuthenticationType),
            "CertificateSource" => typeof(CertificateSource),
            "ChangeProtection.Changes" => typeof(Changes),
            "ChangeProtection.Encryption" => typeof(Encryption),
            "ChangeProtection.Printing" => typeof(Printing),
            "Common.PropertyMapping" => typeof(PropertyMapping),
            "Obsolete.CertificateSource" => typeof(CertificateSource),
            "Obsolete.StoredCertificate" => typeof(StoredCertificate),
            "Read.FormDataSpecificationType" => typeof(FormDataSpecificationType),
            "Read.SignatureInfo" => typeof(SignatureInfo),
            "Read.TextExtractionStrategy" => typeof(TextExtractionStrategy),
            "Read.TextSplit" => typeof(TextSplit),
            "Shared.AuthenticationType" => typeof(AuthenticationType),
            "Shared.SignatureInfo" => typeof(SignatureInfo),
            "Shared.SignaturePlacement" => typeof(SignaturePlacement),
            "Shared.TextExtractionStrategy" => typeof(TextExtractionStrategy),
            "Shared.WatermarkPosition" => typeof(WatermarkPosition),
            "Sign.SignaturePlacement" => typeof(SignaturePlacement),
            "StoredCertificate" => typeof(StoredCertificate),
            _ => null
        };

        resolvedTypeName = resolvedType?.AssemblyQualifiedName;
        return resolvedType != null;
    }
}
