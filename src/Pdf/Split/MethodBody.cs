﻿using Twenty57.Linx.Interfaces.Plugin.FunctionProvider;
using Twenty57.Linx.Plugins.Pdf.Common.CodeGeneration;
using Twenty57.Linx.Plugins.Pdf.Runtime;
using Twenty57.Linx.Plugins.Pdf.Shared.Split.Constants;
using Twenty57.Linx.Sdk.Core.LinxProperties;

namespace Twenty57.Linx.Plugins.Pdf.Split;

internal partial class MethodBody : CustomT4RuntimeBase
{
    public MethodBody(IFunctionBuilder functionBuilder)
        : base(functionBuilder)
    {
        InputPdfFileInfo = new PdfFileInfo(Shared.Common.Constants.CategoryNames.Input, functionBuilder);

        OutputFilePathParameterName = functionBuilder.GetParamName(Shared.Common.Constants.PropertyIds.OutputFilePath);

        var functionData = functionBuilder.Data;
        SplitLoopResults = functionData.Properties[PropertyIds.LoopResults].GetValue<bool>();

        ExecutionPathResultParameterName = functionBuilder.ExecutionPathParamName;
        GetCreateNextResult = valueExpression => functionBuilder.GetCreateNextResult(ExecutionPathNames.PageFiles, valueExpression);

        ResultTypeName = functionBuilder.GetTypeName(functionData.Result);
        ResultNumberOfPagesPropertyName = functionBuilder.GetParamName(OutputNames.NumberOfPages);
        ResultPageFilesPropertyName = functionBuilder.GetParamName(OutputNames.PageFiles);

        ContextParameterName = functionBuilder.ContextParamName;
        ExecutorName = typeof(Pdf.Runtime.Split.Executor).FullName;
    }

    public PdfFileInfo InputPdfFileInfo { get; }
    public string OutputFilePathParameterName { get; }

    public bool SplitLoopResults { get; }

    public string ExecutionPathResultParameterName { get; }
    public Func<string, string> GetCreateNextResult { get; }

    public string ResultTypeName { get; }
    public string ResultNumberOfPagesPropertyName { get; }
    public string ResultPageFilesPropertyName { get; }

    public string ContextParameterName { get; }
    public string ExecutorName { get; }
}
