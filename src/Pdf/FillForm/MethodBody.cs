﻿using Twenty57.Linx.Interfaces.Plugin.FunctionProvider;
using Twenty57.Linx.Plugins.Pdf.Common.CodeGeneration;
using Twenty57.Linx.Plugins.Pdf.Runtime;
using Twenty57.Linx.Plugins.Pdf.Shared.Common;
using Twenty57.Linx.Plugins.Pdf.Shared.FillForm.Constants;
using Twenty57.Linx.Sdk.Core.LinxProperties;

namespace Twenty57.Linx.Plugins.Pdf.FillForm;

internal partial class MethodBody : CustomT4RuntimeBase
{
    public MethodBody(IFunctionBuilder functionBuilder)
        : base(functionBuilder)
    {
        InputPdfFileInfo = new PdfFileInfo(Shared.Common.Constants.CategoryNames.Input, functionBuilder);
        OutputFilePathParameterName = functionBuilder.GetParamName(Shared.Common.Constants.PropertyIds.OutputFilePath);
        FormDataParameterName = functionBuilder.GetParamName(PropertyIds.FormData);

        var propertyMappingString = functionBuilder.Data.Properties[PropertyIds.PropertyMapping].GetValue<string>();
        if (!PropertyMapping.TryParse(propertyMappingString, out var propertyMapping))
        {
            throw new Exception("Property mapping is not a valid JSON string.");
        }
        PropertyMapping = propertyMapping;

        ContextParameterName = functionBuilder.ContextParamName;
        ExecutorName = typeof(Runtime.FillForm.Executor).FullName;
    }

    public PdfFileInfo InputPdfFileInfo { get; }
    public string OutputFilePathParameterName { get; }
    public string FormDataParameterName { get; }
    public PropertyMapping PropertyMapping { get; }
    public string ContextParameterName { get; }
    public string ExecutorName { get; }
}
