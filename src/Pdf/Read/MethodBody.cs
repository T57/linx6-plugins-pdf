﻿using Twenty57.Linx.Interfaces.Plugin.FunctionProvider;
using Twenty57.Linx.Plugins.Pdf.Common.CodeGeneration;
using Twenty57.Linx.Plugins.Pdf.Runtime;
using Twenty57.Linx.Plugins.Pdf.Shared.Common;
using Twenty57.Linx.Plugins.Pdf.Shared.Read;
using Twenty57.Linx.Plugins.Pdf.Shared.Read.Constants;
using Twenty57.Linx.Sdk.Core.LinxProperties;

namespace Twenty57.Linx.Plugins.Pdf.Read;

internal partial class MethodBody : CustomT4RuntimeBase
{
    public MethodBody(IFunctionBuilder functionBuilder)
        : base(functionBuilder)
    {
        InputPdfFileInfo = new PdfFileInfo(Shared.Common.Constants.CategoryNames.Input, functionBuilder);

        var functionData = functionBuilder.Data;
        ReadText = functionData.Properties[PropertyIds.ReadText].GetValue<bool>();
        ReadFormData = functionData.Properties[PropertyIds.ReadFormData].GetValue<bool>();
        ReadSignature = functionData.Properties[PropertyIds.ReadSignature].GetValue<bool>();

        HasResult = ReadText || ReadFormData || ReadSignature;

        if (HasResult)
        {
            ResultTypeName = functionBuilder.GetTypeName(functionData.Result);
        }

        if (ReadText)
        {
            ExtractionStrategy = functionData.Properties[PropertyIds.ExtractionStrategy].GetValue<TextExtractionStrategy>();
            TextSplit = functionData.Properties[PropertyIds.SplitText].GetValue<TextSplit>();

            ResultTextPropertyName = functionBuilder.GetParamName(OutputNames.Text);
        }

        if (ReadFormData)
        {
            FormDataSpecificationType = functionData.Properties[PropertyIds.FormDataSpecificationType].GetValue<FormDataSpecificationType>();

            if (FormDataSpecificationType == FormDataSpecificationType.List)
            {
                ResultFormDataListPropertyName = functionBuilder.GetParamName(OutputNames.FormDataList);
            }
            else
            {
                PropertyMapping = functionBuilder.Data.Properties[PropertyIds.ComputedPropertyMapping].GetValue<PropertyMapping>();
                ResultFormDataPropertyName = functionBuilder.GetParamName(OutputNames.FormData);
                ResultFormDataTypeName = functionBuilder.GetTypeName(functionData.Result.GetProperty(OutputNames.FormData).TypeReference);
            }
        }

        if (ReadSignature)
        {
            ResultSignaturesPropertyName = functionBuilder.GetParamName(OutputNames.Signatures);
        }

        ContextParameterName = functionBuilder.ContextParamName;
        ExecutorName = typeof(Runtime.Read.Executor).FullName;
    }

    public PdfFileInfo InputPdfFileInfo { get; }

    public bool ReadText { get; }
    public TextExtractionStrategy ExtractionStrategy { get; }
    public TextSplit TextSplit { get; }

    public bool ReadFormData { get; }
    public FormDataSpecificationType FormDataSpecificationType { get; }
    public PropertyMapping PropertyMapping { get; }

    public bool ReadSignature { get; }

    public bool HasResult { get; }
    public string ResultTypeName { get; }

    public string ResultTextPropertyName { get; }

    public string ResultFormDataPropertyName { get; }
    public string ResultFormDataTypeName { get; }
    public string ResultFormDataListPropertyName { get; }

    public string ResultSignaturesPropertyName { get; }
    public string ResultSignaturesTypeName { get; }
    public string ResultIsSignedPropertyName { get; }
    public string ResultLatestSignaturePropertyName { get; }
    public string ResultLatestSignatureTypeName { get; }
    public string ResultAllSignaturesPropertyName { get; }
    public string ResultSignedByName { get; }
    public string ResultSignedAtName { get; }
    public string ResultReasonName { get; }
    public string ResultSignedOnName { get; }
    public string ResultUnmodifiedName { get; }
    public string ResultSignedRevisionName { get; }
    public string ResultIsLatestRevisionName { get; }
    public string ResultVerifiedName { get; }
    public string ResultVerificationMessageName { get; }

    public string ContextParameterName { get; }
    public string ExecutorName { get; }
}
