﻿using Twenty57.Linx.Interfaces.Plugin.FunctionProvider;
using Twenty57.Linx.Plugins.Pdf.Common.CodeGeneration;
using Twenty57.Linx.Plugins.Pdf.Runtime;
using Twenty57.Linx.Plugins.Pdf.Shared.Sign;
using Twenty57.Linx.Plugins.Pdf.Shared.Sign.Constants;
using Twenty57.Linx.Sdk.Core.LinxProperties;

namespace Twenty57.Linx.Plugins.Pdf.Sign;

internal partial class MethodBody : CustomT4RuntimeBase
{
    public MethodBody(IFunctionBuilder functionBuilder)
        : base(functionBuilder)
    {
        InputPdfFileInfo = new PdfFileInfo(Shared.Common.Constants.CategoryNames.Input, functionBuilder);

        OutputFilePathParameterName = functionBuilder.GetParamName(Shared.Common.Constants.PropertyIds.OutputFilePath);

        SignedAtParameterName = functionBuilder.GetParamName(PropertyIds.SignedAt);
        ReasonParameterName = functionBuilder.GetParamName(PropertyIds.Reason);

        var functionData = functionBuilder.Data;
        LockAfterSigning = functionData.Properties[PropertyIds.LockAfterSigning].GetValue<bool>();

        SigningCertificateInfo = new CertificateInfo(PropertyGroupIds.SigningCertificate, functionBuilder);

        SignaturePlacement = functionData.Properties[PropertyIds.SignaturePlacement].GetValue<SignaturePlacement>();

        switch (SignaturePlacement)
        {
            case SignaturePlacement.FormField:
                {
                    FieldNameParameterName = functionBuilder.GetParamName(PropertyIds.FieldName);
                    break;
                }
            case SignaturePlacement.OnPage:
                {
                    PositionXParameterName = functionBuilder.GetParamName(PropertyIds.PositionX);
                    PositionYParameterName = functionBuilder.GetParamName(PropertyIds.PositionY);
                    WidthParameterName = functionBuilder.GetParamName(PropertyIds.Width);
                    HeightParameterName = functionBuilder.GetParamName(PropertyIds.Height);
                    PageNumberParameterName = functionBuilder.GetParamName(PropertyIds.Page);
                    break;
                }
        }

        if (SignaturePlacement != SignaturePlacement.Hidden)
        {
            BackgroundImageParameterName = functionBuilder.GetParamName(PropertyIds.BackgroundImage);
        }

        ContextParameterName = functionBuilder.ContextParamName;
        ExecutorName = typeof(Pdf.Runtime.Sign.Executor).FullName;
    }

    public PdfFileInfo InputPdfFileInfo { get; }

    public string OutputFilePathParameterName { get; }

    public string SignedAtParameterName { get; }
    public string ReasonParameterName { get; }
    public bool LockAfterSigning { get; }
    public SignaturePlacement SignaturePlacement { get; }

    public CertificateInfo SigningCertificateInfo { get; }

    public string FieldNameParameterName { get; }
    public string PositionXParameterName { get; }
    public string PositionYParameterName { get; }
    public string WidthParameterName { get; }
    public string HeightParameterName { get; }
    public string BackgroundImageParameterName { get; }
    public string PageNumberParameterName { get; }

    public string ContextParameterName { get; }
    public string ExecutorName { get; }
}
