﻿using System.Security.Cryptography.X509Certificates;
using iText.Bouncycastle.X509;
using iText.Commons.Bouncycastle.Cert;
using iText.Kernel.Pdf;
using Org.BouncyCastle.Security;
using Twenty57.Linx.Interfaces.Plugin.FunctionProvider;
using Twenty57.Linx.Plugins.Pdf.Runtime.ChangeProtection;

namespace Twenty57.Linx.Plugins.Pdf.ChangeProtection;

internal class CodeGenerator : IFunctionCodeGenerator
{
    public void GenerateCode(IFunctionBuilder functionBuilder)
    {
        var template = new MethodBody(functionBuilder);
        functionBuilder.AddCode(template.TransformText());

        functionBuilder.AddAssemblyReference(typeof(PdfReader));
        functionBuilder.AddAssemblyReference(typeof(IX509Certificate));
        functionBuilder.AddAssemblyReference(typeof(X509Certificate));
        functionBuilder.AddAssemblyReference(typeof(X509CertificateBC));
        functionBuilder.AddAssemblyReference(typeof(DotNetUtilities));
        functionBuilder.AddAssemblyReference(typeof(Executor));
    }
}
