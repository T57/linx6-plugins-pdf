﻿using Twenty57.Linx.Interfaces.Plugin.Contexts;
using Twenty57.Linx.Interfaces.Plugin.FunctionProvider;
#if Windows
using Twenty57.Linx.Plugins.Pdf.Design.ChangeProtection;
#endif
using Twenty57.Linx.Plugins.Pdf.Shared.ChangeProtection;
using Twenty57.Linx.Sdk.Core.LinxFunctionProvider;

namespace Twenty57.Linx.Plugins.Pdf.ChangeProtection;

public class Provider : FunctionProvider
{
    public override string Name => "ChangeProtection";

    public override string SearchKeywords => "pdf change protection";

    public override IFunctionDesigner CreateDesigner(IFunctionDesignerContext context)
    {
#if Windows
        return new Designer(context);
#else
        throw new NotImplementedException("Designer not available in non-Windows target.");
#endif
    }

    public override IFunctionDesigner CreateDesigner(IFunctionData data, IFunctionDesignerContext context)
    {
#if Windows
        return new Designer(data, context);
#else
        throw new NotImplementedException("Designer not available in non-Windows target.");
#endif
    }

    public override IFunctionCodeGenerator CreateCodeGenerator() => new CodeGenerator();

    public override bool TryUpdateToLatestVersion(IFunctionData data, IUpdateContext context, out IFunctionData updatedData)
    {
        return Updater.Instance.TryUpdateToLatestVersion(data, context, out updatedData);
    }
}
