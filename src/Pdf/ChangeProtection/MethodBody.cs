﻿using Twenty57.Linx.Interfaces.Plugin.FunctionProvider;
using Twenty57.Linx.Plugins.Pdf.Common.CodeGeneration;
using Twenty57.Linx.Plugins.Pdf.Runtime;
using Twenty57.Linx.Plugins.Pdf.Shared.ChangeProtection;
using Twenty57.Linx.Plugins.Pdf.Shared.ChangeProtection.Constants;
using Twenty57.Linx.Plugins.Pdf.Shared.Common;
using Twenty57.Linx.Sdk.Core.LinxProperties;

namespace Twenty57.Linx.Plugins.Pdf.ChangeProtection;

internal partial class MethodBody : CustomT4RuntimeBase
{
    public MethodBody(IFunctionBuilder functionBuilder)
        : base(functionBuilder)
    {
        InputPdfFileInfo = new PdfFileInfo(Shared.Common.Constants.CategoryNames.Input, functionBuilder);
        OutputPdfFileInfo = new PdfFileInfo(Shared.Common.Constants.CategoryNames.Output, functionBuilder);

        var functionData = functionBuilder.Data;
        if (OutputPdfFileInfo.AuthenticationType != AuthenticationType.None)
        {
            AddDocumentRestrictions = functionData.Properties[PropertyIds.AddDocumentRestrictions].GetValue<bool>();
            if (AddDocumentRestrictions)
            {
                AllowPrinting = functionData.Properties[PropertyIds.AllowPrinting].GetValue<Printing>();
                AllowChanges = functionData.Properties[PropertyIds.AllowChanges].GetValue<Changes>();
                AllowCopying = functionData.Properties[PropertyIds.AllowCopying].GetValue<bool>();
                AllowScreenReaders = functionData.Properties[PropertyIds.AllowScreenReaders].GetValue<bool>();

                if (OutputPdfFileInfo.AuthenticationType == AuthenticationType.Password)
                {
                    PermissionsPasswordParameterName = functionBuilder.GetParamName(PropertyIds.PermissionsPassword);
                }
            }

            Encryption = functionData.Properties[PropertyIds.Encryption].GetValue<Encryption>();
            DontEncryptMetadata = functionData.Properties[PropertyIds.DontEncryptMetadata].GetValue<bool>();
        }

        ContextParameterName = functionBuilder.ContextParamName;
        ExecutorName = typeof(Pdf.Runtime.ChangeProtection.Executor).FullName;
    }

    public PdfFileInfo InputPdfFileInfo { get; }
    public PdfFileInfo OutputPdfFileInfo { get; }

    public bool AddDocumentRestrictions { get; }
    public Printing AllowPrinting { get; }
    public Changes AllowChanges { get; }
    public bool AllowCopying { get; }
    public bool AllowScreenReaders { get; }
    public string PermissionsPasswordParameterName { get; }

    public Encryption Encryption { get; }
    public bool DontEncryptMetadata { get; }

    public string ContextParameterName { get; }
    public string ExecutorName { get; }
}
