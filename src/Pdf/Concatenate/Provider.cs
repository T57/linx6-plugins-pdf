﻿using Twenty57.Linx.Interfaces.Plugin.Contexts;
using Twenty57.Linx.Interfaces.Plugin.FunctionProvider;
#if Windows
using Twenty57.Linx.Plugins.Pdf.Design.Concatenate;
#endif
using Twenty57.Linx.Sdk.Core.LinxFunctionProvider;

namespace Twenty57.Linx.Plugins.Pdf.Concatenate;

public class Provider : FunctionProvider
{
    public override string Name => "Concatenate";

    public override string SearchKeywords => "pdf concatenate";

    public override IFunctionDesigner CreateDesigner(IFunctionDesignerContext context)
    {
#if Windows
        return new Designer(context);
#else
        throw new NotImplementedException("Designer not available in non-Windows target.");
#endif
    }

    public override IFunctionDesigner CreateDesigner(IFunctionData data, IFunctionDesignerContext context)
    {
#if Windows
        return new Designer(data, context);
#else
        throw new NotImplementedException("Designer not available in non-Windows target.");
#endif
    }

    public override IFunctionCodeGenerator CreateCodeGenerator() => new CodeGenerator();
}
