﻿using iText.Kernel.Pdf;
using Twenty57.Linx.Interfaces.Plugin.FunctionProvider;
using Twenty57.Linx.Plugins.Pdf.Runtime.Concatenate;
using Twenty57.Linx.Plugins.Pdf.Shared.Concatenate.Constants;

namespace Twenty57.Linx.Plugins.Pdf.Concatenate;

internal class CodeGenerator : IFunctionCodeGenerator
{
    public void GenerateCode(IFunctionBuilder functionBuilder)
    {
        functionBuilder.AddCode(
$@"{typeof(Executor).FullName}.Concatenate(
    {functionBuilder.GetParamName(PropertyIds.InputFiles)},
    {functionBuilder.GetParamName(Shared.Common.Constants.PropertyIds.OutputFilePath)},
    {functionBuilder.ContextParamName});");

        functionBuilder.AddAssemblyReference(typeof(PdfReader));
        functionBuilder.AddAssemblyReference(typeof(Executor));
    }
}
