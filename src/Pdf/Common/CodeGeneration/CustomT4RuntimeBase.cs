﻿using Twenty57.Linx.Interfaces.Plugin.FunctionProvider;

namespace Twenty57.Linx.Plugins.Pdf.Common.CodeGeneration;

internal abstract class CustomT4RuntimeBase : T4RuntimeBase
{
    protected CustomT4RuntimeBase(IFunctionBuilder _) { }

    public virtual string TransformText() => throw new NotImplementedException();

    public virtual void Initialize() { }
}
