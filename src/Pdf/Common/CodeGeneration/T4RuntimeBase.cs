﻿namespace Twenty57.Linx.Plugins.Pdf.Common.CodeGeneration;

internal abstract class T4RuntimeBase
{
    private System.Text.StringBuilder builder;

    private IDictionary<string, object> session;

    private System.CodeDom.Compiler.CompilerErrorCollection errors;

    private string currentIndent = string.Empty;

    private Stack<int> indents;

    private readonly ToStringInstanceHelper _toStringHelper = new ToStringInstanceHelper();

    public virtual IDictionary<string, object> Session
    {
        get => this.session;
        set => this.session = value;
    }

    public System.Text.StringBuilder GenerationEnvironment
    {
        get => this.builder ??= new System.Text.StringBuilder();
        set => this.builder = value;
    }

    protected System.CodeDom.Compiler.CompilerErrorCollection Errors
    {
        get => this.errors ??= new System.CodeDom.Compiler.CompilerErrorCollection();
    }

    public string CurrentIndent => this.currentIndent;

    private Stack<int> Indents => this.indents ??= new Stack<int>();

    public ToStringInstanceHelper ToStringHelper => this._toStringHelper;

    public void Error(string message)
    {
        Errors.Add(new System.CodeDom.Compiler.CompilerError(null, -1, -1, null, message));
    }

    public void Warning(string message)
    {
        System.CodeDom.Compiler.CompilerError val = new System.CodeDom.Compiler.CompilerError(null, -1, -1, null, message)
        {
            IsWarning = true
        };
        Errors.Add(val);
    }

    public string PopIndent()
    {
        if (Indents.Count == 0)
        {
            return string.Empty;
        }
        int lastPos = this.currentIndent.Length - Indents.Pop();
        string last = this.currentIndent.Substring(lastPos);
        this.currentIndent = this.currentIndent.Substring(0, lastPos);
        return last;
    }

    public void PushIndent(string indent)
    {
        Indents.Push(indent.Length);
        this.currentIndent = (this.currentIndent + indent);
    }

    public void ClearIndent()
    {
        this.currentIndent = string.Empty;
        Indents.Clear();
    }

    public void Write(string textToAppend)
    {
        GenerationEnvironment.Append(textToAppend);
    }

    public void Write(string format, params object[] args)
    {
        GenerationEnvironment.AppendFormat(format, args);
    }

    public void WriteLine(string textToAppend)
    {
        GenerationEnvironment.Append(this.currentIndent);
        GenerationEnvironment.AppendLine(textToAppend);
    }

    public void WriteLine(string format, params object[] args)
    {
        GenerationEnvironment.Append(this.currentIndent);
        GenerationEnvironment.AppendFormat(format, args);
        GenerationEnvironment.AppendLine();
    }

    public class ToStringInstanceHelper
    {

        private IFormatProvider formatProvider = System.Globalization.CultureInfo.InvariantCulture;

        public IFormatProvider FormatProvider
        {
            get => this.formatProvider;
            set
            {
                if (value != null)
                {
                    this.formatProvider = value;
                }
            }
        }

        public string ToStringWithCulture(object objectToConvert)
        {
            if (objectToConvert == null)
            {
                throw new ArgumentNullException("objectToConvert");
            }
            Type type = objectToConvert.GetType();
            Type iConvertibleType = typeof(IConvertible);
            if (iConvertibleType.IsAssignableFrom(type))
            {
                return ((IConvertible)(objectToConvert)).ToString(this.formatProvider);
            }
            System.Reflection.MethodInfo methInfo = type.GetMethod("ToString", new Type[] {
                        iConvertibleType});
            if (methInfo != null)
            {
                return (string)(methInfo.Invoke(objectToConvert, new object[] {
                            this.formatProvider}));
            }
            return objectToConvert.ToString();
        }
    }
}
