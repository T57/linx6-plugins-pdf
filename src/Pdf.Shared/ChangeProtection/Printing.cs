﻿using System.ComponentModel;

namespace Twenty57.Linx.Plugins.Pdf.Shared.ChangeProtection;

public enum Printing
{
    None,
    [Description("Low resolution (150 dpi)")]
    LowResolution,
    [Description("High resolution")]
    HighResolution
}
