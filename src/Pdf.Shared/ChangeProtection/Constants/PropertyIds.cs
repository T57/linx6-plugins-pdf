﻿namespace Twenty57.Linx.Plugins.Pdf.Shared.ChangeProtection.Constants;

public static class PropertyIds
{
    public const string AddDocumentRestrictions = "AddDocumentRestrictions";
    public const string AllowChanges = "AllowChanges";
    public const string AllowCopying = "AllowCopying";
    public const string AllowPrinting = "AllowPrinting";
    public const string AllowScreenReaders = "AllowScreenReaders";
    public const string DontEncryptMetadata = "DontEncryptMetadata";
    public const string Encryption = "Encryption";
    public const string PermissionsPassword = "PermissionsPassword";
}
