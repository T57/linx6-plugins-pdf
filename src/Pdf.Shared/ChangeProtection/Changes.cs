﻿using System.ComponentModel;

namespace Twenty57.Linx.Plugins.Pdf.Shared.ChangeProtection;

public enum Changes
{
    None,
    [Description("Inserting, deleting and rotating pages")]
    Assembly,
    [Description("Filling in form fields and signing existing signature fields")]
    FillIn,
    [Description("Commenting, filling in form fields and signing existing signature fields")]
    AnnotateAndFillIn,
    [Description("Any except extracting pages")]
    AnyExceptExtract
}
