﻿using System.ComponentModel;

namespace Twenty57.Linx.Plugins.Pdf.Shared.ChangeProtection;

public enum Encryption
{
    [Description("RC4 encryption (128 bit)")]
    Standard128,
    [Description("AES encryption (128 bit)")]
    AES128,
    [Description("AES encryption (256 bit)")]
    AES256
}
