﻿using System.ComponentModel;

namespace Twenty57.Linx.Plugins.Pdf.Shared.AddWatermark;

public enum WatermarkPosition
{
    [Description("Above original")]
    Above,
    [Description("Below original")]
    Below
}
