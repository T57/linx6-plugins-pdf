﻿namespace Twenty57.Linx.Plugins.Pdf.Shared.AddWatermark.Constants;

public static class PropertyIds
{
    public const string Pages = "Pages";
    public const string Position = "Position";
}
