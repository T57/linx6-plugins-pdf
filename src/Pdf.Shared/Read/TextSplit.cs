﻿using System.ComponentModel;

namespace Twenty57.Linx.Plugins.Pdf.Shared.Read;

public enum TextSplit
{
    Never,
    [Description("Per page")]
    Page
}
