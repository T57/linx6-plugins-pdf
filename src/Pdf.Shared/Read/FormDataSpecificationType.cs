﻿using System.ComponentModel;

namespace Twenty57.Linx.Plugins.Pdf.Shared.Read;

public enum FormDataSpecificationType
{
    [Description("Custom type")]
    CustomType,
    [Description("Infer type from a sample PDF")]
    Infer,
    List
}
