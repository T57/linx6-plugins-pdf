﻿namespace Twenty57.Linx.Plugins.Pdf.Shared.Read;

public sealed class SignatureInfo
{
    public SignatureInfo(IEnumerable<Signature> signatures)
    {
        AllSignatures = signatures.ToList();
    }

    public bool IsSigned => AllSignatures.Any();

    public Signature? LatestSignature => AllSignatures.MaxBy(s => s.SignedOn);

    public List<Signature> AllSignatures { get; }
}

public record Signature(
    string SignedBy,
    string SignedAt,
    DateTime SignedOn,
    string Reason,
    bool Unmodified,
    int SignedVersion,
    bool IsLatestVersionSigned,
    bool Verified,
    string VerificationMessage
);
