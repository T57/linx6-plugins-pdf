﻿namespace Twenty57.Linx.Plugins.Pdf.Shared.Read.Constants;

public static class PropertyIds
{
    public const string ReadText = "ReadText";
    public const string ReadFormData = "ReadFormData";
    public const string ReadSignature = "ReadSignature";

    public const string ExtractionStrategy = "ExtractionStrategy";
    public const string SplitText = "SplitText";

    public const string FormDataSpecificationType = "FormDataSpecificationType";
    public const string FormDataType = "FormDataType";
    public const string SamplePdf = "SamplePDF";
    public const string PropertyMappingOverrides = "PropertyMappingOverrides";
    public const string ComputedPropertyMapping = "ComputedPropertyMapping";
}
