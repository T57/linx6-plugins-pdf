﻿namespace Twenty57.Linx.Plugins.Pdf.Shared.Read.Constants;

public static class OutputNames
{
    public const string Text = "Text";
    public const string FormData = "FormData";
    public const string FormDataList = "FormDataList";
    public const string Signatures = "Signatures";
}
