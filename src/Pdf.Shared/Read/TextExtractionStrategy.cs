﻿using System.ComponentModel;

namespace Twenty57.Linx.Plugins.Pdf.Shared.Read;

public enum TextExtractionStrategy
{
    Location,
    Simple,
    [Description("Top to bottom")]
    TopToBottom
}
