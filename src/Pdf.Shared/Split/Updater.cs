﻿using Twenty57.Linx.Interfaces.Plugin.Contexts;
using Twenty57.Linx.Interfaces.Plugin.FunctionProvider;
using Twenty57.Linx.Plugins.Pdf.Shared.Common;
using Twenty57.Linx.Sdk.Core.PluginData;

namespace Twenty57.Linx.Plugins.Pdf.Shared.Split;

public sealed class Updater
{
    private static Updater? instance;

    private Updater() { }

    public static Updater Instance => instance ??= new();
    public string CurrentVersion => "1";

    public bool TryUpdateToLatestVersion(IFunctionData functionData, IUpdateContext updateContext, out IFunctionData updatedData)
    {
        updatedData = functionData;

        if (functionData.Version == CurrentVersion)
        {
            return false;
        }
        if (string.IsNullOrEmpty(functionData.Version))
        {
            updatedData = UpdateToVersion1(updatedData, updateContext);
        }

        return updatedData.Version == CurrentVersion
            ? true : throw new Exception($"Unexpected version [{updatedData.Version}].");
    }

    private IFunctionData UpdateToVersion1(IFunctionData updatedData, IUpdateContext updateContext)
    {
        updatedData = UpdaterHelpers.ReplaceCertificateStoreProperties(updatedData, updateContext, Common.Constants.CategoryNames.Input);
        return updatedData.UpdateVersion("1");
    }
}
