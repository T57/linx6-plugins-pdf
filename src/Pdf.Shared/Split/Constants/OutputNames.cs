﻿namespace Twenty57.Linx.Plugins.Pdf.Shared.Split.Constants;

public static class OutputNames
{
    public const string NumberOfPages = "NumberOfPages";
    public const string PageFiles = "PageFiles";
}
