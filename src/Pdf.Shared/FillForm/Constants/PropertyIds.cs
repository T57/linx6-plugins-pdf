﻿namespace Twenty57.Linx.Plugins.Pdf.Shared.FillForm.Constants;

public static class PropertyIds
{
    public const string FormData = "FormData";
    public const string PropertyMapping = "PropertyMapping";
}
