﻿namespace Twenty57.Linx.Plugins.Pdf.Shared.Obsolete;

internal class ObsoletePropertyNames
{
    public const string CertificateSourceSuffix = "CertificateSource";
    public const string StoredCertificateSuffix = "StoredCertificate";
}
