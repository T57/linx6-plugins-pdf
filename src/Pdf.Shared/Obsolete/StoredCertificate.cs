﻿using System.Runtime.Serialization;
using System.Security.Cryptography.X509Certificates;

namespace Twenty57.Linx.Plugins.Pdf.Shared.Obsolete;

[Serializable]
public class StoredCertificate : ISerializable
{
    private X509Certificate2? certificate;

    public StoredCertificate()
    { }

    public StoredCertificate(StoreLocation location, StoreName name, X509Certificate2 certificate)
    {
        StoreLocation = location;
        StoreName = name;
        Thumbprint = certificate.Thumbprint;

        this.certificate = certificate;
    }

    public StoredCertificate(StoreLocation location, StoreName name, string thumbprint)
    {
        StoreLocation = location;
        StoreName = name;
        Thumbprint = thumbprint;

        GetCertificate();
    }

    protected StoredCertificate(SerializationInfo info, StreamingContext context)
    {
        StoreLocation = (StoreLocation)info.GetValue("StoreLocation", typeof(StoreLocation))!;
        StoreName = (StoreName)info.GetValue("StoreName", typeof(StoreName))!;
        Thumbprint = info.GetString("Thumbprint");
    }

    public StoreLocation StoreLocation { get; }
    public StoreName StoreName { get; }
    public string? Thumbprint { get; }

    public void GetObjectData(SerializationInfo info, StreamingContext context)
    {
        info.AddValue("StoreLocation", StoreLocation);
        info.AddValue("StoreName", StoreName);
        info.AddValue("Thumbprint", Thumbprint);
    }

    public X509Certificate2 GetCertificate()
    {
        if (this.certificate != null)
        {
            return this.certificate;
        }

        using var store = new X509Store(StoreName, StoreLocation);
        store.Open(OpenFlags.ReadOnly);
        return this.certificate = store.Certificates.Find(X509FindType.FindByThumbprint, Thumbprint!, false)
            .OfType<X509Certificate2>()
            .SingleOrDefault() ?? throw new Exception($"Certificate with thumbprint [{Thumbprint}] not found.");
    }

    public override string ToString()
    {
        if (string.IsNullOrWhiteSpace(Thumbprint))
        {
            return "Select a certificate";
        }

        if (this.certificate == null)
        {
            return "Certificate not found";
        }

        return new string[] { this.certificate.FriendlyName, this.certificate.Subject, this.certificate.Issuer, this.certificate.Thumbprint }
        .First(v => !string.IsNullOrEmpty(v));
    }
}
