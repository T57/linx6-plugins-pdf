﻿namespace Twenty57.Linx.Plugins.Pdf.Shared.Concatenate.Constants;

public static class PropertyIds
{
    public const string InputFiles = "InputFilePaths";
    public const string OutputFilePath = "OutputFilePath";
}
