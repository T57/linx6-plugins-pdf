﻿namespace Twenty57.Linx.Plugins.Pdf.Shared.Common.Constants;

public static class PropertyIds
{
    public const string OutputFilePath = "OutputFilePath";

    public const string PdfFilePathSuffix = "FilePath";
    public const string PdfPasswordSuffix = "Password";
    public const string PdfAuthenticationTypeSuffix = "AuthenticationType";
    public const string CertificateFilePathSuffix = "CertificateFilePath";
    public const string CertificateFilePasswordSuffix = "CertificateFilePassword";
}
