﻿namespace Twenty57.Linx.Plugins.Pdf.Shared.Common.Constants;

public static class CategoryNames
{
    public const string Input = "Input";
    public const string Output = "Output";
}
