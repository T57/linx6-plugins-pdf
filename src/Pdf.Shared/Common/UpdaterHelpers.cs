﻿using Twenty57.Linx.Interfaces.Plugin.ComponentProperties;
using Twenty57.Linx.Interfaces.Plugin.Contexts;
using Twenty57.Linx.Interfaces.Plugin.FunctionProvider;
using Twenty57.Linx.Plugins.Pdf.Shared.Common.Constants;
using Twenty57.Linx.Plugins.Pdf.Shared.Obsolete;
using Twenty57.Linx.Sdk.Core.LinxProperties;
using Twenty57.Linx.Sdk.Core.PluginData;

namespace Twenty57.Linx.Plugins.Pdf.Shared.Common;

internal static class UpdaterHelpers
{
    public static IFunctionData ReplaceCertificateStoreProperties(IFunctionData functionData, IUpdateContext updateContext, string propertyGroupId)
    {
        if (functionData.TryFindPropertyById(propertyGroupId + ObsoletePropertyNames.CertificateSourceSuffix, out var certificateSourceProperty))
        {
            functionData = functionData.RemoveProperty(certificateSourceProperty);
        }

        if (functionData.TryFindPropertyById(propertyGroupId + ObsoletePropertyNames.StoredCertificateSuffix, out var certificateStoreProperty))
        {
            functionData = functionData.RemoveProperty(certificateStoreProperty);

            var certificateFilePathProperty = updateContext.CreatePropertyData(propertyGroupId + PropertyIds.CertificateFilePathSuffix, typeof(string), ValueUseOption.RuntimeRead, string.Empty);
            var certificatePasswordProperty = updateContext.CreatePropertyData(propertyGroupId + PropertyIds.CertificateFilePasswordSuffix, typeof(string), ValueUseOption.RuntimeRead, string.Empty);
            functionData = functionData
                .AddProperty(certificateFilePathProperty)
                .AddProperty(certificatePasswordProperty);
        }
        return functionData;
    }
}
