﻿namespace Twenty57.Linx.Plugins.Pdf.Shared.Common;

public enum AuthenticationType
{
    None,
    Password,
    Certificate
}
