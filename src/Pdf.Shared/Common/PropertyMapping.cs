﻿using System.Diagnostics.CodeAnalysis;
using System.Text.Json;

namespace Twenty57.Linx.Plugins.Pdf.Shared.Common;

public sealed class PropertyMapping : List<PropertyMappingEntry>
{
    public PropertyMapping() { }

    public PropertyMapping(IEnumerable<PropertyMappingEntry> propertyMappingEntries)
    {
        AddRange(propertyMappingEntries);
    }

    public override string ToString()
    {
        return JsonSerializer.Serialize(this);
    }

    public static bool TryParse(string? value, [NotNullWhen(true)] out PropertyMapping? propertyMapping)
    {
        if (string.IsNullOrEmpty(value))
        {
            propertyMapping = new PropertyMapping();
            return true;
        }

        try
        {
            propertyMapping = JsonSerializer.Deserialize<PropertyMapping>(value)!;
            return true;
        }
        catch (JsonException)
        {
            propertyMapping = null;
            return false;
        }
    }
}

public record PropertyMappingEntry(string FieldName, string PropertyName);
