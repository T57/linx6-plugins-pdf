﻿namespace Twenty57.Linx.Plugins.Pdf.Shared.Sign.Constants;

public static class PropertyGroupIds
{
    public const string SigningCertificate = "SigningCertificate";
}
