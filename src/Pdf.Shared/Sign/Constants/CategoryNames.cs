﻿namespace Twenty57.Linx.Plugins.Pdf.Shared.Sign.Constants;

public static class CategoryNames
{
    public const string SigningCertificate = "Signing certificate";
}
