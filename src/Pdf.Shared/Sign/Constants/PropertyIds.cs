﻿namespace Twenty57.Linx.Plugins.Pdf.Shared.Sign.Constants;

public static class PropertyIds
{
    public const string SignedAt = "SignedAt";
    public const string Reason = "Reason";
    public const string LockAfterSigning = "LockAfterSigning";
    public const string SignaturePlacement = "SignaturePlacement";
    public const string FieldName = "FieldName";
    public const string PositionX = "PositionX";
    public const string PositionY = "PositionY";
    public const string Width = "Width";
    public const string Height = "Height";
    public const string BackgroundImage = "BackgroundImage";
    public const string Page = "Page";
}
