﻿using System.ComponentModel;

namespace Twenty57.Linx.Plugins.Pdf.Shared.Sign;

public enum SignaturePlacement
{
    [Description("No visible signature")]
    Hidden,
    [Description("Form signature field")]
    FormField,
    [Description("On page")]
    OnPage
}
