﻿using Twenty57.Linx.Interfaces.Plugin.ComponentProperties;
using Twenty57.Linx.Interfaces.Plugin.Contexts;
using Twenty57.Linx.Interfaces.Plugin.FunctionProvider;
using Twenty57.Linx.Plugins.Pdf.Design.Common;
using Twenty57.Linx.Plugins.Pdf.Shared.AddWatermark;
using Twenty57.Linx.Plugins.Pdf.Shared.AddWatermark.Constants;
using Twenty57.Linx.Sdk.Core;

namespace Twenty57.Linx.Plugins.Pdf.Design.AddWatermark;

public sealed class Designer : Common.Designer
{
    private readonly PdfFilePropertyGroup watermarkPropertyGroup = new(CategoryNames.Watermark);

    public Designer(IFunctionDesignerContext context)
        : base(context)
    {
        Version = Updater.Instance.CurrentVersion;

        this.watermarkPropertyGroup.AddInitialProperties(Context, Properties);

        Properties.Add(Context.CreateProperty(PropertyIds.Pages, typeof(string), ValueUseOption.RuntimeRead, string.Empty));
        Properties.Add(Context.CreateProperty(PropertyIds.Position, typeof(WatermarkPosition), ValueUseOption.DesignTime, WatermarkPosition.Above));

        SetPropertyAttributes();
    }

    public Designer(IFunctionData data, IFunctionDesignerContext context)
        : base(data, context)
    { }

    protected override void SetPropertyAttributes()
    {
        base.SetPropertyAttributes();

        this.watermarkPropertyGroup.SetPropertyAttributes(Context, Properties);

        var pagesProperty = Properties[PropertyIds.Pages];
        pagesProperty.Description = "Page range to stamp with watermark. Leave this blank to add the watermark to all pages.";

        var positionProperty = Properties[PropertyIds.Position];
        positionProperty.Description = "Draws the watermark above or below the original document content.";
    }
}
