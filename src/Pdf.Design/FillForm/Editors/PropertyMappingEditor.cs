﻿using Twenty57.Linx.Interfaces.Plugin.Types;
using Twenty57.Linx.Plugins.Pdf.Design.Common.Editors.PropertyMappingEditor;
using Twenty57.Linx.Plugins.Pdf.Shared.FillForm.Constants;
using Twenty57.Linx.Sdk.Core.LinxFunctionProvider;

namespace Twenty57.Linx.Plugins.Pdf.Design.FillForm.Editors;

internal class PropertyMappingEditor : PropertyEditor
{
    protected override ITypeReference? GetMappedTypeReference(FunctionDesigner designer)
    {
        return designer.Context.ValueResolver.GetTypeReference(designer.Properties[PropertyIds.FormData].Value);
    }
}
