﻿using Twenty57.Linx.Interfaces.Plugin.Types;
using Twenty57.Linx.Plugins.Pdf.Shared.FillForm.Constants;
using Twenty57.Linx.Sdk.Core.LinxFunctionProvider;

namespace Twenty57.Linx.Plugins.Pdf.Design.FillForm.Validators;

internal class PropertyMappingValidator : Common.Validators.PropertyMappingValidator
{
    public PropertyMappingValidator(FunctionDesigner functionDesigner, INameUtility nameUtility)
        : base(functionDesigner, nameUtility) { }

    protected override ITypeReference? GetMappedTypeReference(FunctionDesigner functionDesigner)
    {
        return functionDesigner.Context.ValueResolver.GetTypeReference(functionDesigner.Properties[PropertyIds.FormData].Value);
    }
}
