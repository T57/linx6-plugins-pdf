﻿using Twenty57.Linx.Interfaces.Plugin.ComponentProperties;
using Twenty57.Linx.Interfaces.Plugin.Contexts;
using Twenty57.Linx.Interfaces.Plugin.FunctionProvider;
using Twenty57.Linx.Plugins.Pdf.Design.FillForm.Editors;
using Twenty57.Linx.Plugins.Pdf.Design.FillForm.Validators;
using Twenty57.Linx.Plugins.Pdf.Shared.FillForm;
using Twenty57.Linx.Plugins.Pdf.Shared.FillForm.Constants;
using Twenty57.Linx.Sdk.Core;
using Twenty57.Linx.Sdk.Core.Validation;

namespace Twenty57.Linx.Plugins.Pdf.Design.FillForm;

public sealed class Designer : Common.Designer
{
    public Designer(IFunctionDesignerContext context)
        : base(context)
    {
        Version = Updater.Instance.CurrentVersion;

        Properties.Add(Context.CreateProperty(PropertyIds.FormData, typeof(object), ValueUseOption.RuntimeRead, null));
        Properties.Add(Context.CreateProperty(PropertyIds.PropertyMapping, typeof(string), ValueUseOption.DesignTime, null));

        SetPropertyAttributes();
    }

    public Designer(IFunctionData data, IFunctionDesignerContext context)
        : base(data, context)
    { }

    protected override void SetPropertyAttributes()
    {
        base.SetPropertyAttributes();

        var formDataProperty = Properties[PropertyIds.FormData];
        formDataProperty.DisplayName = "Form data";
        formDataProperty.Description = "A custom object that contains the form data to insert.";
        formDataProperty.Validations.Add(new RequiredValidator());

        var propertyMappingProperty = Properties[PropertyIds.PropertyMapping];
        propertyMappingProperty.DisplayName = "Property mapping";
        propertyMappingProperty.Description = "Specify the field names that the properties in the form data should map to.";
        propertyMappingProperty.Validations.Add(new PropertyMappingValidator(this, Context.NameUtility));
        propertyMappingProperty.Editor = new PropertyMappingEditor();
    }
}
