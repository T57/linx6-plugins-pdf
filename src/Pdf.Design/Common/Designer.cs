﻿using Twenty57.Linx.Interfaces.Plugin.ComponentProperties;
using Twenty57.Linx.Interfaces.Plugin.Contexts;
using Twenty57.Linx.Interfaces.Plugin.FunctionProvider;
using Twenty57.Linx.Plugins.Pdf.Shared.Common.Constants;
using Twenty57.Linx.Sdk.Core;
using Twenty57.Linx.Sdk.Core.LinxFunctionProvider;
using Twenty57.Linx.Sdk.Core.Validation;
using Twenty57.Linx.Sdk.UI.Editors;

namespace Twenty57.Linx.Plugins.Pdf.Design.Common;

public abstract class Designer : FunctionDesigner
{
    protected int propertyOrder = 1;

    private readonly PdfFilePropertyGroup inputPdfPropertyGroup = new(CategoryNames.Input);

    protected Designer(IFunctionDesignerContext context)
        : base(context)
    {
        this.inputPdfPropertyGroup.AddInitialProperties(Context, Properties);

        if (HasOutputFilePathProperty)
        {
            Properties.Add(Context.CreateProperty(PropertyIds.OutputFilePath, typeof(string), ValueUseOption.RuntimeRead, string.Empty));
        }
    }

    protected Designer(IFunctionData data, IFunctionDesignerContext context)
        : base(data, context)
    { }

    protected virtual bool HasOutputFilePathProperty => true;

    protected override void InitializeProperties(IReadOnlyDictionary<string, IPropertyData> properties)
    {
        base.InitializeProperties(properties);
        SetPropertyAttributes();
    }

    protected virtual void SetPropertyAttributes()
    {
        this.inputPdfPropertyGroup.SetPropertyAttributes(Context, Properties);

        if (HasOutputFilePathProperty)
        {
            var outputFilePath = Properties[PropertyIds.OutputFilePath];
            outputFilePath.Category = CategoryNames.Output;
            outputFilePath.DisplayName = "File path";
            outputFilePath.Description = "Path of the PDF file to write to.";
            outputFilePath.Editor = new FilePathEditor();
            outputFilePath.Validations.Add(new RequiredValidator());
        }
    }
}
