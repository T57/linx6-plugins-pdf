﻿using Twenty57.Linx.Interfaces.Plugin.Types;
using Twenty57.Linx.Plugins.Pdf.Design.Common.Extensions;
using Twenty57.Linx.Plugins.Pdf.Shared.Common;
using Twenty57.Linx.Sdk.Core;
using Twenty57.Linx.Sdk.Core.LinxFunctionProvider;
using Twenty57.Linx.Sdk.Core.Validation;

namespace Twenty57.Linx.Plugins.Pdf.Design.Common.Validators;

internal abstract class PropertyMappingValidator : Validator
{
    private readonly FunctionDesigner functionDesigner;
    private readonly INameUtility nameUtility;

    protected PropertyMappingValidator(FunctionDesigner functionDesigner, INameUtility nameUtility)
    {
        this.functionDesigner = functionDesigner;
        this.nameUtility = nameUtility;
    }

    protected override bool IsValid(object? value, string name)
    {
        if (!PropertyMapping.TryParse((string)value!, out var propertyMapping))
        {
            ErrorMessage = "Value is not a valid JSON string.";
            return false;
        }

        if (propertyMapping.Any(m => string.IsNullOrEmpty(m.FieldName) || string.IsNullOrEmpty(m.PropertyName)))
        {
            ErrorMessage = "Mapping cannot contain empty values.";
            return false;
        }

        if (propertyMapping.Select(m => m.FieldName).Distinct().Count() < propertyMapping.Count || propertyMapping.Select(m => m.PropertyName).Distinct().Count() < propertyMapping.Count)
        {
            ErrorMessage = "Mapping values must be unique.";
            return false;
        }

        foreach (var propertyMappingEntry in propertyMapping)
        {
            if (!this.nameUtility.IsNameValid(propertyMappingEntry.PropertyName))
            {
                ErrorMessage = $"[{propertyMappingEntry.PropertyName}] is not a valid property name.";
                return false;
            }
        }

        var mappedTypeReference = GetMappedTypeReference(this.functionDesigner);
        if (mappedTypeReference != null)
        {
            var allPropertyNames = new HashSet<string>(mappedTypeReference.GetAllPropertyNames());
            foreach (var propertyMappingEntry in propertyMapping)
            {
                if (!allPropertyNames.Contains(propertyMappingEntry.PropertyName))
                {
                    ErrorMessage = $"The target type does not have a property named [{propertyMappingEntry.PropertyName}].";
                    return false;
                }
            }
        }

        return true;
    }

    protected abstract ITypeReference? GetMappedTypeReference(FunctionDesigner functionDesigner);
}
