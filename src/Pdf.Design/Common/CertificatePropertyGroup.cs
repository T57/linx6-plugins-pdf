﻿using Twenty57.Linx.Interfaces.Plugin.ComponentProperties;
using Twenty57.Linx.Interfaces.Plugin.Contexts;
using Twenty57.Linx.Plugins.Pdf.Shared.Common.Constants;
using Twenty57.Linx.Sdk.Core;
using Twenty57.Linx.Sdk.Core.LinxProperties;
using Twenty57.Linx.Sdk.Core.Validation;
using Twenty57.Linx.Sdk.UI.Editors;

namespace Twenty57.Linx.Plugins.Pdf.Design.Common;

public sealed class CertificatePropertyGroup
{
    private readonly string propertyGroupId;
    private readonly string category;

    public CertificatePropertyGroup(string propertyGroupId)
        : this(propertyGroupId, propertyGroupId) { }

    public CertificatePropertyGroup(string propertyGroupId, string category)
    {
        this.propertyGroupId = propertyGroupId;
        this.category = category;
    }

    public string FilePathPropertyId => this.propertyGroupId + PropertyIds.CertificateFilePathSuffix;
    public string FilePasswordPropertyId => this.propertyGroupId + PropertyIds.CertificateFilePasswordSuffix;

    public void AddInitialProperties(
        IDesignerContext context,
        DesignerPropertyCollection designerProperties)
    {
        designerProperties.Add(context.CreateProperty(FilePathPropertyId, typeof(string), ValueUseOption.RuntimeRead, string.Empty));
        designerProperties.Add(context.CreateProperty(FilePasswordPropertyId, typeof(string), ValueUseOption.RuntimeRead, string.Empty));
    }

    public void RemoveProperties(DesignerPropertyCollection designerProperties)
    {
        designerProperties.Remove(FilePathPropertyId);
        designerProperties.Remove(FilePasswordPropertyId);
    }

    public void SetPropertyAttributes(DesignerPropertyCollection designerProperties)
    {
        var filePathProperty = designerProperties[FilePathPropertyId];
        filePathProperty.Category = this.category;
        filePathProperty.Order = 3;
        filePathProperty.DisplayName = "Certificate file path";
        filePathProperty.Description = "Path to the file containing a certificate.";
        filePathProperty.Editor = new FilePathEditor();
        filePathProperty.Validations.Add(new RequiredValidator());

        var filePasswordProperty = designerProperties[FilePasswordPropertyId];
        filePasswordProperty.Category = this.category;
        filePasswordProperty.Order = 4;
        filePasswordProperty.DisplayName = "Certificate file password";
        filePasswordProperty.Description = "Password required to open the certificate file.";
    }
}
