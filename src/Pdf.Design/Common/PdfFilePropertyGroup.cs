﻿using Twenty57.Linx.Interfaces.Plugin.ComponentProperties;
using Twenty57.Linx.Interfaces.Plugin.Contexts;
using Twenty57.Linx.Plugins.Pdf.Shared.Common;
using Twenty57.Linx.Plugins.Pdf.Shared.Common.Constants;
using Twenty57.Linx.Sdk.Core;
using Twenty57.Linx.Sdk.Core.LinxProperties;
using Twenty57.Linx.Sdk.Core.Validation;
using Twenty57.Linx.Sdk.UI.Editors;

namespace Twenty57.Linx.Plugins.Pdf.Design.Common;

public sealed class PdfFilePropertyGroup
{
    private readonly string propertyGroupId;
    private readonly string category;
    private readonly CertificatePropertyGroup certificatePropertyGroup;

    public PdfFilePropertyGroup(string propertyGroupId)
        : this(propertyGroupId, propertyGroupId)
    { }

    public PdfFilePropertyGroup(string propertyGroupId, string category)
    {
        this.propertyGroupId = propertyGroupId;
        this.category = category;
        this.certificatePropertyGroup = new(propertyGroupId, category);
    }

    public string FilePathPropertyId => this.propertyGroupId + PropertyIds.PdfFilePathSuffix;
    public string AuthenticationTypePropertyId => this.propertyGroupId + PropertyIds.PdfAuthenticationTypeSuffix;
    public string PasswordPropertyId => this.propertyGroupId + PropertyIds.PdfPasswordSuffix;

    public string CertificateFilePathPropertyId => this.certificatePropertyGroup.FilePathPropertyId;
    public string CertificateFilePasswordPropertyId => this.certificatePropertyGroup.FilePasswordPropertyId;

    public void AddInitialProperties(IDesignerContext context, DesignerPropertyCollection designerProperties)
    {
        designerProperties.Add(context.CreateProperty(FilePathPropertyId, typeof(string), ValueUseOption.RuntimeRead, string.Empty));
        designerProperties.Add(context.CreateProperty(AuthenticationTypePropertyId, typeof(AuthenticationType), ValueUseOption.DesignTime, AuthenticationType.None));
    }

    public void SetPropertyAttributes(IDesignerContext context, DesignerPropertyCollection designerProperties)
    {
        var filePathProperty = designerProperties[FilePathPropertyId];
        filePathProperty.Category = this.category;
        filePathProperty.Order = 0;
        filePathProperty.DisplayName = "File path";
        filePathProperty.Description = "Path to the PDF file.";
        filePathProperty.Editor = new FilePathEditor();
        filePathProperty.Validations.Add(new RequiredValidator());

        var authenticationTypeProperty = designerProperties[AuthenticationTypePropertyId];
        authenticationTypeProperty.Category = this.category;
        authenticationTypeProperty.Order = 1;
        authenticationTypeProperty.DisplayName = "Authentication type";
        authenticationTypeProperty.Description = "Authentication type required to open the PDF file.";
        authenticationTypeProperty.ValueChanged += (_, _) => AuthenticationTypeProperty_ValueChanged(context, designerProperties);

        var authenticationType = designerProperties[AuthenticationTypePropertyId].GetValue<AuthenticationType>();
        switch (authenticationType)
        {
            case AuthenticationType.Password:
                {
                    var passwordProperty = designerProperties[PasswordPropertyId];
                    passwordProperty.Category = this.category;
                    passwordProperty.Order = 2;
                    passwordProperty.DisplayName = "Password";
                    passwordProperty.Description = "Password required to access the PDF file.";
                    passwordProperty.Validations.Add(new RequiredValidator());
                    break;
                }
            case AuthenticationType.Certificate:
                {
                    this.certificatePropertyGroup.SetPropertyAttributes(designerProperties);
                    break;
                }
        }
    }

    private void AuthenticationTypeProperty_ValueChanged(IDesignerContext context, DesignerPropertyCollection designerProperties)
    {
        var authenticationType = designerProperties[AuthenticationTypePropertyId].GetValue<AuthenticationType>();
        switch (authenticationType)
        {
            case AuthenticationType.None:
                {
                    designerProperties.Remove(PasswordPropertyId);
                    this.certificatePropertyGroup.RemoveProperties(designerProperties);
                    break;
                }
            case AuthenticationType.Password:
                {
                    designerProperties.Add(context.CreateProperty(PasswordPropertyId, typeof(string), ValueUseOption.RuntimeRead, string.Empty));
                    this.certificatePropertyGroup.RemoveProperties(designerProperties);
                    break;
                }
            case AuthenticationType.Certificate:
                {
                    this.certificatePropertyGroup.AddInitialProperties(context, designerProperties);
                    designerProperties.Remove(PasswordPropertyId);
                    break;
                }
        }
    }
}
