﻿using Twenty57.Linx.Interfaces.Plugin.Types;

namespace Twenty57.Linx.Plugins.Pdf.Design.Common.Extensions;

public static class TypeReferenceExtensions
{
    public static IEnumerable<string> GetAllPropertyNames(this ITypeReference typeReference)
    {
        return typeReference.GetAllPropertyNames(new HashSet<ITypeReference>()).Distinct();
    }

    private static IEnumerable<string> GetAllPropertyNames(this ITypeReference typeReference, HashSet<ITypeReference> processedTypes)
    {
        if (processedTypes.Contains(typeReference))
        {
            return Enumerable.Empty<string>();
        }

        processedTypes.Add(typeReference);

        var properties = typeReference.GetProperties();
        return properties
            .Where(p => p.TypeReference.IsCompiled)
            .Select(p => p.Name)
        .Concat(properties
            .Where(p => p.TypeReference.IsEnumerable)
            .SelectMany(p => p.TypeReference.GetEnumerableContentType()!.GetAllPropertyNames(processedTypes)))
        .Concat(properties
            .Where(p => p.TypeReference.IsGenerated)
            .SelectMany(p => p.TypeReference.GetAllPropertyNames(processedTypes)));
    }
}
