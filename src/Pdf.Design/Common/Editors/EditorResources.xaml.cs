﻿using System.Windows;

namespace Twenty57.Linx.Plugins.Pdf.Design.Common.Editors;

internal partial class EditorResources
{
    private static EditorResources? instance;

    private EditorResources()
    {
        InitializeComponent();
    }

    public static DataTemplate CertificateInlineEditorTemplate => (DataTemplate)Instance["CertificateInlineEditorTemplate"];

    public static DataTemplate PropertyMapInlineEditorTemplate => (DataTemplate)Instance["PropertyMapInlineEditorTemplate"];

    private static EditorResources Instance => instance ??= new EditorResources();
}
