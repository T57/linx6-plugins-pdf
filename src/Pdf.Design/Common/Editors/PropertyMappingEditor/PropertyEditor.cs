﻿using Twenty57.Linx.Interfaces.Plugin.ComponentProperties;
using Twenty57.Linx.Interfaces.Plugin.Types;
using Twenty57.Linx.Plugins.Pdf.Design.Common.Editors.PropertyMappingEditor.Dialog;
using Twenty57.Linx.Plugins.Pdf.Design.Common.Editors.PropertyMappingEditor.Dialog.ViewModels;
using Twenty57.Linx.Plugins.Pdf.Shared.Common;
using Twenty57.Linx.Sdk.Core.LinxFunctionProvider;
using Twenty57.Linx.Sdk.Core.LinxProperties;
using Twenty57.Linx.Sdk.UI.Editors;

namespace Twenty57.Linx.Plugins.Pdf.Design.Common.Editors.PropertyMappingEditor;

internal abstract class PropertyEditor : DefaultPropertyEditor
{
    public override string Name => "Property Map Editor";

    public override object InlineTemplate => EditorResources.PropertyMapInlineEditorTemplate;

    public override void EditValue(IDesignerProperty propertyMappingProperty, object designer)
    {
        var functionDesigner = (FunctionDesigner)designer;

        if (!PropertyMapping.TryParse(propertyMappingProperty.GetValue<string>()!, out PropertyMapping? currentPropertyMapping))
        {
            currentPropertyMapping = new();
        }

        var viewModel = new PropertyMappingViewModel(currentPropertyMapping, GetMappedTypeReference(functionDesigner), functionDesigner.Context.NameUtility);
        if (Window.Display(viewModel))
        {
            propertyMappingProperty.Value = viewModel.SavedPropertyMapping!.ToString();
        }
    }

    protected abstract ITypeReference? GetMappedTypeReference(FunctionDesigner designer);
}
