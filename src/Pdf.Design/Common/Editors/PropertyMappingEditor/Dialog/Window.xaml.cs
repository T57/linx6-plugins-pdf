﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Twenty57.Linx.Plugins.Pdf.Design.Common.Editors.PropertyMappingEditor.Dialog.ViewModels;

namespace Twenty57.Linx.Plugins.Pdf.Design.Common.Editors.PropertyMappingEditor.Dialog;

internal partial class Window
{
    private Window(PropertyMappingViewModel context)
    {
        DataContext = context;
        InitializeComponent();
    }

    protected override bool PersistLayout => true;

    public static bool Display(PropertyMappingViewModel context)
    {
        var window = new Window(context)
        {
            Owner = Application.Current.MainWindow
        };

        return window.ShowDialog() ?? false;
    }

    private void ListView_Loaded(object sender, RoutedEventArgs e)
    {
        MoveFocus(new TraversalRequest(FocusNavigationDirection.First));
    }

    private void TextBox_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
    {
        ((TextBox)e.OriginalSource).SelectAll();
    }
}
