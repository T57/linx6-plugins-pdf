﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Twenty57.Linx.Interfaces.Plugin.Types;
using Twenty57.Linx.Plugins.Pdf.Shared.Common;
using Twenty57.Linx.Sdk.Core;
using Twenty57.Linx.Sdk.UI.Commands;

namespace Twenty57.Linx.Plugins.Pdf.Design.Common.Editors.PropertyMappingEditor.Dialog.ViewModels;

internal class PropertyMappingEntryModel : INotifyPropertyChanged
{
    private readonly PropertyMappingViewModel parent;
    private readonly INameUtility nameUtility;

    private string? fieldName;
    private string? propertyName;
    private bool isDefault;

    public PropertyMappingEntryModel(PropertyMappingEntry mappingEntry, PropertyMappingViewModel parent, INameUtility nameUtility)
        : this(parent, nameUtility)
    {
        this.fieldName = mappingEntry.FieldName;
        this.propertyName = mappingEntry.PropertyName;
    }

    private PropertyMappingEntryModel(PropertyMappingViewModel parent, INameUtility nameUtility)
    {
        this.parent = parent;
        this.nameUtility = nameUtility;
        RemoveCommand = new DelegateCommand(Remove);
    }

    public bool IsDefault
    {
        get => this.isDefault;
        set
        {
            this.isDefault = value;
            NotifyPropertyChanged();
            NotifyFieldNameValidityChanged();
            NotifyPropertyNameValidityChanged();
        }
    }

    public string? FieldName
    {
        get => this.fieldName;
        set
        {
            var oldName = this.fieldName;
            this.fieldName = value;
            NotifyPropertyChanged();
            NotifyFieldNameValidityChanged();
            FieldNameChanged?.Invoke(this, new(oldName, this.fieldName));
        }
    }

    public string? PropertyName
    {
        get => this.propertyName;
        set
        {
            var oldName = this.propertyName;
            this.propertyName = value;
            NotifyPropertyChanged();
            NotifyPropertyNameValidityChanged();
            PropertyNameChanged?.Invoke(this, new(oldName, this.propertyName));
        }
    }

    public bool FieldNameIsValid => FieldNameValidationMessage == null;

    public string? FieldNameValidationMessage
    {
        get
        {
            if (IsDefault)
            {
                return null;
            }

            if (string.IsNullOrEmpty(FieldName))
            {
                return "Field Name is required";
            }

            if (this.parent.PropertyMappingEntries.Any(entry => entry != this && entry.FieldName == FieldName))
            {
                return "Duplicate name";
            }

            return null;
        }
    }

    public bool PropertyNameIsValid => PropertyNameValidationMessage == null;

    public string? PropertyNameValidationMessage
    {
        get
        {
            if (IsDefault)
            {
                return null;
            }

            if (string.IsNullOrEmpty(PropertyName))
            {
                return "Property Name is required";
            }

            if (this.parent.PropertyMappingEntries.Any(entry => entry != this && entry.PropertyName == PropertyName))
            {
                return "Duplicate name";
            }

            if (!this.nameUtility.IsNameValid(PropertyName))
            {
                return "Name contains invalid characters";
            }

            if (this.parent.HasAvailablePropertyNames && !this.parent.AvailablePropertyNames!.Contains(PropertyName))
            {
                return "Please select an available property name";
            }

            return null;
        }
    }

    public bool IsValid => FieldNameIsValid && PropertyNameIsValid;

    public DelegateCommand RemoveCommand { get; }

    public void Remove()
    {
        this.parent.Remove(this);
    }

    public static PropertyMappingEntryModel CreateDefault(PropertyMappingViewModel parent, INameUtility nameUtility)
    {
        return new PropertyMappingEntryModel(parent, nameUtility)
        {
            IsDefault = true
        };
    }

    internal void NotifyFieldNameValidityChanged()
    {
        NotifyPropertyChanged(nameof(FieldNameIsValid));
        NotifyPropertyChanged(nameof(FieldNameValidationMessage));
        NotifyPropertyChanged(nameof(IsValid));
    }

    internal void NotifyPropertyNameValidityChanged()
    {
        NotifyPropertyChanged(nameof(PropertyNameIsValid));
        NotifyPropertyChanged(nameof(PropertyNameValidationMessage));
        NotifyPropertyChanged(nameof(IsValid));
    }

    private void NotifyPropertyChanged([CallerMemberName] string? propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    public event EventHandler<NameChangedEventArgs>? FieldNameChanged;
    public event EventHandler<NameChangedEventArgs>? PropertyNameChanged;
    public event PropertyChangedEventHandler? PropertyChanged;
}

public class NameChangedEventArgs : EventArgs
{
    public NameChangedEventArgs(string? oldName, string? newName)
    {
        OldName = oldName;
        NewName = newName;
    }

    public string? OldName { get; }
    public string? NewName { get; }
}
