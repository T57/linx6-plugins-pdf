﻿using System.Collections.Immutable;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Twenty57.Linx.Interfaces.Plugin.Types;
using Twenty57.Linx.Plugins.Pdf.Design.Common.Extensions;
using Twenty57.Linx.Plugins.Pdf.Shared.Common;
using Twenty57.Linx.Sdk.UI.Commands;

namespace Twenty57.Linx.Plugins.Pdf.Design.Common.Editors.PropertyMappingEditor.Dialog.ViewModels;

internal class PropertyMappingViewModel : INotifyPropertyChanged
{
    private readonly INameUtility nameUtility;
    private ICommand? saveCommand;
    private bool? dialogResult;

    public PropertyMappingViewModel(PropertyMapping currentPropertyMapping, ITypeReference? mappedTypeReference, INameUtility nameUtility)
    {
        this.nameUtility = nameUtility ?? throw new ArgumentNullException(nameof(nameUtility));
        InitializePropertyMappingEntries(currentPropertyMapping ?? throw new ArgumentNullException(nameof(currentPropertyMapping)));
        InitializeAvailablePropertyNames(mappedTypeReference);
    }

    public ObservableCollection<PropertyMappingEntryModel> PropertyMappingEntries { get; } = new();

    public bool HasAvailablePropertyNames => AvailablePropertyNames != null;

    public IReadOnlyList<string>? AvailablePropertyNames { get; private set; }

    public bool IsValid => PropertyMappingEntries.All(c => c.IsValid);

    public ICommand SaveCommand => this.saveCommand ??= new DelegateCommand(Save, () => IsValid);

    public bool? DialogResult
    {
        get => this.dialogResult;
        set
        {
            if (this.dialogResult != value)
            {
                this.dialogResult = value;
                NotifyPropertyChanged();
            }
        }
    }

    public PropertyMapping? SavedPropertyMapping { get; private set; }

    internal void Remove(PropertyMappingEntryModel propertyMapModel)
    {
        PropertyMappingEntries.Remove(propertyMapModel);
    }

    private void InitializePropertyMappingEntries(PropertyMapping propertyMapping)
    {
        PropertyMappingEntries.CollectionChanged += PropertyMappingEntries_CollectionChanged;

        foreach (PropertyMappingEntry entry in propertyMapping)
        {
            PropertyMappingEntries.Add(new PropertyMappingEntryModel(entry, this, this.nameUtility));
        }

        AddDefaultItem();
    }

    private void InitializeAvailablePropertyNames(ITypeReference? mappedTypeReference)
    {
        if (mappedTypeReference != null)
        {
            AvailablePropertyNames = mappedTypeReference.GetAllPropertyNames().OrderBy(n => n).ToImmutableArray();
        }
    }

    private void AddDefaultItem()
    {
        PropertyMappingEntries.Add(PropertyMappingEntryModel.CreateDefault(this, this.nameUtility));
    }

    private void PropertyMappingEntries_CollectionChanged(object? sender, NotifyCollectionChangedEventArgs e)
    {
        switch (e.Action)
        {
            case NotifyCollectionChangedAction.Add:
                {
                    foreach (var addedEntry in e.NewItems!.Cast<PropertyMappingEntryModel>())
                    {
                        addedEntry.PropertyChanged += PropertyMappingEntryModel_PropertyChanged;
                        addedEntry.FieldNameChanged += PropertyMappingEntryModel_FieldNameChanged;
                        addedEntry.PropertyNameChanged += PropertyMappingEntryModel_PropertyNameChanged;
                    }
                    break;
                }
            case NotifyCollectionChangedAction.Remove:
                {
                    foreach (var removedEntry in e.OldItems!.Cast<PropertyMappingEntryModel>())
                    {
                        removedEntry.PropertyChanged -= PropertyMappingEntryModel_PropertyChanged;
                        removedEntry.FieldNameChanged -= PropertyMappingEntryModel_FieldNameChanged;
                        removedEntry.PropertyNameChanged -= PropertyMappingEntryModel_PropertyNameChanged;

                        var sameFieldNameEntries = PropertyMappingEntries
                            .Where(remainingEntry => !remainingEntry.IsDefault && remainingEntry.FieldName == removedEntry.FieldName);
                        foreach (var sameFieldNameEntry in sameFieldNameEntries)
                        {
                            sameFieldNameEntry.NotifyFieldNameValidityChanged();
                        }

                        var samePropertyNameEntries = PropertyMappingEntries
                            .Where(remainingEntry => !remainingEntry.IsDefault && remainingEntry.PropertyName == removedEntry.PropertyName);
                        foreach (var samePropertyNameEntry in samePropertyNameEntries)
                        {
                            samePropertyNameEntry.NotifyPropertyNameValidityChanged();
                        }
                    }

                    NotifyPropertyChanged(nameof(IsValid));
                    break;
                }
        }
    }

    private void PropertyMappingEntryModel_PropertyChanged(object? sender, PropertyChangedEventArgs e)
    {
        var changedEntry = (PropertyMappingEntryModel)sender!;

        if (changedEntry.IsDefault)
        {
            changedEntry.IsDefault = false;
            AddDefaultItem();
        }

        if (e.PropertyName == nameof(PropertyMappingEntryModel.IsValid))
        {
            NotifyPropertyChanged(nameof(IsValid));
        }
    }

    private void PropertyMappingEntryModel_FieldNameChanged(object? sender, NameChangedEventArgs e)
    {
        var changedEntry = (PropertyMappingEntryModel)sender!;

        var sameNameEntries = PropertyMappingEntries
            .Where(entry => entry != changedEntry && !entry.IsDefault && (entry.FieldName == e.OldName || entry.FieldName == e.NewName));
        foreach (var sameNameEntry in sameNameEntries)
        {
            sameNameEntry.NotifyFieldNameValidityChanged();
        }
    }

    private void PropertyMappingEntryModel_PropertyNameChanged(object? sender, NameChangedEventArgs e)
    {
        var changedEntry = (PropertyMappingEntryModel)sender!;

        var sameNameEntries = PropertyMappingEntries
            .Where(entry => entry != changedEntry && !entry.IsDefault && (entry.PropertyName == e.OldName || entry.PropertyName == e.NewName));
        foreach (var sameNameEntry in sameNameEntries)
        {
            sameNameEntry.NotifyPropertyNameValidityChanged();
        }
    }

    private void Save()
    {
        var mappings = new PropertyMapping();
        mappings.AddRange(PropertyMappingEntries
            .Where(c => !c.IsDefault)
            .Select(c => new PropertyMappingEntry(c.FieldName!, c.PropertyName!)));

        SavedPropertyMapping = mappings;
        DialogResult = true;
    }

    private void NotifyPropertyChanged([CallerMemberName] string? propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    public event PropertyChangedEventHandler? PropertyChanged;
}
