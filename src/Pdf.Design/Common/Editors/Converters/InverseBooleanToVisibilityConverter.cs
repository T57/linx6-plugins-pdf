﻿using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Twenty57.Linx.Plugins.Pdf.Design.Common.Editors.Converters;

[ValueConversion(typeof(bool), typeof(Visibility))]
internal class InverseBooleanToVisibilityConverter : IValueConverter
{
    private static readonly BooleanToVisibilityConverter instance;

    static InverseBooleanToVisibilityConverter()
    {
        instance = new();
    }

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return instance.Convert(!(bool)value, targetType, parameter, culture);
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}
