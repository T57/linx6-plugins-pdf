﻿using Twenty57.Linx.Interfaces.Plugin.ComponentProperties;
using Twenty57.Linx.Interfaces.Plugin.Contexts;
using Twenty57.Linx.Interfaces.Plugin.FunctionProvider;
using Twenty57.Linx.Plugins.Pdf.Design.Common;
using Twenty57.Linx.Plugins.Pdf.Shared.Sign;
using Twenty57.Linx.Plugins.Pdf.Shared.Sign.Constants;
using Twenty57.Linx.Sdk.Core;
using Twenty57.Linx.Sdk.Core.LinxProperties;
using Twenty57.Linx.Sdk.Core.Validation;
using Twenty57.Linx.Sdk.UI.Editors;

namespace Twenty57.Linx.Plugins.Pdf.Design.Sign;

public sealed class Designer : Common.Designer
{
    private readonly CertificatePropertyGroup signingCertificatePropertyGroup
        = new(PropertyGroupIds.SigningCertificate, CategoryNames.SigningCertificate);

    public Designer(IFunctionDesignerContext context)
        : base(context)
    {
        Version = Updater.Instance.CurrentVersion;

        Properties.Add(Context.CreateProperty(PropertyIds.SignedAt, typeof(string), ValueUseOption.RuntimeRead, string.Empty));
        Properties.Add(Context.CreateProperty(PropertyIds.Reason, typeof(string), ValueUseOption.RuntimeRead, string.Empty));
        Properties.Add(Context.CreateProperty(PropertyIds.LockAfterSigning, typeof(bool), ValueUseOption.DesignTime, false));
        this.signingCertificatePropertyGroup.AddInitialProperties(Context, Properties);
        Properties.Add(Context.CreateProperty(PropertyIds.SignaturePlacement, typeof(SignaturePlacement), ValueUseOption.DesignTime, SignaturePlacement.Hidden));

        SetPropertyAttributes();
    }

    public Designer(IFunctionData data, IFunctionDesignerContext context)
        : base(data, context)
    { }

    private SignaturePlacement SignaturePlacement => Properties[PropertyIds.SignaturePlacement].GetValue<SignaturePlacement>();

    protected override void SetPropertyAttributes()
    {
        base.SetPropertyAttributes();

        var signedAtProperty = Properties[PropertyIds.SignedAt];
        signedAtProperty.Order = 0;
        signedAtProperty.DisplayName = "Signed at";
        signedAtProperty.Description = "Location where the signing took place.";
        signedAtProperty.Validations.Add(new RequiredValidator());

        var reasonProperty = Properties[PropertyIds.Reason];
        reasonProperty.Order = 1;
        reasonProperty.Description = "Reason for signing the document.";
        reasonProperty.Validations.Add(new RequiredValidator());

        var lockAfterSigningProperty = Properties[PropertyIds.LockAfterSigning];
        lockAfterSigningProperty.Order = 2;
        lockAfterSigningProperty.DisplayName = "Lock document after signing";
        lockAfterSigningProperty.Description = "Lock the document to prevent further changes.";

        this.signingCertificatePropertyGroup.SetPropertyAttributes(Properties);

        var signaturePlacementProperty = Properties[PropertyIds.SignaturePlacement];
        signaturePlacementProperty.Category = "Signature";
        signaturePlacementProperty.Order = 0;
        signaturePlacementProperty.DisplayName = "Signature placement";
        signaturePlacementProperty.Description = "Where to put the signature in the document.";
        signaturePlacementProperty.ValueChanged += SignaturePlacementProperty_ValueChanged;

        var signaturePlacement = SignaturePlacement;
        if (signaturePlacement == SignaturePlacement.FormField)
        {
            var fieldNameProperty = Properties[PropertyIds.FieldName];
            fieldNameProperty.Category = "Signature";
            fieldNameProperty.Order = 1;
            fieldNameProperty.DisplayName = "Field name";
            fieldNameProperty.Description = "Form field name to place the signature in.";
            fieldNameProperty.Validations.Add(new RequiredValidator());
        }
        else if (signaturePlacement == SignaturePlacement.OnPage)
        {
            var positionXProperty = Properties[PropertyIds.PositionX];
            positionXProperty.Category = "Signature";
            positionXProperty.Order = 1;
            positionXProperty.DisplayName = "Position X (mm)";
            positionXProperty.Description = "Distance of the left side of the signature from the left side of the page in millimeters.";

            var positionYProperty = Properties[PropertyIds.PositionY];
            positionYProperty.Category = "Signature";
            positionYProperty.Order = 2;
            positionYProperty.DisplayName = "Position Y (mm)";
            positionYProperty.Description = "Distance of the bottom of the signature from the bottom of the page in millimeters.";

            var widthProperty = Properties[PropertyIds.Width];
            widthProperty.Category = "Signature";
            widthProperty.Order = 3;
            widthProperty.DisplayName = "Width (mm)";
            widthProperty.Description = "Width of the signature box in millimeters.";

            var heightProperty = Properties[PropertyIds.Height];
            heightProperty.Category = "Signature";
            heightProperty.Order = 4;
            heightProperty.DisplayName = "Height (mm)";
            heightProperty.Description = "Height of the signature box in millimeters.";

            var pageProperty = Properties[PropertyIds.Page];
            pageProperty.Category = "Signature";
            pageProperty.Order = 20;
            pageProperty.Description = "Page on which to include the visible signature.";
            pageProperty.Validations.Add(new RangeValidator<int>(1, int.MaxValue));
        }

        if (signaturePlacement != SignaturePlacement.Hidden)
        {
            var backgroundImageProperty = Properties[PropertyIds.BackgroundImage];
            backgroundImageProperty.Category = "Signature";
            backgroundImageProperty.Order = 10;
            backgroundImageProperty.DisplayName = "Background image";
            backgroundImageProperty.Description = "Path to an image file to use as a background for the signature.";
            backgroundImageProperty.Editor = new FilePathEditor();
        }
    }

    private void SignaturePlacementProperty_ValueChanged(object? sender, EventArgs e)
    {
        switch (SignaturePlacement)
        {
            case SignaturePlacement.Hidden:
                {
                    Properties.Remove(PropertyIds.FieldName);
                    Properties.Remove(PropertyIds.PositionX);
                    Properties.Remove(PropertyIds.PositionY);
                    Properties.Remove(PropertyIds.Width);
                    Properties.Remove(PropertyIds.Height);
                    Properties.Remove(PropertyIds.Page);
                    break;
                }
            case SignaturePlacement.FormField:
                {
                    Properties.Add(Context.CreateProperty(PropertyIds.FieldName, typeof(string), ValueUseOption.RuntimeRead, string.Empty));
                    Properties.Remove(PropertyIds.PositionX);
                    Properties.Remove(PropertyIds.PositionY);
                    Properties.Remove(PropertyIds.Width);
                    Properties.Remove(PropertyIds.Height);
                    Properties.Remove(PropertyIds.Page);
                    break;
                }
            case SignaturePlacement.OnPage:
                {
                    Properties.Add(Context.CreateProperty(PropertyIds.PositionX, typeof(int), ValueUseOption.RuntimeRead, 0));
                    Properties.Add(Context.CreateProperty(PropertyIds.PositionY, typeof(int), ValueUseOption.RuntimeRead, 0));
                    Properties.Add(Context.CreateProperty(PropertyIds.Width, typeof(int), ValueUseOption.RuntimeRead, 100));
                    Properties.Add(Context.CreateProperty(PropertyIds.Height, typeof(int), ValueUseOption.RuntimeRead, 50));
                    Properties.Add(Context.CreateProperty(PropertyIds.Page, typeof(int), ValueUseOption.RuntimeRead, 1));
                    Properties.Remove(PropertyIds.FieldName);
                    break;
                }
        }

        if (SignaturePlacement == SignaturePlacement.Hidden)
        {
            Properties.Remove(PropertyIds.BackgroundImage);
        }
        else if (!Properties.Contains(PropertyIds.BackgroundImage))
        {
            Properties.Add(Context.CreateProperty(PropertyIds.BackgroundImage, typeof(string), ValueUseOption.RuntimeRead, string.Empty));
        }
    }
}
