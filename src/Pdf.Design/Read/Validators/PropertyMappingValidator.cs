﻿using Twenty57.Linx.Interfaces.Plugin.Types;
using Twenty57.Linx.Plugins.Pdf.Shared.Read.Constants;
using Twenty57.Linx.Sdk.Core.LinxFunctionProvider;

namespace Twenty57.Linx.Plugins.Pdf.Design.Read.Validators;

internal class PropertyMappingValidator : Common.Validators.PropertyMappingValidator
{
    public PropertyMappingValidator(FunctionDesigner functionDesigner, INameUtility nameUtility)
        : base(functionDesigner, nameUtility) { }

    protected override ITypeReference? GetMappedTypeReference(FunctionDesigner functionDesigner)
    {
        return functionDesigner.Result?.GetProperty(OutputNames.FormData)?.TypeReference;
    }
}
