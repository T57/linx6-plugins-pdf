﻿using System.IO;
using iText.Commons.Exceptions;
using iText.Forms;
using iText.Kernel.Pdf;
using Twenty57.Linx.Sdk.Core.Validation;

namespace Twenty57.Linx.Plugins.Pdf.Design.Read.Validators;

internal class PdfHasFieldsValidator : Validator
{
    protected override bool IsValid(object? value, string name)
    {
        try
        {
            if (HasFields(value as string))
            {
                return true;
            }

            ErrorMessage = "No form fields found in PDF file.";
        }
        catch (ITextException)
        {
            ErrorMessage = "Could not open PDF file.";
        }

        return false;
    }

    private static bool HasFields(string? pdfFile)
    {
        if (!File.Exists(pdfFile))
        {
            return false;
        }

        using var pdfReader = new PdfReader(pdfFile);
        using var pdfDocument = new PdfDocument(pdfReader);

        var form = PdfAcroForm.GetAcroForm(pdfDocument, createIfNotExist: false);
        return form != null && (form.GetXfaForm().IsXfaPresent() || form.GetAllFormFields().Any());
    }
}
