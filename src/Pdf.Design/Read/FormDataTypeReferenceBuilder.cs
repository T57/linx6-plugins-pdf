﻿using System.Collections.Immutable;
using System.Xml.Linq;
using iText.Forms;
using iText.Forms.Xfa;
using iText.Kernel.Pdf;
using Twenty57.Linx.Interfaces.Plugin.Types;
using Twenty57.Linx.Plugins.Pdf.Shared.Common;
using Twenty57.Linx.Sdk.Core;

namespace Twenty57.Linx.Plugins.Pdf.Design.Read;

internal sealed class FormDataTypeReferenceBuilder
{
    private readonly PdfDocument pdfDocument;
    private readonly IReadOnlyDictionary<string, string> propertyNameOverridesLookup;
    private readonly Dictionary<string, string> computedPropertyNameLookup = new();

    private readonly ITypeReferenceFactory typeFactory;
    private readonly INameUtility nameUtility;

    public FormDataTypeReferenceBuilder(
        PdfDocument pdfDocument,
        PropertyMapping propertyMappingOverrides,
        ITypeReferenceFactory typeFactory,
        INameUtility nameUtility)
    {
        this.pdfDocument = pdfDocument;
        this.propertyNameOverridesLookup = propertyMappingOverrides.ToImmutableDictionary(entry => entry.FieldName, entry => entry.PropertyName);
        this.typeFactory = typeFactory;
        this.nameUtility = nameUtility;
    }

    public bool DocumentHasForm => PdfAcroForm.GetAcroForm(this.pdfDocument, createIfNotExist: false) != null;

    public PropertyMapping GetComputedPropertyMapping()
    {
        return new PropertyMapping(this.computedPropertyNameLookup.Select(entry => new PropertyMappingEntry(entry.Key, entry.Value)));
    }

    public ITypeReference BuildTypeReference()
    {
        var form = PdfAcroForm.GetAcroForm(this.pdfDocument, createIfNotExist: false);
        var xfaForm = form.GetXfaForm();
        return xfaForm.IsXfaPresent() ? GetFormDataType(xfaForm) : GetFormDataType(form);
    }

    private ITypeReference GetFormDataType(PdfAcroForm form)
    {
        return this.typeFactory.CreateGenerated(
            form.GetAllFormFields().Keys
            .Select(GetPropertyName)
            .Select(propertyName => this.typeFactory.CreateTypeProperty(propertyName, typeof(string)))
            .ToArray());
    }

    private ITypeReference GetFormDataType(XfaForm form)
    {
        var rootFieldNode = form.GetDatasetsNode()!
            .Element(XName.Get("data", @"http://www.xfa.org/schema/xfa-data/1.0/"))!
            .Elements().Single();

        return GetFormFieldType(rootFieldNode);
    }

    private ITypeReference GetFormFieldType(XElement fieldNode)
    {
        if (!fieldNode.HasElements)
        {
            return this.typeFactory.CreateCompiled(typeof(string));
        }

        return this.typeFactory.CreateGenerated(fieldNode.Elements()
            .Select(e => this.typeFactory.CreateTypeProperty(GetPropertyName(e.Name.LocalName), GetFormFieldType(e)))
            .ToArray());
    }

    private string GetPropertyName(string fieldName)
    {
        if (!this.propertyNameOverridesLookup.TryGetValue(fieldName, out var propertyName))
        {
            propertyName = this.nameUtility.GetValidName(fieldName);
        }

        if (propertyName != fieldName && !this.computedPropertyNameLookup.ContainsKey(fieldName))
        {
            this.computedPropertyNameLookup.Add(fieldName, propertyName);
        }
        return propertyName;
    }
}
