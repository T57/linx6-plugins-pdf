﻿using Twenty57.Linx.Interfaces.Plugin.Types;
using Twenty57.Linx.Plugins.Pdf.Design.Common.Editors.PropertyMappingEditor;
using Twenty57.Linx.Plugins.Pdf.Shared.Read.Constants;
using Twenty57.Linx.Sdk.Core.LinxFunctionProvider;

namespace Twenty57.Linx.Plugins.Pdf.Design.Read.Editors;

internal class PropertyMappingEditor : PropertyEditor
{
    protected override ITypeReference? GetMappedTypeReference(FunctionDesigner designer)
    {
        return designer.Result?.GetProperty(OutputNames.FormData)?.TypeReference;
    }
}
