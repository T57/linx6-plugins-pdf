﻿using System.IO;
using iText.Commons.Exceptions;
using iText.Kernel.Pdf;
using Twenty57.Linx.Interfaces.Plugin.ComponentProperties;
using Twenty57.Linx.Interfaces.Plugin.Contexts;
using Twenty57.Linx.Interfaces.Plugin.FunctionProvider;
using Twenty57.Linx.Interfaces.Plugin.Types;
using Twenty57.Linx.Plugins.Pdf.Design.Read.Editors;
using Twenty57.Linx.Plugins.Pdf.Design.Read.Validators;
using Twenty57.Linx.Plugins.Pdf.Shared.Common;
using Twenty57.Linx.Plugins.Pdf.Shared.Read;
using Twenty57.Linx.Plugins.Pdf.Shared.Read.Constants;
using Twenty57.Linx.Sdk.Core;
using Twenty57.Linx.Sdk.Core.LinxFunctionProvider;
using Twenty57.Linx.Sdk.Core.LinxProperties;
using Twenty57.Linx.Sdk.Core.Validation;
using Twenty57.Linx.Sdk.UI.Editors;

namespace Twenty57.Linx.Plugins.Pdf.Design.Read;

public sealed class Designer : Common.Designer
{
    private PropertyMapping? computedPropertyMapping;

    public Designer(IFunctionDesignerContext context)
        : base(context)
    {
        Version = Updater.Instance.CurrentVersion;

        Properties.Add(Context.CreateProperty(PropertyIds.ReadText, typeof(bool), ValueUseOption.DesignTime, false));
        Properties.Add(Context.CreateProperty(PropertyIds.ReadFormData, typeof(bool), ValueUseOption.DesignTime, false));
        Properties.Add(Context.CreateProperty(PropertyIds.ReadSignature, typeof(bool), ValueUseOption.DesignTime, false));

        SetPropertyAttributes();
        RefreshResult();
    }

    public Designer(IFunctionData data, IFunctionDesignerContext context)
        : base(data, context)
    { }

    protected override bool HasOutputFilePathProperty => false;

    private bool ReadText => Properties[PropertyIds.ReadText].GetValue<bool>();

    private bool ReadFormData => Properties[PropertyIds.ReadFormData].GetValue<bool>();

    private bool ReadSignature => Properties[PropertyIds.ReadSignature].GetValue<bool>();

    private FormDataSpecificationType FormDataSpecificationType => Properties[PropertyIds.FormDataSpecificationType].GetValue<FormDataSpecificationType>();

    public override IFunctionData GetFunctionData()
    {
        var functionData = (FunctionData)base.GetFunctionData();
        if (this.computedPropertyMapping != null)
        {
            functionData.Properties.Add(Context.CreatePropertyData(PropertyIds.ComputedPropertyMapping, typeof(PropertyMapping), ValueUseOption.DesignTime, this.computedPropertyMapping));
        }
        return functionData;
    }

    protected override void InitializeProperties(IReadOnlyDictionary<string, IPropertyData> properties)
    {
        if (properties.TryGetValue(PropertyIds.ComputedPropertyMapping, out var computedPropertyMappingProperty))
        {
            this.computedPropertyMapping = computedPropertyMappingProperty.GetValue<PropertyMapping>();
        }

        foreach (var propertyData in properties.Values.Where(p => p.Id != PropertyIds.ComputedPropertyMapping))
        {
            Properties.Add(Context.CreateProperty(propertyData));
        }

        SetPropertyAttributes();
    }

    protected override void SetPropertyAttributes()
    {
        base.SetPropertyAttributes();

        var readTextProperty = Properties[PropertyIds.ReadText];
        readTextProperty.Category = "Output";
        readTextProperty.Order = 0;
        readTextProperty.DisplayName = "Read text";
        readTextProperty.Description = "Reads the document's text content.";
        readTextProperty.ValueChanged += ReadTextProperty_ValueChanged;

        var readFormDataProperty = Properties[PropertyIds.ReadFormData];
        readFormDataProperty.Category = "Output";
        readFormDataProperty.Order = 1;
        readFormDataProperty.DisplayName = "Read form data";
        readFormDataProperty.Description = "Reads the field values filled into the document's form.";
        readFormDataProperty.ValueChanged += ReadFormDataProperty_ValueChanged;

        var readSignatureProperty = Properties[PropertyIds.ReadSignature];
        readSignatureProperty.Category = "Output";
        readSignatureProperty.Order = 2;
        readSignatureProperty.DisplayName = "Read signature";
        readSignatureProperty.Description = "Reads the document's signature-related information.";
        readSignatureProperty.ValueChanged += (_, _) => RefreshResult();

        if (ReadText)
        {
            var textExtractionStrategyProperty = Properties[PropertyIds.ExtractionStrategy];
            textExtractionStrategyProperty.Category = "Text";
            textExtractionStrategyProperty.DisplayName = "Extraction strategy";
            textExtractionStrategyProperty.Description = "Extraction strategy to use when reading the text from the document.";

            var splitTextProperty = Properties[PropertyIds.SplitText];
            splitTextProperty.Category = "Text";
            splitTextProperty.DisplayName = "Split text";
            splitTextProperty.Description = "Controls how the document text is split.";
            splitTextProperty.ValueChanged += (_, _) => RefreshResult();
        }

        if (ReadFormData)
        {
            var formDataSpecificationTypeProperty = Properties[PropertyIds.FormDataSpecificationType];
            formDataSpecificationTypeProperty.Category = "Form data";
            formDataSpecificationTypeProperty.Order = 0;
            formDataSpecificationTypeProperty.DisplayName = "Return form data as";
            formDataSpecificationTypeProperty.Description = "Controls how the form data is returned.";
            formDataSpecificationTypeProperty.ValueChanged += FormDataSpecificationTypeProperty_ValueChanged;

            var formDataSpecificationType = FormDataSpecificationType;
            switch (formDataSpecificationType)
            {
                case FormDataSpecificationType.CustomType:
                    {
                        var formDataTypeProperty = Properties[PropertyIds.FormDataType];
                        formDataTypeProperty.Category = "Form data";
                        formDataTypeProperty.Order = 1;
                        formDataTypeProperty.DisplayName = "Form data type";
                        formDataTypeProperty.Description = "The expected type for the document's form data.";
                        formDataTypeProperty.Validations.Add(new RequiredValidator());
                        formDataTypeProperty.ValueChanged += (_, _) => RefreshResult();
                        break;
                    }
                case FormDataSpecificationType.Infer:
                    {
                        var samplePdfProperty = Properties[PropertyIds.SamplePdf];
                        samplePdfProperty.Category = "Form data";
                        samplePdfProperty.Order = 1;
                        samplePdfProperty.DisplayName = "Sample PDF";
                        samplePdfProperty.Description = "A sample PDF containing the empty form.";
                        samplePdfProperty.Validations.Add(new RequiredValidator());
                        samplePdfProperty.Validations.Add(new PdfHasFieldsValidator());
                        samplePdfProperty.Editor = new FilePathEditor();
                        samplePdfProperty.ValueChanged += (_, _) => RefreshResult();
                        break;
                    }
            }

            if (formDataSpecificationType != FormDataSpecificationType.List)
            {
                var propertyMappingProperty = Properties[PropertyIds.PropertyMappingOverrides];
                propertyMappingProperty.Category = "Form data";
                propertyMappingProperty.Order = 2;
                propertyMappingProperty.DisplayName = "Property mapping";
                propertyMappingProperty.Description = "Specify the field names that the properties in the form data should map to.";
                propertyMappingProperty.Validations.Add(new PropertyMappingValidator(this, Context.NameUtility));
                propertyMappingProperty.Editor = new PropertyMappingEditor();
                propertyMappingProperty.ValueChanged += (_, _) => RefreshResult();
            }
        }
    }

    private void ReadTextProperty_ValueChanged(object? sender, EventArgs e)
    {
        if (ReadText)
        {
            Properties.Add(Context.CreateProperty(PropertyIds.ExtractionStrategy, typeof(TextExtractionStrategy), ValueUseOption.DesignTime, TextExtractionStrategy.TopToBottom));

            var splitTextProperty = Context.CreateProperty(PropertyIds.SplitText, typeof(TextSplit), ValueUseOption.DesignTime, TextSplit.Never);
            Properties.Add(splitTextProperty);
            splitTextProperty.ValueChanged += (_, _) => RefreshResult();
        }
        else
        {
            Properties.Remove(PropertyIds.ExtractionStrategy);
            Properties.Remove(PropertyIds.SplitText);
            this.computedPropertyMapping = null;
        }

        RefreshResult();
    }

    private void ReadFormDataProperty_ValueChanged(object? sender, EventArgs e)
    {
        if (ReadFormData)
        {
            var returnFormDataAsProperty = Context.CreateProperty(PropertyIds.FormDataSpecificationType, typeof(FormDataSpecificationType), ValueUseOption.DesignTime, FormDataSpecificationType.CustomType);
            Properties.Add(returnFormDataAsProperty);
            returnFormDataAsProperty.ValueChanged += FormDataSpecificationTypeProperty_ValueChanged;

            RefreshFormDataTypeProperties();
        }
        else
        {
            Properties.Remove(PropertyIds.FormDataSpecificationType);
            Properties.Remove(PropertyIds.FormDataType);
            Properties.Remove(PropertyIds.SamplePdf);
            Properties.Remove(PropertyIds.PropertyMappingOverrides);
        }

        RefreshResult();
    }

    private void FormDataSpecificationTypeProperty_ValueChanged(object? sender, EventArgs e)
    {
        RefreshFormDataTypeProperties();
        RefreshResult();
    }

    private void RefreshFormDataTypeProperties()
    {
        var formDataSpecificationType = FormDataSpecificationType;

        if (formDataSpecificationType == FormDataSpecificationType.CustomType)
        {
            var formDataTypeProperty = Context.CreateProperty(PropertyIds.FormDataType, typeof(ITypeReference), ValueUseOption.DesignTime, null);
            Properties.Add(formDataTypeProperty);
            formDataTypeProperty.ValueChanged += (_, _) => RefreshResult();
        }
        else
        {
            Properties.Remove(PropertyIds.FormDataType);
        }

        if (formDataSpecificationType == FormDataSpecificationType.Infer)
        {
            var samplePdfProperty = Context.CreateProperty(PropertyIds.SamplePdf, typeof(string), ValueUseOption.DesignTime, string.Empty);
            Properties.Add(samplePdfProperty);
            samplePdfProperty.ValueChanged += (_, _) => RefreshResult();
        }
        else
        {
            Properties.Remove(PropertyIds.SamplePdf);
        }

        if (formDataSpecificationType == FormDataSpecificationType.List)
        {
            Properties.Remove(PropertyIds.PropertyMappingOverrides);
        }
        else if (!Properties.Contains(PropertyIds.PropertyMappingOverrides))
        {
            var propertyMappingProperty = Context.CreateProperty(PropertyIds.PropertyMappingOverrides, typeof(string), ValueUseOption.DesignTime, null);
            Properties.Add(propertyMappingProperty);
            propertyMappingProperty.ValueChanged += (_, _) => RefreshResult();
        }
    }

    private void RefreshResult()
    {
        var resultBuilder = Context.TypeReferenceFactory.CreateBuilder();
        if (ReadText)
        {
            BuildTextResult(resultBuilder);
        }

        if (ReadFormData)
        {
            BuildFormDataResult(resultBuilder);
        }

        if (ReadSignature)
        {
            BuildSignatureResult(resultBuilder);
        }

        ITypeReference outputReference = resultBuilder.CreateTypeReference();
        Result = outputReference.GetProperties().Any() ? outputReference : null;
    }

    private void BuildTextResult(ITypeReferenceBuilder resultBuilder)
    {
        var textSplitOption = Properties[PropertyIds.SplitText].GetValue<TextSplit>();
        resultBuilder.AddProperty(OutputNames.Text, textSplitOption == TextSplit.Never
            ? Context.TypeReferenceFactory.CreateCompiled(typeof(string))
            : Context.TypeReferenceFactory.CreateList(typeof(string)));
    }

    private void BuildFormDataResult(ITypeReferenceBuilder resultBuilder)
    {
        PropertyMapping GetPropertyMappingOverridesOrDefault()
        {
            return PropertyMapping.TryParse(Properties[PropertyIds.PropertyMappingOverrides].GetValue<string>(), out var propertyMapping)
                ? propertyMapping : new PropertyMapping();
        }

        var formDataSpecificationType = FormDataSpecificationType;
        switch (formDataSpecificationType)
        {
            case FormDataSpecificationType.CustomType:
                var formDataType = Properties[PropertyIds.FormDataType].GetValue<ITypeReference>();
                if (formDataType != null)
                {
                    resultBuilder.AddProperty(OutputNames.FormData, formDataType);
                }
                this.computedPropertyMapping = GetPropertyMappingOverridesOrDefault();
                break;
            case FormDataSpecificationType.Infer:
                var sampleFilePath = Properties[PropertyIds.SamplePdf].GetValue<string>();
                var formDataTypeReference = GetFormDataType(sampleFilePath, GetPropertyMappingOverridesOrDefault(), out this.computedPropertyMapping);
                if (formDataTypeReference != null)
                {
                    resultBuilder.AddProperty(OutputNames.FormData, formDataTypeReference);
                }
                break;
            case FormDataSpecificationType.List:
                resultBuilder.AddProperty(OutputNames.FormDataList, Context.TypeReferenceFactory.CreateList(typeof(KeyValuePair<string, string>)));
                this.computedPropertyMapping = null;
                break;
            default:
                throw new NotSupportedException($"Unhandled FormExtraction: [{formDataSpecificationType}]");
        }
    }

    private void BuildSignatureResult(ITypeReferenceBuilder resultBuilder)
    {
        resultBuilder.AddProperty(OutputNames.Signatures, Context.TypeReferenceFactory.CreateCompiled(typeof(SignatureInfo)));
    }

    private ITypeReference? GetFormDataType(
        string? pdfFilePath,
        PropertyMapping propertyMappingOverrides,
        out PropertyMapping? computedPropertyMapping)
    {
        computedPropertyMapping = null;

        if (!File.Exists(pdfFilePath))
        {
            return null;
        }

        try
        {
            using var pdfReader = new PdfReader(pdfFilePath);
            using var pdfDocument = new PdfDocument(pdfReader);

            var typeReferenceBuilder = new FormDataTypeReferenceBuilder(pdfDocument, propertyMappingOverrides, Context.TypeReferenceFactory, Context.NameUtility);

            if (!typeReferenceBuilder.DocumentHasForm)
            {
                return null;
            }

            var typeReference = typeReferenceBuilder.BuildTypeReference();
            computedPropertyMapping = typeReferenceBuilder.GetComputedPropertyMapping();
            return typeReference;
        }
        catch (ITextException) { return null; }
    }
}
