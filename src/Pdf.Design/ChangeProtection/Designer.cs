﻿using Twenty57.Linx.Interfaces.Plugin.ComponentProperties;
using Twenty57.Linx.Interfaces.Plugin.Contexts;
using Twenty57.Linx.Interfaces.Plugin.FunctionProvider;
using Twenty57.Linx.Plugins.Pdf.Design.Common;
using Twenty57.Linx.Plugins.Pdf.Shared.ChangeProtection;
using Twenty57.Linx.Plugins.Pdf.Shared.ChangeProtection.Constants;
using Twenty57.Linx.Plugins.Pdf.Shared.Common;
using Twenty57.Linx.Sdk.Core;
using Twenty57.Linx.Sdk.Core.LinxProperties;
using Twenty57.Linx.Sdk.Core.Validation;

namespace Twenty57.Linx.Plugins.Pdf.Design.ChangeProtection;

public sealed class Designer : Common.Designer
{
    private const string PermissionsCategory = "Document permissions";
    private const string EncryptionCategory = "Encryption";

    private readonly PdfFilePropertyGroup outputPdfPropertyGroup = new(Shared.Common.Constants.CategoryNames.Output);

    public Designer(IFunctionDesignerContext context)
        : base(context)
    {
        Version = Updater.Instance.CurrentVersion;

        this.outputPdfPropertyGroup.AddInitialProperties(Context, Properties);
        SetPropertyAttributes();
    }

    public Designer(IFunctionData data, IFunctionDesignerContext context)
        : base(data, context) { }

    protected override bool HasOutputFilePathProperty => false;

    private AuthenticationType OutputAuthenticationType => Properties[this.outputPdfPropertyGroup.AuthenticationTypePropertyId].GetValue<AuthenticationType>();

    private bool AddDocumentRestrictions => Properties[PropertyIds.AddDocumentRestrictions].GetValue<bool>();

    protected override void SetPropertyAttributes()
    {
        base.SetPropertyAttributes();

        this.outputPdfPropertyGroup.SetPropertyAttributes(Context, Properties);

        var outputAuthenticationTypeProperty = Properties[this.outputPdfPropertyGroup.AuthenticationTypePropertyId];
        outputAuthenticationTypeProperty.ValueChanged += OutputAuthenticationTypeProperty_ValueChanged;

        var outputAuthenticationType = OutputAuthenticationType;
        if (outputAuthenticationType != AuthenticationType.None)
        {
            var addDocumentRestrictionsProperty = Properties[PropertyIds.AddDocumentRestrictions];
            addDocumentRestrictionsProperty.Category = PermissionsCategory;
            addDocumentRestrictionsProperty.Order = 0;
            addDocumentRestrictionsProperty.DisplayName = "Add document restrictions";
            addDocumentRestrictionsProperty.Description = "Specify restrictions on the PDF document.";
            addDocumentRestrictionsProperty.ValueChanged += AddDocumentRestrictionsProperty_ValueChanged;

            if (AddDocumentRestrictions)
            {
                var allowPrintingProperty = Properties[PropertyIds.AllowPrinting];
                allowPrintingProperty.Category = PermissionsCategory;
                allowPrintingProperty.Order = 1;
                allowPrintingProperty.DisplayName = "Allow printing";
                allowPrintingProperty.Description = "The level of printing allowed on the PDF document.";

                var allowChangesProperty = Properties[PropertyIds.AllowChanges];
                allowChangesProperty.Category = PermissionsCategory;
                allowChangesProperty.Order = 2;
                allowChangesProperty.DisplayName = "Allow changes";
                allowChangesProperty.Description = "The editing actions allowed on the PDF document.";

                var allowCopyingProperty = Properties[PropertyIds.AllowCopying];
                allowCopyingProperty.Category = PermissionsCategory;
                allowCopyingProperty.Order = 3;
                allowCopyingProperty.DisplayName = "Allow copying";
                allowCopyingProperty.Description = "Enable copying of text, images and other content.";
                allowCopyingProperty.ValueChanged += (sender, args) =>
                {
                    bool currentValue = Properties[PropertyIds.AllowCopying].GetValue<bool>();
                    if (currentValue)
                    {
                        Properties[PropertyIds.AllowScreenReaders].Value = true;
                    }
                };

                var allowScreenReadersProperty = Properties[PropertyIds.AllowScreenReaders];
                allowScreenReadersProperty.Category = PermissionsCategory;
                allowScreenReadersProperty.Order = 4;
                allowScreenReadersProperty.DisplayName = "Allow screen readers";
                allowScreenReadersProperty.Description = "Enable text access for screen reader devices for the visually impaired.";

                if (outputAuthenticationType == AuthenticationType.Password)
                {
                    var permissionsPasswordProperty = Properties[PropertyIds.PermissionsPassword];
                    permissionsPasswordProperty.Category = PermissionsCategory;
                    permissionsPasswordProperty.Order = 5;
                    permissionsPasswordProperty.DisplayName = "Permissions password";
                    permissionsPasswordProperty.Description = "Password to override restrictions placed on the PDF document.";
                    permissionsPasswordProperty.Validations.Add(new RequiredValidator());
                }
            }

            var encryptionProperty = Properties[PropertyIds.Encryption];
            encryptionProperty.Category = EncryptionCategory;
            encryptionProperty.Order = this.propertyOrder++;
            encryptionProperty.Description = "Encryption method used to protect the PDF.";

            var dontEncryptMetadataProperty = Properties[PropertyIds.DontEncryptMetadata];
            dontEncryptMetadataProperty.Category = EncryptionCategory;
            dontEncryptMetadataProperty.Order = this.propertyOrder++;
            dontEncryptMetadataProperty.DisplayName = "Don't encrypt metadata";
            dontEncryptMetadataProperty.Description = "Don't encrypt the document metadata.";
        }
    }

    private void OutputAuthenticationTypeProperty_ValueChanged(object? sender, EventArgs e)
    {
        var outputAuthenticationType = OutputAuthenticationType;
        if (outputAuthenticationType != AuthenticationType.None)
        {
            if (!Properties.Contains(PropertyIds.AddDocumentRestrictions))
            {
                var addDocumentRestrictionsProperty = Context.CreateProperty(PropertyIds.AddDocumentRestrictions, typeof(bool), ValueUseOption.DesignTime, false);
                Properties.Add(addDocumentRestrictionsProperty);
                addDocumentRestrictionsProperty.ValueChanged += AddDocumentRestrictionsProperty_ValueChanged;

                Properties.Add(Context.CreateProperty(PropertyIds.Encryption, typeof(Encryption), ValueUseOption.DesignTime, Encryption.AES128));
                Properties.Add(Context.CreateProperty(PropertyIds.DontEncryptMetadata, typeof(bool), ValueUseOption.DesignTime, false));
            }

            RefreshPermissionsPasswordProperty();
        }
        else
        {
            Properties.Remove(PropertyIds.AddDocumentRestrictions);
            Properties.Remove(PropertyIds.Encryption);
            Properties.Remove(PropertyIds.DontEncryptMetadata);
            Properties.Remove(PropertyIds.AllowPrinting);
            Properties.Remove(PropertyIds.AllowChanges);
            Properties.Remove(PropertyIds.AllowCopying);
            Properties.Remove(PropertyIds.AllowScreenReaders);
            Properties.Remove(PropertyIds.PermissionsPassword);
        }
    }

    private void AddDocumentRestrictionsProperty_ValueChanged(object? sender, EventArgs e)
    {
        if (AddDocumentRestrictions)
        {
            Properties.Add(Context.CreateProperty(PropertyIds.AllowPrinting, typeof(Printing), ValueUseOption.DesignTime, Printing.None));
            Properties.Add(Context.CreateProperty(PropertyIds.AllowChanges, typeof(Changes), ValueUseOption.DesignTime, Changes.None));
            Properties.Add(Context.CreateProperty(PropertyIds.AllowCopying, typeof(bool), ValueUseOption.DesignTime, false));
            Properties.Add(Context.CreateProperty(PropertyIds.AllowScreenReaders, typeof(bool), ValueUseOption.DesignTime, false));
        }
        else
        {
            Properties.Remove(PropertyIds.AllowPrinting);
            Properties.Remove(PropertyIds.AllowChanges);
            Properties.Remove(PropertyIds.AllowCopying);
            Properties.Remove(PropertyIds.AllowScreenReaders);
        }

        RefreshPermissionsPasswordProperty();
    }

    private void RefreshPermissionsPasswordProperty()
    {
        if (AddDocumentRestrictions && OutputAuthenticationType == AuthenticationType.Password)
        {
            Properties.Add(Context.CreateProperty(PropertyIds.PermissionsPassword, typeof(string), ValueUseOption.RuntimeRead, string.Empty));
        }
        else
        {
            Properties.Remove(PropertyIds.PermissionsPassword);
        }
    }
}
