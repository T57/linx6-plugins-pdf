﻿using Twenty57.Linx.Interfaces.Plugin.ComponentProperties;
using Twenty57.Linx.Interfaces.Plugin.Contexts;
using Twenty57.Linx.Interfaces.Plugin.FunctionProvider;
using Twenty57.Linx.Plugins.Pdf.Shared.Split;
using Twenty57.Linx.Plugins.Pdf.Shared.Split.Constants;
using Twenty57.Linx.Sdk.Core;
using Twenty57.Linx.Sdk.Core.LinxProperties;

namespace Twenty57.Linx.Plugins.Pdf.Design.Split;

public sealed class Designer : Common.Designer
{
    public Designer(IFunctionDesignerContext context)
        : base(context)
    {
        Version = Updater.Instance.CurrentVersion;

        Properties.Add(Context.CreateProperty(PropertyIds.LoopResults, typeof(bool), ValueUseOption.DesignTime, false));
        SetPropertyAttributes();
        RefreshResult();
    }

    public Designer(IFunctionData data, IFunctionDesignerContext context)
        : base(data, context) { }

    protected override void SetPropertyAttributes()
    {
        base.SetPropertyAttributes();

        var loopResultsProperty = Properties[PropertyIds.LoopResults];
        loopResultsProperty.DisplayName = "Loop results";
        loopResultsProperty.Description = "Loop through the generated file names.";
        loopResultsProperty.ValueChanged += (_, _) => RefreshResult();
    }

    private void RefreshResult()
    {
        Result = null;
        ExecutionPaths.Clear();

        var resultBuilder = Context.TypeReferenceFactory.CreateBuilder();

        bool loopResultsValue = Properties[PropertyIds.LoopResults].GetValue<bool>();
        if (loopResultsValue)
        {
            ExecutionPaths.Add(ExecutionPathNames.PageFiles, ExecutionPathNames.PageFiles, Context.TypeReferenceFactory.CreateCompiled(typeof(string)));
        }
        else
        {
            resultBuilder.AddProperty(OutputNames.PageFiles, Context.TypeReferenceFactory.CreateList(typeof(string)));
        }

        resultBuilder.AddProperty(OutputNames.NumberOfPages, typeof(int));
        Result = resultBuilder.CreateTypeReference();
    }
}
