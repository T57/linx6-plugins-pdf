﻿using Twenty57.Linx.Interfaces.Plugin.ComponentProperties;
using Twenty57.Linx.Interfaces.Plugin.Contexts;
using Twenty57.Linx.Interfaces.Plugin.FunctionProvider;
using Twenty57.Linx.Plugins.Pdf.Shared.Concatenate.Constants;
using Twenty57.Linx.Sdk.Core;
using Twenty57.Linx.Sdk.Core.LinxFunctionProvider;
using Twenty57.Linx.Sdk.Core.Validation;
using Twenty57.Linx.Sdk.UI.Editors;

namespace Twenty57.Linx.Plugins.Pdf.Design.Concatenate;

public sealed class Designer : FunctionDesigner
{
    public Designer(IFunctionDesignerContext context)
        : base(context)
    {
        Properties.Add(Context.CreateProperty(PropertyIds.InputFiles, Context.TypeReferenceFactory.CreateList(typeof(string)), ValueUseOption.RuntimeRead, null));
        Properties.Add(Context.CreateProperty(PropertyIds.OutputFilePath, typeof(string), ValueUseOption.RuntimeRead, string.Empty));
    }

    public Designer(IFunctionData data, IFunctionDesignerContext context)
        : base(data, context)
    { }

    protected override void InitializeProperties(IReadOnlyDictionary<string, IPropertyData> properties)
    {
        base.InitializeProperties(properties);

        var inputFilesProperty = Properties[PropertyIds.InputFiles];
        inputFilesProperty.Order = 1;
        inputFilesProperty.DisplayName = "Input file paths";
        inputFilesProperty.Description = "List of PDF files to concatenate.";
        inputFilesProperty.Validations.Add(new RequiredValidator());

        var outputFilePathProperty = Properties[PropertyIds.OutputFilePath];
        outputFilePathProperty.Order = 2;
        outputFilePathProperty.DisplayName = "Output file path";
        outputFilePathProperty.Description = "Path of the PDF file to write to.";
        outputFilePathProperty.Editor = new FilePathEditor();
        outputFilePathProperty.Validations.Add(new RequiredValidator());
    }
}
