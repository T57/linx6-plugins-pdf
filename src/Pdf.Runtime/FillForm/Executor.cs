﻿using System.Xml.Linq;
using System.Xml.Serialization;
using iText.Forms;
using iText.Forms.Fields;
using iText.Forms.Fields.Properties;
using iText.Forms.Xfa;
using iText.Kernel.Pdf;
using Twenty57.Linx.Interfaces.Plugin.Runtime;

namespace Twenty57.Linx.Plugins.Pdf.Runtime.FillForm;

public static class Executor
{
    public static void FillForm(
        PdfReader inputPdfReader,
        string outputFilePath,
        object formData,
        IReadOnlyDictionary<string, string> fieldNameLookup,
        IRunContext runContext)
    {
        ArgumentException.ThrowIfNullOrEmpty(outputFilePath);
        ArgumentNullException.ThrowIfNull(formData);

        using var outputStream = new FileStream(outputFilePath, FileMode.Create);
        using var pdfWriter = new PdfWriter(outputStream);
        using var outputPdfDocument = new PdfDocument(inputPdfReader, pdfWriter, new StampingProperties().UseAppendMode());

        var form = PdfAcroForm.GetAcroForm(outputPdfDocument, createIfNotExist: false)
            ?? throw new Exception("The input document does not contain a form.");

        runContext.Log("Adding form data to {0}.", outputFilePath);

        var xfaForm = form.GetXfaForm();
        if (xfaForm.IsXfaPresent())
        {
            FillXfaForm(xfaForm, formData, fieldNameLookup, runContext);
            xfaForm.Write(outputPdfDocument);
        }
        else
        {
            FillAcroForm(form, formData, fieldNameLookup, runContext);
        }
    }

    private static void FillAcroForm(PdfAcroForm form, object formData, IReadOnlyDictionary<string, string> fieldNameLookup, IRunContext runContext)
    {
        var formFields = form.GetAllFormFields();
        foreach (var property in formData.GetType().GetProperties())
        {
            if (!fieldNameLookup.TryGetValue(property.Name, out var fieldName))
            {
                fieldName = property.Name;
            }

            if (!formFields.TryGetValue(fieldName, out var field))
            {
                runContext.Log("No field with name {0} was found.", fieldName);
            }
            else
            {
                string stringValue;
                if (field is PdfButtonFormField)
                {
                    field.SetCheckType(CheckBoxType.CHECK);
                    var appearanceStates = field.GetAppearanceStates();
                    bool value = (bool)Convert.ChangeType(property.GetValue(formData)!, typeof(bool));
                    stringValue = (appearanceStates == null) || (appearanceStates.Length == 0)
                        ? (value ? "Yes" : "No")
                        : appearanceStates[value ? 1 : 0];
                }
                else
                {
                    stringValue = property.GetValue(formData)?.ToString() ?? string.Empty;
                }

                runContext.Log("Setting field {0} to {1}.", fieldName, stringValue);
                field.SetValue(stringValue);
            }
        }
    }

    private static void FillXfaForm(XfaForm form, object formData, IReadOnlyDictionary<string, string> fieldNameLookup, IRunContext runContext)
    {
        static XDocument SerializeToXml(object formData)
        {
            var xmlDocument = new XDocument();
            using var xmlWriter = xmlDocument.CreateWriter();
            var xmlSerializer = new XmlSerializer(formData.GetType());
            var xmlNamespaces = new XmlSerializerNamespaces();
            xmlNamespaces.Add("", "");
            xmlSerializer.Serialize(xmlWriter, formData, xmlNamespaces);
            return xmlDocument;
        }

        XDocument formDataAsXml = SerializeToXml(formData);

        if (fieldNameLookup.Any())
        {
            ApplyPropertyMapping(formDataAsXml.Root!, fieldNameLookup);
        }

        ConvertValues(formDataAsXml.Root!);

        runContext.Log($"Filling in the form using the following XML content:{Environment.NewLine}{{0}}", formDataAsXml);

        using var xmlReader = formDataAsXml.CreateReader();
        form.FillXfaForm(xmlReader);
    }

    private static void ApplyPropertyMapping(XElement element, IReadOnlyDictionary<string, string> propertyNameLookup)
    {
        if (propertyNameLookup.TryGetValue(element.Name.LocalName, out var mappedFieldName))
        {
            element.Name = XName.Get(mappedFieldName, element.Name.NamespaceName);
        }

        foreach (var childElement in element.Elements())
        {
            ApplyPropertyMapping(childElement, propertyNameLookup);
        }
    }

    private static void ConvertValues(XElement element)
    {
        switch (element.Value)
        {
            case "true":
                element.Value = "1";
                break;
            case "false":
                element.Value = "0";
                break;
        }

        foreach (var childElement in element.Elements())
        {
            ConvertValues(childElement);
        }
    }
}
