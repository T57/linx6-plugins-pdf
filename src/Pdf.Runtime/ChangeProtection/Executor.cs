﻿using System.IO;
using iText.Kernel.Pdf;
using Twenty57.Linx.Interfaces.Plugin.Runtime;

namespace Twenty57.Linx.Plugins.Pdf.Runtime.ChangeProtection;

public static class Executor
{
    public static void ApplyProtection(
        PdfReader inputPdfReader,
        string outputFilePath,
        WriterProperties writerProperties,
        IRunContext runContext)
    {
        if (string.IsNullOrEmpty(outputFilePath))
        {
            throw new ArgumentNullException(nameof(outputFilePath));
        }

        runContext.Log("Protecting {0}", outputFilePath);

        using var outputStream = new FileStream(outputFilePath, FileMode.Create);
        using var pdfWriter = new PdfWriter(outputStream, writerProperties);
        using var outputPdfDocument = new PdfDocument(inputPdfReader, pdfWriter);
    }
}
