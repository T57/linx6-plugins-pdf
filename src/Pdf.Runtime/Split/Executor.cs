﻿using System.IO;
using iText.Forms;
using iText.Kernel.Pdf;
using Twenty57.Linx.Interfaces.Plugin.Runtime;

namespace Twenty57.Linx.Plugins.Pdf.Runtime.Split;

public static class Executor
{
    public static IEnumerable<string> Split(PdfDocument inputPdfDocument, string unnumberedOutputFilePath, IRunContext runContext)
    {
        if (string.IsNullOrEmpty(unnumberedOutputFilePath))
        {
            throw new ArgumentNullException(nameof(unnumberedOutputFilePath));
        }

        var form = PdfAcroForm.GetAcroForm(inputPdfDocument, createIfNotExist: false);
        if ((form != null) && form.GetXfaForm().IsXfaPresent())
        {
            throw new NotSupportedException("Splitting is not supported for XFA documents.");
        }

        string outputFilePathBase = Path.Combine(Path.GetDirectoryName(unnumberedOutputFilePath)!, Path.GetFileNameWithoutExtension(unnumberedOutputFilePath));
        for (int pageNumber = 1; pageNumber <= inputPdfDocument.GetNumberOfPages(); pageNumber++)
        {
            string outputFilePath = $"{outputFilePathBase}_{pageNumber}.pdf";

            runContext.Log("Copying page {0} to {1}", pageNumber, outputFilePath);

            using (var outputStream = new FileStream(outputFilePath, FileMode.Create))
            using (var outputPdfWriter = new PdfWriter(outputStream))
            using (var outputPdfDocument = new PdfDocument(outputPdfWriter))
            {
                inputPdfDocument.CopyPagesTo(pageNumber, pageNumber, outputPdfDocument);
            }

            yield return outputFilePath;
        }
    }
}
