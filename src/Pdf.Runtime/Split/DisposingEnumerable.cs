﻿using System.Collections;

namespace Twenty57.Linx.Plugins.Pdf.Runtime.Split;

public sealed class DisposingEnumerable<T> : IEnumerable<T>
{
    private readonly IEnumerable<T> innerEnumerable;
    private readonly IDisposable objectToDispose;

    public DisposingEnumerable(IEnumerable<T> innerEnumerable, IDisposable objectToDispose)
    {
        this.innerEnumerable = innerEnumerable;
        this.objectToDispose = objectToDispose;
    }

    public IEnumerator<T> GetEnumerator()
    {
        return new DisposingEnumerator<T>(this.innerEnumerable.GetEnumerator(), this.objectToDispose);
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
}

internal sealed class DisposingEnumerator<T> : IEnumerator<T>, IDisposable
{
    private readonly IEnumerator<T> innerEnumerator;
    private readonly IDisposable objectToDispose;

    public DisposingEnumerator(IEnumerator<T> innerEnumerator, IDisposable objectToDispose)
    {
        this.innerEnumerator = innerEnumerator;
        this.objectToDispose = objectToDispose;
    }

    public T Current => this.innerEnumerator.Current;

    object? IEnumerator.Current => Current;

    public void Dispose()
    {
        this.objectToDispose.Dispose();
    }

    public bool MoveNext()
    {
        return this.innerEnumerator.MoveNext();
    }

    public void Reset()
    {
        this.innerEnumerator.Reset();
    }
}
