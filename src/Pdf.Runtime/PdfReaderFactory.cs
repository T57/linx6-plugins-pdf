﻿using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using iText.Bouncycastle.Crypto;
using iText.Bouncycastle.X509;
using iText.Kernel.Pdf;
using Org.BouncyCastle.Security;
using Twenty57.Linx.Interfaces.Plugin.Runtime;

namespace Twenty57.Linx.Plugins.Pdf.Runtime;

public static class PdfReaderFactory
{
    public static PdfReader Create(string filePath, IRunContext context)
    {
        CheckAndLogFilePath(filePath, context);
        return new PdfReader(filePath);
    }

    public static PdfReader Create(string filePath, string password, IRunContext context)
    {
        CheckAndLogFilePath(filePath, context);
        var readerProperties = new ReaderProperties().SetPassword(Encoding.UTF8.GetBytes(password));
        return new PdfReader(filePath, readerProperties);
    }

    public static PdfReader Create(string filePath, Func<X509Certificate2> createCertificate, IRunContext context)
    {
        CheckAndLogFilePath(filePath, context);

        using var certificate = createCertificate();
        if (!certificate.HasPrivateKey)
        {
            throw new NotSupportedException("Certificate must have a private key.");
        }

        var bouncyCertificate = new X509CertificateBC(DotNetUtilities.FromX509Certificate(certificate));
        var keyPair = DotNetUtilities.GetKeyPair(GetCertificatePrivateKey(certificate));
        var readerProperties = new ReaderProperties()
            .SetPublicKeySecurityParams(bouncyCertificate, new PrivateKeyBC(keyPair.Private));
        return new PdfReader(filePath, readerProperties);
    }

    private static AsymmetricAlgorithm GetCertificatePrivateKey(X509Certificate2 certificate)
    {
        const string RSA = "1.2.840.113549.1.1.1";
        const string DSA = "1.2.840.10040.4.1";
        const string ECC = "1.2.840.10045.2.1";

        return certificate.PublicKey.Oid.Value switch
        {
            RSA => certificate.GetRSAPrivateKey()!,
            DSA => certificate.GetDSAPrivateKey()!,
            ECC => certificate.GetECDsaPrivateKey()!,
            _ => throw new Exception("Unsupported key-pair type: " + certificate.PublicKey.Oid.FriendlyName)
        };
    }

    private static void CheckAndLogFilePath(string filePath, IRunContext context)
    {
        if (string.IsNullOrEmpty(filePath))
        {
            throw new ArgumentNullException(nameof(filePath));
        }
        context.Log("Opening file {0}.", filePath);
    }
}
