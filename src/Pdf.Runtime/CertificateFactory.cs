﻿using System.Security.Cryptography.X509Certificates;

namespace Twenty57.Linx.Plugins.Pdf.Runtime;

public static class CertificateFactory
{
    public static X509Certificate2 CreateFromFile(string certificateFilePath, string certificatePassword)
    {
        return new X509Certificate2(certificateFilePath, certificatePassword, X509KeyStorageFlags.Exportable);
    }
}
