﻿using System.Security.Cryptography.X509Certificates;
using iText.Bouncycastle.Crypto;
using iText.Bouncycastle.X509;
using iText.Forms;
using iText.Forms.Form.Element;
using iText.IO.Image;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Signatures;
using Org.BouncyCastle.Security;
using Twenty57.Linx.Interfaces.Plugin.Runtime;
using Twenty57.Linx.Plugins.Pdf.Shared.Sign;

namespace Twenty57.Linx.Plugins.Pdf.Runtime.Sign;

public static class Executor
{
    public static void Sign(
        PdfReader inputPdfReader,
        X509Certificate2 signingCertificate,
        string outputFilePath,
        string signedAt,
        string reason,
        bool lockAfterSigning,
        SignaturePlacement signaturePlacement,
        string fieldName,
        int? positionX, int? positionY, int? width, int? height,
        int? signaturePageNumber,
        string backgroundImageFilePath,
        IRunContext runContext)
    {
        ArgumentException.ThrowIfNullOrEmpty(nameof(outputFilePath));

        if (!signingCertificate.HasPrivateKey)
        {
            throw new Exception("Signing certificate must have a private key.");
        }

        runContext.Log("Adding signature to {0}", outputFilePath);

        var privateKey = new PrivateKeyBC(DotNetUtilities.GetKeyPair(signingCertificate.GetRSAPrivateKey()).Private);
        var externalSignature = new PrivateKeySignature(privateKey, "SHA-256");
        using var certChain = new X509Chain();
        certChain.Build(signingCertificate);
        var bouncyCertChain = certChain.ChainElements
            .Cast<X509ChainElement>()
            .Select(ce => new X509CertificateBC(DotNetUtilities.FromX509Certificate(ce.Certificate)))
            .ToArray();

        using var outputStream = new FileStream(outputFilePath, FileMode.Create);

        var pdfSigner = new PdfSigner(inputPdfReader, outputStream, new StampingProperties().UseAppendMode());
        pdfSigner.SetSignDate(DateTime.Now);
        if (lockAfterSigning)
        {
            pdfSigner.SetCertificationLevel(PdfSigner.CERTIFIED_NO_CHANGES_ALLOWED);
        }

        if (signaturePlacement == SignaturePlacement.FormField)
        {
            SetFormFieldToSign(pdfSigner, fieldName);
        }

        pdfSigner.SetLocation(signedAt);
        pdfSigner.SetReason(reason);
        if (!string.IsNullOrEmpty(backgroundImageFilePath))
        {
            var signatureAppearance = new SignatureFieldAppearance(pdfSigner.GetFieldName())
                .SetContent(ImageDataFactory.Create(backgroundImageFilePath));
            pdfSigner.SetSignatureAppearance(signatureAppearance);
        }

        if (signaturePlacement == SignaturePlacement.OnPage)
        {
            pdfSigner.SetPageRect(new Rectangle(
                MillimetersToPoints(positionX!.Value),
                MillimetersToPoints(positionY!.Value),
                MillimetersToPoints(width!.Value),
                MillimetersToPoints(height!.Value)));
            pdfSigner.SetPageNumber(signaturePageNumber!.Value);

            static float MillimetersToPoints(int millimeters) => millimeters * 72 / 25.4f;
        }

        pdfSigner.SignDetached(externalSignature, bouncyCertChain, null, null, null, 0, PdfSigner.CryptoStandard.CMS);
    }

    private static void SetFormFieldToSign(PdfSigner pdfSigner, string fieldName)
    {
        if (string.IsNullOrEmpty(fieldName))
        {
            throw new ArgumentNullException(nameof(fieldName));
        }

        var form = PdfAcroForm.GetAcroForm(pdfSigner.GetDocument(), createIfNotExist: false);
        var xfaForm = form?.GetXfaForm() ?? throw new Exception("The input document does not contain a form.");
        if (xfaForm.IsXfaPresent())
        {
            throw new NotSupportedException("Field signatures are not supported for XFA documents.");
        }
        if (form.GetField(fieldName) == null)
        {
            throw new Exception($"The form does not have a field named [{fieldName}].");
        }
        pdfSigner.SetFieldName(fieldName);
    }
}
