﻿using System.Text;
using iText.Forms;
using iText.Kernel.Pdf;
using iText.Kernel.Utils;
using Twenty57.Linx.Interfaces.Plugin.Runtime;

namespace Twenty57.Linx.Plugins.Pdf.Runtime.Concatenate;

public static class Executor
{
    public static void Concatenate(IEnumerable<string> inputFilePaths, string outputFilePath, IRunContext runContext)
    {
        if (string.IsNullOrEmpty(outputFilePath))
        {
            throw new ArgumentNullException(nameof(outputFilePath));
        }

        using var outputStream = new FileStream(outputFilePath, FileMode.Create);
        using var pdfWriter = new PdfWriter(outputStream);
        using var outputPdfDocument = new PdfDocument(pdfWriter);
        var pdfMerger = new PdfMerger(outputPdfDocument);

        runContext.Log("Concatenating files to {0}.", outputFilePath);

        foreach (string inputFilePath in inputFilePaths)
        {
            runContext.Log("Adding file {0}.", inputFilePath);

            using var inputReader = new PdfReader(inputFilePath);
            using var inputPdfDocument = new PdfDocument(inputReader);

            var form = PdfAcroForm.GetAcroForm(inputPdfDocument, false);
            if ((form != null) && form.GetXfaForm().IsXfaPresent())
            {
                throw new NotSupportedException("The Concatenate function cannot be used on an XFA document.");
            }

            pdfMerger.Merge(inputPdfDocument, 1, inputPdfDocument.GetNumberOfPages());

            var javaScriptEntries = GetJavaScriptEntries(inputPdfDocument);
            if (javaScriptEntries.Any())
            {
                string inputFilePrefix = Convert.ToBase64String(Encoding.UTF8.GetBytes(inputFilePath)) + '_';
                PdfNameTree outputJavaScriptNameTree = outputPdfDocument.GetCatalog().GetNameTree(PdfName.JavaScript);
                foreach (var (entryName, javaScriptObject) in javaScriptEntries)
                {
                    outputJavaScriptNameTree.AddEntry(inputFilePrefix + entryName, javaScriptObject.CopyTo(outputPdfDocument));
                }
            }
        }
    }

    private static IEnumerable<(string Name, PdfObject JavaScriptObject)> GetJavaScriptEntries(PdfDocument pdfDocument)
    {
        return pdfDocument.GetCatalog().GetNameTree(PdfName.JavaScript).GetNames()
            .Select(entry => (entry.Key.GetValue(), entry.Value));
    }
}
