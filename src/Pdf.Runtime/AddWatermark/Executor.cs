﻿using System.Text.RegularExpressions;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Kernel.Pdf.Xobject;
using Twenty57.Linx.Interfaces.Plugin.Runtime;
using Twenty57.Linx.Plugins.Pdf.Shared.AddWatermark;

namespace Twenty57.Linx.Plugins.Pdf.Runtime.AddWatermark;

public static class Executor
{
    private static readonly Regex RangeRegex;

    static Executor()
    {
        RangeRegex = new Regex(@"^((?<single>\d+)|((?<from>\d+)?(-|\.\.)(?<to>\d+)?))$", RegexOptions.Compiled);
    }

    public static void AddWatermark(
        PdfReader inputPdfReader,
        string outputFilePath,
        Func<PdfReader> getWatermarkPdfReader,
        string watermarkPages,
        WatermarkPosition watermarkPosition,
        IRunContext runContext)
    {
        if (string.IsNullOrEmpty(outputFilePath))
        {
            throw new ArgumentNullException(nameof(outputFilePath));
        }

        using var outputStream = new FileStream(outputFilePath, FileMode.Create);
        using var pdfWriter = new PdfWriter(outputStream);
        using var outputPdfDocument = new PdfDocument(inputPdfReader, pdfWriter);

        PdfXObject watermark;
        using (var watermarkPdfReader = getWatermarkPdfReader())
        {
            var watermarkDocument = new PdfDocument(watermarkPdfReader);
            watermark = watermarkDocument.GetPage(1).CopyAsFormXObject(outputPdfDocument);
        }

        runContext.Log("Adding watermark to {0}.", outputFilePath);

        var pageNumbers = GetPageNumbers(watermarkPages, outputPdfDocument.GetNumberOfPages());
        foreach (int pageNumber in pageNumbers.OrderBy(p => p).Distinct())
        {
            runContext.Log("Applying watermark to page number {0}.", pageNumber);
            var page = outputPdfDocument.GetPage(pageNumber);
            var canvas = new PdfCanvas(
                watermarkPosition == WatermarkPosition.Above ? page.NewContentStreamAfter() : page.NewContentStreamBefore(),
                page.GetResources(),
                outputPdfDocument);
            canvas.AddXObject(watermark);
        }
    }

    private static IEnumerable<int> GetPageNumbers(string pageRangeExpression, int documentPageCount)
    {
        if (string.IsNullOrEmpty(pageRangeExpression))
        {
            return Enumerable.Range(1, documentPageCount);
        }

        return pageRangeExpression
            .Split(new[] { ' ', ';', ',' }, StringSplitOptions.RemoveEmptyEntries)
            .SelectMany(rangePart => GetPageNumbersForRangePart(rangePart, documentPageCount));
    }

    private static IEnumerable<int> GetPageNumbersForRangePart(string rangePart, int documentPageCount)
    {
        var rangeMatch = RangeRegex.Match(rangePart);
        if (!rangeMatch.Success)
        {
            return Enumerable.Empty<int>();
        }

        var singleGroup = rangeMatch.Groups["single"];
        if (singleGroup.Success)
        {
            return Enumerable.Repeat(int.Parse(singleGroup.Value), 1);
        }

        var fromGroup = rangeMatch.Groups["from"];
        int fromPageNumber = fromGroup.Success ? int.Parse(fromGroup.Value) : 1;
        var toGroup = rangeMatch.Groups["to"];
        int toPageNumber = toGroup.Success ? int.Parse(toGroup.Value) : documentPageCount;
        return Enumerable.Range(fromPageNumber, toPageNumber - fromPageNumber + 1);
    }
}
