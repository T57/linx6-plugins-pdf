﻿using Twenty57.Linx.Interfaces.Plugin.FunctionProvider;
using Twenty57.Linx.Plugins.Pdf.Shared.Common.Constants;

namespace Twenty57.Linx.Plugins.Pdf.Runtime;

public sealed class CertificateInfo
{
    public CertificateInfo(string propertyGroupId, IFunctionBuilder functionBuilder)
    {
        FilePathParameterName = functionBuilder.GetParamName(propertyGroupId + PropertyIds.CertificateFilePathSuffix);
        FilePasswordParameterName = functionBuilder.GetParamName(propertyGroupId + PropertyIds.CertificateFilePasswordSuffix);
    }

    public string FilePathParameterName { get; }

    public string FilePasswordParameterName { get; }
}
