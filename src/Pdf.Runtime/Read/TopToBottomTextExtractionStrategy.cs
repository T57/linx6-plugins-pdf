﻿using System.Text;
using iText.Kernel.Geom;
using iText.Kernel.Pdf.Canvas.Parser;
using iText.Kernel.Pdf.Canvas.Parser.Data;
using iText.Kernel.Pdf.Canvas.Parser.Listener;

namespace Twenty57.Linx.Plugins.Pdf.Runtime.Read;

// Adapted from http://cjhaas.com/blog/2013/03/13/itextsharp-slightly-smarter-text-extraction-strategy/
public class TopToBottomTextExtractionStrategy : ITextExtractionStrategy
{
    private readonly SortedDictionary<int, StringBuilder> results = new();

    private Vector lastStart = new(0, 0, 0);
    private Vector lastEnd = new(0, 0, 0);

    public string GetResultantText()
    {
        return string.Join(Environment.NewLine, this.results.Values);
    }

    public ICollection<EventType> GetSupportedEvents()
    {
        return new List<EventType> { EventType.RENDER_TEXT };
    }

    public void EventOccurred(IEventData data, EventType type)
    {
        var renderInfo = (TextRenderInfo)data;

        LineSegment segment = renderInfo.GetBaseline();
        Vector start = segment.GetStartPoint();
        Vector end = segment.GetEndPoint();
        int currentLineKey = (int)start.Get(1);

        bool isFirstRender = this.results.Count == 0;
        if (!isFirstRender)
        {
            Vector x0 = start;
            Vector x1 = this.lastStart;
            Vector x2 = this.lastEnd;

            float distance = x2.Subtract(x1).Cross(x1.Subtract(x0)).LengthSquared() / x2.Subtract(x1).LengthSquared();

            float sameLineThreshold = 1f;
            if (distance <= sameLineThreshold)
            {
                currentLineKey = (int)this.lastStart.Get(1);
            }
        }
        currentLineKey *= -1;

        if (!this.results.ContainsKey(currentLineKey))
        {
            this.results.Add(currentLineKey, new StringBuilder());
        }

        if (!isFirstRender &&
            this.results[currentLineKey].Length != 0 &&
            !this.results[currentLineKey].ToString().EndsWith(" ") &&
            renderInfo.GetText().Length > 0 &&
            !renderInfo.GetText().StartsWith(" "))
        {
            float spacing = this.lastEnd.Subtract(start).Length();
            if (spacing > renderInfo.GetSingleSpaceWidth() / 2f)
            {
                this.results[currentLineKey].Append(" ");
            }
        }

        this.results[currentLineKey].Append(renderInfo.GetText());

        this.lastStart = start;
        this.lastEnd = end;
    }
}
