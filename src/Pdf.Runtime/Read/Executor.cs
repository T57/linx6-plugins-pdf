﻿using System.Security.Cryptography.X509Certificates;
using System.Xml.Linq;
using System.Xml.Serialization;
using iText.Bouncycastle.X509;
using iText.Forms;
using iText.Forms.Xfa;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas.Parser;
using iText.Kernel.Pdf.Canvas.Parser.Listener;
using iText.Signatures;
using Org.BouncyCastle.Security;
using Twenty57.Linx.Interfaces.Plugin.Runtime;
using Twenty57.Linx.Plugins.Pdf.Shared.Read;

namespace Twenty57.Linx.Plugins.Pdf.Runtime.Read;

public static class Executor
{
    public static IEnumerable<string> ReadPageTexts(PdfDocument pdfDocument, Func<ITextExtractionStrategy> getTextExtractionStrategy)
    {
        return Enumerable.Range(1, pdfDocument.GetNumberOfPages())
            .Select(pageNumber => PdfTextExtractor.GetTextFromPage(pdfDocument.GetPage(pageNumber), getTextExtractionStrategy()));
    }

    public static IEnumerable<KeyValuePair<string, string>> ReadFormData(PdfDocument pdfDocument)
    {
        var form = PdfAcroForm.GetAcroForm(pdfDocument, createIfNotExist: false);
        var xfaForm = form?.GetXfaForm() ?? throw new Exception("The input document does not contain a form.");
        return xfaForm.IsXfaPresent() ? ReadFormData(xfaForm) : ReadFormData(form);
    }

    public static T ReadFormData<T>(PdfDocument pdfDocument, IReadOnlyDictionary<string, string> propertyNameLookup, IRunContext runContext)
    {
        var form = PdfAcroForm.GetAcroForm(pdfDocument, createIfNotExist: false);
        var xfaForm = form?.GetXfaForm() ?? throw new Exception("The input document does not contain a form.");
        return xfaForm.IsXfaPresent()
            ? ReadFormData<T>(xfaForm, propertyNameLookup) : ReadFormData<T>(form, propertyNameLookup, runContext);
    }

    public static SignatureInfo ReadSignatureInfo(PdfDocument pdfDocument)
    {
        var signatureUtil = new SignatureUtil(pdfDocument);
        return new SignatureInfo(signatureUtil.GetSignatureNames().Select(name =>
            {
                var signatureData = signatureUtil.ReadSignatureData(name);

                var bouncyCertificate = ((X509CertificateBC)signatureData.GetSigningCertificate()).GetCertificate();
                using var certificate = new X509Certificate2(DotNetUtilities.ToX509Certificate(bouncyCertificate));
                string verificationMessage;
                using (var chain = new X509Chain())
                {
                    chain.Build(certificate);
                    verificationMessage = string.Join(", ", chain.ChainStatus
                        .Select(cs => cs.StatusInformation)
                        .Where(i => !string.IsNullOrEmpty(i)));
                }

                return new Signature(
                    SignedBy: signatureData.GetSignName(),
                    SignedAt: signatureData.GetLocation(),
                    SignedOn: signatureData.GetSignDate(),
                    Reason: signatureData.GetReason(),
                    Unmodified: signatureData.VerifySignatureIntegrityAndAuthenticity(),
                    SignedVersion: signatureData.GetSigningInfoVersion(),
                    IsLatestVersionSigned: signatureData.GetSigningInfoVersion() == signatureUtil.GetTotalRevisions(),
                    Verified: certificate.Verify(),
                    VerificationMessage: verificationMessage);
            }));
    }

    private static IEnumerable<KeyValuePair<string, string>> ReadFormData(PdfAcroForm form)
    {
        return form.GetAllFormFields()
            .Select(fieldEntry => new KeyValuePair<string, string>(fieldEntry.Key, fieldEntry.Value.GetValueAsString()));
    }

    private static IEnumerable<KeyValuePair<string, string>> ReadFormData(XfaForm form)
    {
        var formDataNode = form.GetDatasetsNode()!
            .Element(XName.Get("data", @"http://www.xfa.org/schema/xfa-data/1.0/"))!
            .Elements().Single();

        return ReadFormData(formDataNode, Enumerable.Empty<string>());
    }

    private static IEnumerable<KeyValuePair<string, string>> ReadFormData(XElement element, IEnumerable<string> currentPath)
    {
        return element.HasElements
            ? element.Elements().SelectMany(e => ReadFormData(e, currentPath.Append(e.Name.LocalName)))
            : Enumerable.Repeat(new KeyValuePair<string, string>(string.Join('.', currentPath), element.Value), 1);
    }

    private static T ReadFormData<T>(PdfAcroForm form, IReadOnlyDictionary<string, string> propertyNameLookup, IRunContext runContext)
    {
        var result = Activator.CreateInstance<T>();
        foreach (var (fieldName, field) in form.GetAllFormFields())
        {
            if (!propertyNameLookup.TryGetValue(fieldName, out var propertyName))
            {
                propertyName = fieldName;
            }

            var property = typeof(T).GetProperty(propertyName);
            if (property == null)
            {
                runContext.Log("No property named {0} was found in the result type.", propertyName);
            }
            else if (property.PropertyType == typeof(string))
            {
                property.SetValue(result, field.GetValueAsString());
            }
            else if (property.PropertyType == typeof(bool))
            {
                property.SetValue(result, new[] { "Yes", "True", "1" }.Contains(field.GetValueAsString(), StringComparer.InvariantCultureIgnoreCase));
            }
            else
            {
                property.SetValue(result, Convert.ChangeType(field.GetValueAsString(), property.PropertyType));
            }
        }
        return result;
    }

    private static T ReadFormData<T>(XfaForm form, IReadOnlyDictionary<string, string> propertyNameLookup)
    {
        var formDataNode = form.GetDatasetsNode()!
            .Element(XName.Get("data", @"http://www.xfa.org/schema/xfa-data/1.0/"))!
            .Elements().Single();
        formDataNode.Name = XName.Get(GetElementNameForType(typeof(T)), formDataNode.Name.NamespaceName);

        if (propertyNameLookup.Any())
        {
            ApplyPropertyMapping(formDataNode, propertyNameLookup);
        }

        var xmlSerializer = new XmlSerializer(typeof(T));
        using var xmlReader = formDataNode.CreateReader();
        return (T)xmlSerializer.Deserialize(xmlReader)!;
    }

    private static string GetElementNameForType(Type type)
    {
        XmlRootAttribute? xmlRootAttribute = (XmlRootAttribute?)type.GetCustomAttributes(typeof(XmlRootAttribute), inherit: false).FirstOrDefault();
        if (xmlRootAttribute == null)
        {
            return type.Name;
        }

        return char.IsDigit(xmlRootAttribute.ElementName[0])
            ? $"_x003{xmlRootAttribute.ElementName[0]}_{xmlRootAttribute.ElementName[1..]}"
            : xmlRootAttribute.ElementName;
    }

    private static void ApplyPropertyMapping(XElement element, IReadOnlyDictionary<string, string> propertyNameLookup)
    {
        if (propertyNameLookup.TryGetValue(element.Name.LocalName, out var mappedPropertyName))
        {
            element.Name = XName.Get(mappedPropertyName, element.Name.NamespaceName);
        }

        foreach (var childElement in element.Elements())
        {
            ApplyPropertyMapping(childElement, propertyNameLookup);
        }
    }
}
