﻿using Twenty57.Linx.Interfaces.Plugin.FunctionProvider;
using Twenty57.Linx.Plugins.Pdf.Shared.Common;
using Twenty57.Linx.Plugins.Pdf.Shared.Common.Constants;
using Twenty57.Linx.Sdk.Core.LinxProperties;

namespace Twenty57.Linx.Plugins.Pdf.Runtime;

public sealed class PdfFileInfo
{
    public PdfFileInfo(string propertyGroupId, IFunctionBuilder functionBuilder)
    {
        var functionData = functionBuilder.Data;

        FilePathParameterName = functionBuilder.GetParamName(propertyGroupId + PropertyIds.PdfFilePathSuffix);
        AuthenticationType = functionData.Properties[propertyGroupId + PropertyIds.PdfAuthenticationTypeSuffix].GetValue<AuthenticationType>();

        if (AuthenticationType == AuthenticationType.Password)
        {
            PasswordParameterName = functionBuilder.GetParamName(propertyGroupId + PropertyIds.PdfPasswordSuffix);
        }
        else if (AuthenticationType == AuthenticationType.Certificate)
        {
            CertificateInfo = new CertificateInfo(propertyGroupId, functionBuilder);
        }
    }

    public string FilePathParameterName { get; }

    public AuthenticationType AuthenticationType { get; }

    public string? PasswordParameterName { get; }

    public CertificateInfo? CertificateInfo { get; }
}
