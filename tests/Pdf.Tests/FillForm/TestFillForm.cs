﻿using System;
using System.IO;
using NUnit.Framework;
using Twenty57.Linx.Plugins.Pdf.FillForm;
using Twenty57.Linx.Plugins.Pdf.Shared.Common;
using Twenty57.Linx.Plugins.Pdf.Shared.FillForm.Constants;
using Twenty57.Linx.Plugins.Pdf.Tests.Common;
using Twenty57.Linx.Plugins.Pdf.Tests.Extensions;
using Twenty57.Linx.Plugins.Pdf.Tests.Helpers;
using Twenty57.Linx.Sdk.TestKit;

namespace Twenty57.Linx.Plugins.Pdf.Tests.FillForm;

[TestFixture]
internal class TestFillForm : TestPdfBase
{
    private string outputFolderPath;

    [SetUp]
    public void SetUp()
    {
        this.outputFolderPath = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString("N"));
        Directory.CreateDirectory(this.outputFolderPath);
    }

    [TearDown]
    public void TearDown()
    {
        Directory.Delete(this.outputFolderPath, true);
    }

    [Test]
    [Combinatorial]
    public void FillFormAcro(
        [Values(
            FileAuthentication.None,
            FileAuthentication.Password,
            FileAuthentication.CertificateFile)] FileAuthentication inputFileAuthentication,
        [Values(true, false)] bool checkAcceptTCs)
    {
        string inputFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.FillForm.Resources.FillForm.pdf", this.inputFolderPath);
        string outputFilePath = Path.Combine(this.outputFolderPath, "Fill.pdf");

        var formData = new
        {
            FirstName = "Jane",
            Surname = "Woods",
            Gender = "Female",
            AcceptTCs = checkAcceptTCs
        };

        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, inputFileAuthentication, inputFilePath);
        propertyValues.AddRuntime(Shared.Common.Constants.PropertyIds.OutputFilePath, outputFilePath);
        propertyValues.AddRuntime(PropertyIds.FormData, formData);
        propertyValues.AddDesignTime(PropertyIds.PropertyMapping, new PropertyMapping
        {
            new PropertyMappingEntry("First Name", "FirstName")
        }.ToString());

        var tester = new FunctionTester<Provider>();
        tester.Execute(propertyValues);

        PdfComparer.AssertAcroFields(outputFilePath, inputFileAuthentication, this.authenticationManager,
            ("First Name", "Jane"),
            ("Surname", "Woods"),
            ("Gender", "Female"),
            ("AcceptTCs", checkAcceptTCs ? "Yes" : "Off"));
    }

    [Test]
    public void FillFormXfa(
        [Values(
            FileAuthentication.None,
            FileAuthentication.Password,
            FileAuthentication.CertificateFile)] FileAuthentication inputFileAuthentication)
    {
        string inputFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.FillForm.Resources.FillFormXFA.pdf", this.inputFolderPath);
        string outputFilePath = Path.Combine(this.outputFolderPath, "FillXfa.pdf");

        var formData = new Person
        {
            FullName = "John Doe",
            LastName = "Doe",
            Email = "johndoe@digiata.com",
            EmailMe = true
        };

        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, inputFileAuthentication, inputFilePath);
        propertyValues.AddRuntime(Shared.Common.Constants.PropertyIds.OutputFilePath, outputFilePath);
        propertyValues.AddRuntime(PropertyIds.FormData, formData);
        propertyValues.AddDesignTime(PropertyIds.PropertyMapping, new PropertyMapping
        {
            new PropertyMappingEntry("Surname", "LastName")
        }.ToString());

        var tester = new FunctionTester<Provider>();
        tester.Execute(propertyValues);

        PdfComparer.AssertXfaFormData(outputFilePath, inputFileAuthentication, this.authenticationManager,
            new FormDataPerson
            {
                FullName = "John Doe",
                Surname = "Doe",
                Email = "johndoe@digiata.com",
                EmailMe = true
            });
    }

    [Test]
    public void NotAForm()
    {
        string inputFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.FillForm.Resources.Text.pdf", this.inputFolderPath);
        string outputFilePath = Path.Combine(this.outputFolderPath, "Fill.pdf");

        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, FileAuthentication.None, inputFilePath);
        propertyValues.AddRuntime(Shared.Common.Constants.PropertyIds.OutputFilePath, outputFilePath);
        propertyValues.AddRuntime(PropertyIds.FormData, new { });

        Assert.That(() => new FunctionTester<Provider>().Execute(propertyValues),
            Throws.Exception.TypeOf<ExecuteException>()
            .With.Property(nameof(Exception.Message))
            .StartsWith("The input document does not contain a form.\r\nSee Code and Parameter properties for more information."));
    }

    private void ConfigureInputFileFunctionValues(FunctionPropertyValues propertyValues, FileAuthentication fileAuthentication, string inputFilePath)
    {
        ConfigureInputFileFunctionValues(
            propertyValues,
            fileAuthentication,
            inputFilePath,
            Shared.Common.Constants.CategoryNames.Input);
    }
}

public class Person
{
    public string FullName { get; init; }
    public string LastName { get; init; }
    public string Email { get; init; }
    public bool EmailMe { get; init; }
}

public class FormDataPerson
{
    public string FullName { get; init; }
    public string Surname { get; init; }
    public string Email { get; init; }
    public bool EmailMe { get; init; }
}
