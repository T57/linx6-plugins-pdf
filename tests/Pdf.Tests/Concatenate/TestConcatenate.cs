﻿using System;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;
using Twenty57.Linx.Plugins.Pdf.Concatenate;
using Twenty57.Linx.Plugins.Pdf.Shared.Concatenate.Constants;
using Twenty57.Linx.Plugins.Pdf.Tests.Common;
using Twenty57.Linx.Plugins.Pdf.Tests.Extensions;
using Twenty57.Linx.Plugins.Pdf.Tests.Helpers;
using Twenty57.Linx.Sdk.TestKit;

namespace Twenty57.Linx.Plugins.Pdf.Tests.Concatenate;

[TestFixture]
internal class TestConcatenate : TestPdfBase
{
    private string outputFolderPath;

    [SetUp]
    public void SetUp()
    {
        this.outputFolderPath = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString("N"));
        Directory.CreateDirectory(this.outputFolderPath);
    }

    [TearDown]
    public void TearDown()
    {
        Directory.Delete(this.outputFolderPath, true);
    }

    [Test]
    public void Concatenate()
    {
        string inputFilePath1 = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Concatenate.Resources.Concatenate1.pdf", this.inputFolderPath);
        PdfComparer.AssertPageCount(inputFilePath1, FileAuthentication.None, this.authenticationManager, 1);
        PdfComparer.AssertText(inputFilePath1, FileAuthentication.None, this.authenticationManager, new[] { "1" });
        PdfComparer.AssertJavaScript(inputFilePath1,
            FileAuthentication.None,
            this.authenticationManager,
            new[] { "function Script1()\r\n{}" });
        string inputFilePath2 = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Concatenate.Resources.Concatenate2.pdf", this.inputFolderPath);
        PdfComparer.AssertPageCount(inputFilePath2, FileAuthentication.None, this.authenticationManager, 1);
        PdfComparer.AssertText(inputFilePath2, FileAuthentication.None, this.authenticationManager, new[] { "2" });
        PdfComparer.AssertJavaScript(inputFilePath2,
            FileAuthentication.None,
            this.authenticationManager,
            new[] { "function Script2()\r\n{}" });
        string outputFilePath = Path.Combine(this.outputFolderPath, "Concat.pdf");

        var propertyValues = new FunctionPropertyValues();
        propertyValues.AddRuntime(PropertyIds.InputFiles, new List<string> { inputFilePath1, inputFilePath2 });
        propertyValues.AddRuntime(Shared.Common.Constants.PropertyIds.OutputFilePath, outputFilePath);

        var tester = new FunctionTester<Provider>();
        tester.Execute(propertyValues);

        Assert.That(File.Exists(outputFilePath), Is.True);
        PdfComparer.AssertPageCount(outputFilePath, FileAuthentication.None, this.authenticationManager, 2);
        PdfComparer.AssertText(
            outputFilePath,
            FileAuthentication.None,
            this.authenticationManager,
            new[] { "1", "2" });
        PdfComparer.AssertJavaScript(
            outputFilePath,
            FileAuthentication.None,
            this.authenticationManager,
            new[] { "function Script1()\r\n{}", "function Script2()\r\n{}" });
    }

    [Test]
    public void ConcatenateWithNonExistentInputFile()
    {
        string inputFilePath1 = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Concatenate.Resources.Concatenate1.pdf", this.inputFolderPath);
        string invalidFilePath = Path.Combine(this.inputFolderPath, Path.GetRandomFileName());
        string outputFilePath = Path.Combine(this.outputFolderPath, "Concat.pdf");

        var propertyValues = new FunctionPropertyValues();
        propertyValues.AddRuntime(PropertyIds.InputFiles, new List<string> { inputFilePath1, invalidFilePath });
        propertyValues.AddRuntime(Shared.Common.Constants.PropertyIds.OutputFilePath, outputFilePath);

        Assert.That(() => new FunctionTester<Provider>().Execute(propertyValues),
            Throws.Exception.TypeOf<ExecuteException>()
            .With.Property(nameof(ExecuteException.Message)).StartsWith(invalidFilePath + " not found as file or resource.\r\nSee Code and Parameter properties for more information."));
    }
}
