﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Twenty57.Linx.Plugins.Pdf.Shared.Split.Constants;
using Twenty57.Linx.Plugins.Pdf.Split;
using Twenty57.Linx.Plugins.Pdf.Tests.Common;
using Twenty57.Linx.Plugins.Pdf.Tests.Extensions;
using Twenty57.Linx.Plugins.Pdf.Tests.Helpers;
using Twenty57.Linx.Sdk.Core.LinxFunctionProvider;
using Twenty57.Linx.Sdk.TestKit;

namespace Twenty57.Linx.Plugins.Pdf.Tests.Split;

[TestFixture]
internal class TestSplit : TestPdfBase
{
    private string outputFolderPath;

    [SetUp]
    public void SetUp()
    {
        this.outputFolderPath = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString("N"));
        Directory.CreateDirectory(this.outputFolderPath);
    }

    [TearDown]
    public void TearDown()
    {
        Directory.Delete(this.outputFolderPath, true);
    }

    [Test]
    public void ResultTypeWithNoLoop()
    {
        var tester = new FunctionTester<Provider>();
        var designer = (FunctionDesigner)tester.CreateDesigner();
        designer.Properties[PropertyIds.LoopResults].Value = false;

        Assert.That(designer.ExecutionPaths.Count, Is.EqualTo(0));

        var resultTypeProperties = designer.Result.GetProperties();
        Assert.That(resultTypeProperties.Count(), Is.EqualTo(2));
        resultTypeProperties.ElementAt(0).AssertList(OutputNames.PageFiles, typeof(string));
        resultTypeProperties.ElementAt(1).AssertCompiled(OutputNames.NumberOfPages, typeof(int));
    }

    [Test]
    public void ResultTypeWithLoop()
    {
        var tester = new FunctionTester<Provider>();
        var designer = (FunctionDesigner)tester.CreateDesigner();
        designer.Properties[PropertyIds.LoopResults].Value = true;

        var executionPath = designer.ExecutionPaths.Single();
        Assert.That(executionPath.Name, Is.EqualTo(ExecutionPathNames.PageFiles));
        Assert.That(executionPath.Result, Is.EqualTo(TypeReference.CreateCompiled(typeof(string))));

        var resultTypeProperties = designer.Result.GetProperties();
        resultTypeProperties.Single().AssertCompiled(OutputNames.NumberOfPages, typeof(int));
    }

    [Test]
    public void ExecuteWithNoLoop(
        [Values(
            FileAuthentication.None,
            FileAuthentication.Password,
            FileAuthentication.CertificateFile)] FileAuthentication fileAuthentication)
    {
        string inputFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Split.Resources.Split.pdf", this.inputFolderPath);
        string outputFilePath = Path.Combine(this.outputFolderPath, "Split.pdf");

        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, fileAuthentication, inputFilePath);
        propertyValues.AddRuntime(Shared.Common.Constants.PropertyIds.OutputFilePath, outputFilePath);
        propertyValues.AddDesignTime(PropertyIds.LoopResults, false);

        var tester = new FunctionTester<Provider>();
        var result = tester.Execute(propertyValues);

        Assert.That(result.Value.NumberOfPages, Is.EqualTo(2));
        Assert.That(result.Value.PageFiles.Count, Is.EqualTo(2));

        string pageFile = result.Value.PageFiles[0];
        AssertOutputFile(pageFile, fileAuthentication, Path.Combine(this.outputFolderPath, "Split_1.pdf"), 1, "1");

        pageFile = result.Value.PageFiles[1];
        AssertOutputFile(pageFile, fileAuthentication, Path.Combine(this.outputFolderPath, "Split_2.pdf"), 1, "2");
    }

    [Test]
    public void ExecuteWithLoop(
        [Values(
            FileAuthentication.None,
            FileAuthentication.Password,
            FileAuthentication.CertificateFile)] FileAuthentication fileAuthentication)
    {
        string inputFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Split.Resources.Split.pdf", this.inputFolderPath);
        string outputFilePath = Path.Combine(this.outputFolderPath, "Split.pdf");

        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, fileAuthentication, inputFilePath);
        propertyValues.AddRuntime(Shared.Common.Constants.PropertyIds.OutputFilePath, outputFilePath);
        propertyValues.AddDesignTime(PropertyIds.LoopResults, true);

        var tester = new FunctionTester<Provider>();
        tester.AddExecutionPathAction(ExecutionPathNames.PageFiles, _ =>
        {
            Assert.That(IsFileLocked(inputFilePath), Is.True, "Input file should be open while execution path is running.");
        });
        FunctionResult result = tester.Execute(propertyValues);
        Assert.That(IsFileLocked(inputFilePath), Is.False, "Input file should be closed after execution.");

        Assert.That(result.Value.NumberOfPages, Is.EqualTo(2));
        Assert.That(result.ExecutionPathResult.Count(), Is.EqualTo(2));

        var nextResult = result.ExecutionPathResult.ElementAt(0);
        Assert.That(nextResult.ExecutionPathId, Is.EqualTo(ExecutionPathNames.PageFiles));
        AssertOutputFile((string)nextResult.Value, fileAuthentication, Path.Combine(this.outputFolderPath, "Split_1.pdf"), 1, "1");

        nextResult = result.ExecutionPathResult.ElementAt(1);
        Assert.That(nextResult.ExecutionPathId, Is.EqualTo(ExecutionPathNames.PageFiles));
        AssertOutputFile((string)nextResult.Value, fileAuthentication, Path.Combine(this.outputFolderPath, "Split_2.pdf"), 1, "2");
    }

    private void ConfigureInputFileFunctionValues(FunctionPropertyValues propertyValues, FileAuthentication inputAuth, string inputFilePath)
    {
        ConfigureInputFileFunctionValues(
            propertyValues,
            inputAuth,
            inputFilePath,
            Shared.Common.Constants.CategoryNames.Input);
    }

    private void AssertOutputFile(
        string outputFilePath,
        FileAuthentication outputAuth,
        string expectedFilePath,
        int expectedNumberOfPages,
        string expectedText)
    {
        Assert.That(File.Exists(outputFilePath), Is.True);
        Assert.That(outputFilePath, Is.EqualTo(expectedFilePath));
        PdfComparer.AssertPageCount(outputFilePath, outputAuth, this.authenticationManager, expectedNumberOfPages);
        PdfComparer.AssertText(outputFilePath, outputAuth, this.authenticationManager, new[] { expectedText });
    }

    private static bool IsFileLocked(string filePath)
    {
        if (!File.Exists(filePath))
        {
            return false;
        }

        FileStream stream = null;
        try
        {
            stream = File.Open(filePath, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
        }
        catch (IOException)
        {
            return true;
        }
        finally
        {
            stream?.Close();
        }
        return false;
    }
}
