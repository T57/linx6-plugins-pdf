﻿using System;
using System.IO;
using NUnit.Framework;
using Twenty57.Linx.Plugins.Pdf.ChangeProtection;
using Twenty57.Linx.Plugins.Pdf.Design.Common;
using Twenty57.Linx.Plugins.Pdf.Shared.ChangeProtection;
using Twenty57.Linx.Plugins.Pdf.Shared.ChangeProtection.Constants;
using Twenty57.Linx.Plugins.Pdf.Shared.Common;
using Twenty57.Linx.Plugins.Pdf.Tests.Common;
using Twenty57.Linx.Plugins.Pdf.Tests.Extensions;
using Twenty57.Linx.Plugins.Pdf.Tests.Helpers;
using Twenty57.Linx.Sdk.TestKit;

namespace Twenty57.Linx.Plugins.Pdf.Tests.ChangeProtection;

[TestFixture]
internal class TestChangeProtection : TestPdfBase
{
    private const string PermissionsPassword = "permissions";

    private string outputFolderPath;

    [SetUp]
    public void SetUp()
    {
        this.outputFolderPath = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString("N"));
        Directory.CreateDirectory(this.outputFolderPath);
    }

    [TearDown]
    public void TearDown()
    {
        Directory.Delete(this.outputFolderPath, true);
    }

    [Test]
    public void ProtectWithRemoveProtection(
        [Values(
            FileAuthentication.None,
            FileAuthentication.Password,
            FileAuthentication.CertificateFile)] FileAuthentication inputAuthentication)
    {
        string inputFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.ChangeProtection.Resources.Protect.pdf", this.inputFolderPath);
        string outputFilePath = Path.Combine(this.outputFolderPath, "Protect.pdf");

        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, inputAuthentication, inputFilePath);
        ConfigureOutputFileFunctionValues(propertyValues, outputFilePath, FileAuthentication.None, Encryption.AES128, false);

        var tester = new FunctionTester<Provider>();
        tester.Execute(propertyValues);

        PdfComparer.AssertText(outputFilePath, FileAuthentication.None, this.authenticationManager, new[] { "Text on page 1" });
    }

    [Test]
    public void ProtectWithNoRestrictions(
        [Values(
            FileAuthentication.Password,
            FileAuthentication.CertificateFile)] FileAuthentication outputAuthentication,
            [Values(false, true)] bool dontEncryptMetadata)
    {
        string inputFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.ChangeProtection.Resources.Protect.pdf", this.inputFolderPath);
        string outputFilePath = Path.Combine(this.outputFolderPath, "Protect.pdf");

        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, FileAuthentication.None, inputFilePath);
        ConfigureOutputFileFunctionValues(propertyValues, outputFilePath, outputAuthentication, Encryption.AES256, dontEncryptMetadata);

        var tester = new FunctionTester<Provider>();
        tester.Execute(propertyValues);

        PdfComparer.AssertProtectionAllPermissions(outputFilePath, outputAuthentication, this.authenticationManager, dontEncryptMetadata);
    }

    [Test]
    [Combinatorial]
    public void ProtectWithPrintRestrictions(
        [Values(
            FileAuthentication.Password/*,
				Ignore: https://stackoverflow.com/questions/71764980/itext-7-certificate-encrypted-pdf-returns-no-permissions
				FileAuthentication.CertificateFile,
				FileAuthentication.CertificateStore*/)] FileAuthentication protectAuth,
        [Values(
            Printing.None,
            Printing.LowResolution,
            Printing.HighResolution)] Printing printing)
    {
        string inputFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.ChangeProtection.Resources.Protect.pdf", this.inputFolderPath);
        string outputFilePath = Path.Combine(this.outputFolderPath, "Protect.pdf");

        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, FileAuthentication.None, inputFilePath);
        ConfigureOutputFileFunctionValues(propertyValues, outputFilePath, protectAuth, Encryption.AES256, true, true, printing: printing);

        var tester = new FunctionTester<Provider>();
        tester.Execute(propertyValues);

        bool allowDegradedPrinting = false;
        bool allowPrinting = false;
        switch (printing)
        {
            case Printing.None:
                break;
            case Printing.LowResolution:
                allowDegradedPrinting = true;
                break;
            case Printing.HighResolution:
                allowDegradedPrinting = true;
                allowPrinting = true;
                break;
            default:
                throw new NotSupportedException("Invalid Printing specified.");
        }

        PdfComparer.AssertProtection(outputFilePath, protectAuth, this.authenticationManager,
            expectedAllowDegradedPrinting: allowDegradedPrinting,
            expectedAllowPrinting: allowPrinting);

        if (protectAuth == FileAuthentication.Password)
        {
            using var permissionsAuthHelper = new AuthenticationManager(PermissionsPassword);
            PdfComparer.AssertProtectionAllPermissions(outputFilePath, FileAuthentication.Password, permissionsAuthHelper);
        }
    }

    [Test]
    [Combinatorial]
    public void ProtectWithChangeRestrictions(
        [Values(
            FileAuthentication.Password/*,
				Ignore: https://stackoverflow.com/questions/71764980/itext-7-certificate-encrypted-pdf-returns-no-permissions
				FileAuthentication.CertificateFile,
				FileAuthentication.CertificateStore*/)] FileAuthentication protectAuth,
        [Values(
            Changes.None,
            Changes.Assembly,
            Changes.FillIn,
            Changes.AnnotateAndFillIn,
            Changes.AnyExceptExtract)] Changes changes)
    {
        string inputFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.ChangeProtection.Resources.Protect.pdf", this.inputFolderPath);
        string outputFilePath = Path.Combine(this.outputFolderPath, "Protect.pdf");

        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, FileAuthentication.None, inputFilePath);
        ConfigureOutputFileFunctionValues(propertyValues, outputFilePath, protectAuth, Encryption.AES256, true, true, changes: changes);

        var tester = new FunctionTester<Provider>();
        tester.Execute(propertyValues);

        bool allowAssembly = false;
        bool allowFillIn = false;
        bool allowModifyAnnotations = false;
        bool allowModifyContents = false;
        switch (changes)
        {
            case Changes.None:
                break;
            case Changes.Assembly:
                allowAssembly = true;
                break;
            case Changes.FillIn:
                allowFillIn = true;
                break;
            case Changes.AnnotateAndFillIn:
                allowModifyAnnotations = true;
                allowFillIn = true;
                break;
            case Changes.AnyExceptExtract:
                allowModifyContents = true;
                allowModifyAnnotations = true;
                allowFillIn = true;
                break;
            default:
                throw new NotSupportedException("Invalid Changes specified.");
        }

        PdfComparer.AssertProtection(outputFilePath, protectAuth, this.authenticationManager,
            expectedAllowAssembly: allowAssembly,
            expectedAllowFillIn: allowFillIn,
            expectedAllowModifyAnnotations: allowModifyAnnotations,
            expectedAllowModifyContents: allowModifyContents);

        if (protectAuth == FileAuthentication.Password)
        {
            using var permissionsAuthHelper = new AuthenticationManager(PermissionsPassword);
            PdfComparer.AssertProtectionAllPermissions(outputFilePath, FileAuthentication.Password, permissionsAuthHelper);
        }
    }

    [Test]
    [Combinatorial]
    public void ProtectWithCopyRestrictions(
        [Values(
            FileAuthentication.Password/*,
				Ignore: https://stackoverflow.com/questions/71764980/itext-7-certificate-encrypted-pdf-returns-no-permissions
				FileAuthentication.CertificateFile,
				FileAuthentication.CertificateStore*/)] FileAuthentication protectAuth,
        [Values(
            true,
            false)] bool allowCopy)
    {
        string inputFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.ChangeProtection.Resources.Protect.pdf", this.inputFolderPath);
        string outputFilePath = Path.Combine(this.outputFolderPath, "Protect.pdf");

        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, FileAuthentication.None, inputFilePath);
        ConfigureOutputFileFunctionValues(propertyValues, outputFilePath, protectAuth, Encryption.AES256, true, true, allowCopy: allowCopy);

        var tester = new FunctionTester<Provider>();
        tester.Execute(propertyValues);

        PdfComparer.AssertProtection(outputFilePath, protectAuth, this.authenticationManager, expectedAllowCopy: allowCopy);

        if (protectAuth == FileAuthentication.Password)
        {
            using var permissionsAuthHelper = new AuthenticationManager(PermissionsPassword);
            PdfComparer.AssertProtectionAllPermissions(outputFilePath, FileAuthentication.Password, permissionsAuthHelper);
        }
    }

    [Test]
    [Combinatorial]
    public void ProtectWithScreenReaderRestrictions(
        [Values(
            FileAuthentication.Password/*,
				Ignore: https://stackoverflow.com/questions/71764980/itext-7-certificate-encrypted-pdf-returns-no-permissions
				FileAuthentication.CertificateFile,
				FileAuthentication.CertificateStore*/)] FileAuthentication protectAuth,
        [Values(
            true,
            false)] bool allowScreenReaders)
    {
        string inputFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.ChangeProtection.Resources.Protect.pdf", this.inputFolderPath);
        string outputFilePath = Path.Combine(this.outputFolderPath, "Protect.pdf");

        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, FileAuthentication.None, inputFilePath);
        ConfigureOutputFileFunctionValues(propertyValues, outputFilePath, protectAuth, Encryption.AES256, true, true, allowScreenReaders: allowScreenReaders);

        var tester = new FunctionTester<Provider>();
        tester.Execute(propertyValues);

        PdfComparer.AssertProtection(outputFilePath, protectAuth, this.authenticationManager, expectedAllowScreenReaders: allowScreenReaders);

        if (protectAuth == FileAuthentication.Password)
        {
            using var permissionsAuthHelper = new AuthenticationManager(PermissionsPassword);
            PdfComparer.AssertProtectionAllPermissions(outputFilePath, FileAuthentication.Password, permissionsAuthHelper);
        }
    }

    private void ConfigureInputFileFunctionValues(
        FunctionPropertyValues propertyValues,
        FileAuthentication inputAuth,
        string inputFilePath)
    {
        ConfigureInputFileFunctionValues(
            propertyValues,
            inputAuth,
            inputFilePath,
            Shared.Common.Constants.CategoryNames.Input);
    }

    private void ConfigureOutputFileFunctionValues(
        FunctionPropertyValues propertyValues,
        string outputFilePath,
        FileAuthentication outputAuthentication,
        Encryption encryption,
        bool dontEncryptMetadata,
        bool addDocumentRestrictions = false,
        Printing printing = Printing.None,
        Changes changes = Changes.None,
        bool allowCopy = false,
        bool allowScreenReaders = false)
    {
        var outputPdfPropertyGroup = new PdfFilePropertyGroup(Shared.Common.Constants.CategoryNames.Output);
        propertyValues.AddRuntime(outputPdfPropertyGroup.FilePathPropertyId, outputFilePath);

        switch (outputAuthentication)
        {
            case FileAuthentication.None:
                propertyValues.AddDesignTime(outputPdfPropertyGroup.AuthenticationTypePropertyId, AuthenticationType.None);
                break;
            case FileAuthentication.Password:
                propertyValues.AddDesignTime(outputPdfPropertyGroup.AuthenticationTypePropertyId, AuthenticationType.Password);
                propertyValues.AddRuntime(outputPdfPropertyGroup.PasswordPropertyId, this.authenticationManager.Password);
                break;
            case FileAuthentication.CertificateFile:
                propertyValues.AddDesignTime(outputPdfPropertyGroup.AuthenticationTypePropertyId, AuthenticationType.Certificate);
                propertyValues.AddRuntime(outputPdfPropertyGroup.CertificateFilePathPropertyId, this.authenticationManager.CertificateFilePath);
                propertyValues.AddRuntime(outputPdfPropertyGroup.CertificateFilePasswordPropertyId, this.authenticationManager.CertificateFilePassword);
                break;
        }

        if (outputAuthentication != FileAuthentication.None)
        {
            propertyValues.AddDesignTime(PropertyIds.AddDocumentRestrictions, addDocumentRestrictions);

            if (addDocumentRestrictions)
            {
                propertyValues.AddDesignTime(PropertyIds.AllowPrinting, printing);
                propertyValues.AddDesignTime(PropertyIds.AllowChanges, changes);
                propertyValues.AddDesignTime(PropertyIds.AllowCopying, allowCopy);
                propertyValues.AddDesignTime(PropertyIds.AllowScreenReaders, allowScreenReaders);

                if (outputAuthentication == FileAuthentication.Password)
                {
                    propertyValues.AddRuntime(PropertyIds.PermissionsPassword, PermissionsPassword);
                }
            }

            propertyValues.AddDesignTime(PropertyIds.Encryption, encryption);
            propertyValues.AddDesignTime(PropertyIds.DontEncryptMetadata, dontEncryptMetadata);
        }
    }
}
