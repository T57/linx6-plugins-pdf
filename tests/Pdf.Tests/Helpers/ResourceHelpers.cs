﻿using System;
using System.IO;
using System.Reflection;

namespace Twenty57.Linx.Plugins.Pdf.Tests.Helpers;

internal static class ResourceHelpers
{
    public static string WriteResourceToFile(string resourceName, string folderPath)
    {
        if (!Directory.Exists(folderPath))
        {
            throw new DirectoryNotFoundException($"[{folderPath}] does not exist.");
        }

        string filePath = Path.Combine(folderPath, Path.GetRandomFileName());

        using Stream input = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName);
        if (input == null)
        {
            throw new Exception($"Resource [{resourceName}] not found.");
        }

        using Stream output = File.Create(filePath);
        input.CopyTo(output);

        return filePath;
    }
}
