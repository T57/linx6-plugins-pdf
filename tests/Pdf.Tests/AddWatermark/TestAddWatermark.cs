﻿using System;
using System.IO;
using NUnit.Framework;
using Twenty57.Linx.Plugins.Pdf.AddWatermark;
using Twenty57.Linx.Plugins.Pdf.Shared.AddWatermark;
using Twenty57.Linx.Plugins.Pdf.Shared.AddWatermark.Constants;
using Twenty57.Linx.Plugins.Pdf.Tests.Common;
using Twenty57.Linx.Plugins.Pdf.Tests.Extensions;
using Twenty57.Linx.Plugins.Pdf.Tests.Helpers;
using Twenty57.Linx.Sdk.TestKit;

namespace Twenty57.Linx.Plugins.Pdf.Tests.AddWatermark;

[TestFixture]
internal class TestAddWatermark : TestPdfBase
{
    private string outputFolderPath;

    [SetUp]
    public void SetUp()
    {
        this.outputFolderPath = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString("N"));
        Directory.CreateDirectory(this.outputFolderPath);
    }

    [TearDown]
    public void TearDown()
    {
        Directory.Delete(this.outputFolderPath, true);
    }

    [Test]
    [Combinatorial]
    public void AddWatermark(
        [Values(
            FileAuthentication.None,
            FileAuthentication.Password,
            FileAuthentication.CertificateFile)] FileAuthentication inputAuth,
        [Values(
            WatermarkPosition.Above,
            WatermarkPosition.Below)] WatermarkPosition position)
    {
        string inputFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.AddWatermark.Resources.Watermark.pdf", this.inputFolderPath);
        string watermarkFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.AddWatermark.Resources.Overlay.pdf", this.inputFolderPath);
        string outputFilePath = Path.Combine(this.outputFolderPath, "Watermark.pdf");

        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, inputAuth, inputFilePath);
        var watermarkPages = "4;1-2,2,2";
        ConfigureWatermarkFunctionValues(propertyValues, FileAuthentication.None, watermarkFilePath, position, watermarkPages);
        propertyValues.AddRuntime(Shared.Common.Constants.PropertyIds.OutputFilePath, outputFilePath);

        var tester = new FunctionTester<Provider>();
        tester.Execute(propertyValues);

        PdfComparer.AssertText(
            outputFilePath,
            inputAuth,
            this.authenticationManager,
            new[] { "1\nWatermark", "2\nWatermark", "3", "4\nWatermark" });
    }

    [Test]
    public void AddWatermarkWithAuthentication(
        [Values(
            FileAuthentication.None,
            FileAuthentication.Password,
            FileAuthentication.CertificateFile)] FileAuthentication watermarkAuth)
    {
        string inputFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.AddWatermark.Resources.Watermark.pdf", this.inputFolderPath);
        string watermarkFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.AddWatermark.Resources.Overlay.pdf", this.inputFolderPath);
        string outputFilePath = Path.Combine(this.outputFolderPath, "Watermark.pdf");

        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, FileAuthentication.None, inputFilePath);
        var watermarkPages = string.Empty;
        ConfigureWatermarkFunctionValues(propertyValues, watermarkAuth, watermarkFilePath, WatermarkPosition.Below, watermarkPages);
        propertyValues.AddRuntime(Shared.Common.Constants.PropertyIds.OutputFilePath, outputFilePath);

        var tester = new FunctionTester<Provider>();
        tester.Execute(propertyValues);

        PdfComparer.AssertText(
            outputFilePath,
            FileAuthentication.None,
            this.authenticationManager,
            new[] { "1\nWatermark", "2\nWatermark", "3\nWatermark", "4\nWatermark" });
    }

    private void ConfigureInputFileFunctionValues(FunctionPropertyValues propertyValues, FileAuthentication inputAuth, string inputFilePath)
    {
        ConfigureInputFileFunctionValues(
            propertyValues,
            inputAuth,
            inputFilePath,
            Shared.Common.Constants.CategoryNames.Input);
    }

    private void ConfigureWatermarkFunctionValues(FunctionPropertyValues propertyValues, FileAuthentication watermarkAuth, string watermarkFilePath, WatermarkPosition position,
        string watermarkPages)
    {
        propertyValues.AddDesignTime(PropertyIds.Position, position);
        propertyValues.AddRuntime(PropertyIds.Pages, watermarkPages);

        ConfigureInputFileFunctionValues(
            propertyValues,
            watermarkAuth,
            watermarkFilePath,
            CategoryNames.Watermark);
    }
}
