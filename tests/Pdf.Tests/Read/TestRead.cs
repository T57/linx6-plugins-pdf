﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using NUnit.Framework;
using Twenty57.Linx.Plugins.Pdf.Design.Common;
using Twenty57.Linx.Plugins.Pdf.Read;
using Twenty57.Linx.Plugins.Pdf.Shared.Common;
using Twenty57.Linx.Plugins.Pdf.Shared.Read;
using Twenty57.Linx.Plugins.Pdf.Shared.Read.Constants;
using Twenty57.Linx.Plugins.Pdf.Tests.Common;
using Twenty57.Linx.Plugins.Pdf.Tests.Extensions;
using Twenty57.Linx.Plugins.Pdf.Tests.Helpers;
using Twenty57.Linx.Sdk.Core.LinxFunctionProvider;
using Twenty57.Linx.Sdk.TestKit;

namespace Twenty57.Linx.Plugins.Pdf.Tests.Read;

[TestFixture]
internal class TestRead : TestPdfBase
{
    [Test]
    public void ResultTypeWithNoOutputOptions()
    {
        var tester = new FunctionTester<Provider>();
        var designer = (FunctionDesigner)tester.CreateDesigner();
        Assert.That(designer.Result, Is.Null);
    }

    [Test]
    public void ReadTextResultTypeWithSplitTextNever()
    {
        var tester = new FunctionTester<Provider>();
        var designer = (FunctionDesigner)tester.CreateDesigner();
        designer.Properties[PropertyIds.ReadText].Value = true;
        designer.Properties[PropertyIds.SplitText].Value = TextSplit.Never;

        var resultTypeProperties = designer.Result.GetProperties();
        resultTypeProperties.Single().AssertCompiled(OutputNames.Text, typeof(string));
    }

    [Test]
    public void ReadTextResultTypeWithSplitTextPage()
    {
        var tester = new FunctionTester<Provider>();
        var designer = (FunctionDesigner)tester.CreateDesigner();
        designer.Properties[PropertyIds.ReadText].Value = true;
        designer.Properties[PropertyIds.SplitText].Value = TextSplit.Page;

        var resultTypeProperties = designer.Result.GetProperties();
        resultTypeProperties.Single().AssertList(OutputNames.Text, typeof(string));
    }

    [Test]
    public void ReadFormDataResultTypeWithNoCustomType()
    {
        var tester = new FunctionTester<Provider>();
        var designer = (FunctionDesigner)tester.CreateDesigner();
        designer.Properties[PropertyIds.ReadFormData].Value = true;
        designer.Properties[PropertyIds.FormDataSpecificationType].Value = FormDataSpecificationType.CustomType;

        Assert.That(designer.Result, Is.Null);
    }

    [Test]
    public void ReadFormDataResultTypeWithCustomType()
    {
        var tester = new FunctionTester<Provider>();
        var designer = (FunctionDesigner)tester.CreateDesigner();
        designer.Properties[PropertyIds.ReadFormData].Value = true;
        designer.Properties[PropertyIds.FormDataSpecificationType].Value = FormDataSpecificationType.CustomType;
        designer.Properties[PropertyIds.FormDataType].Value = TypeReference.CreateCompiled(typeof(string));

        var resultTypeProperties = designer.Result.GetProperties();
        resultTypeProperties.Single().AssertCompiled(OutputNames.FormData, typeof(string));
    }

    [Test]
    public void ReadFormDataResultTypeWithNoSamplePdf()
    {
        var tester = new FunctionTester<Provider>();
        var designer = (FunctionDesigner)tester.CreateDesigner();
        designer.Properties[PropertyIds.ReadFormData].Value = true;
        designer.Properties[PropertyIds.FormDataSpecificationType].Value = FormDataSpecificationType.Infer;

        Assert.That(designer.Result, Is.Null);
    }

    [Test]
    public void ReadFormDataResultTypeWithBlankSamplePdf()
    {
        var tester = new FunctionTester<Provider>();
        var designer = (FunctionDesigner)tester.CreateDesigner();
        designer.Properties[PropertyIds.ReadFormData].Value = true;
        designer.Properties[PropertyIds.FormDataSpecificationType].Value = FormDataSpecificationType.Infer;
        string blankPdfFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Read.Resources.Blank.pdf", this.inputFolderPath);
        designer.Properties[PropertyIds.SamplePdf].Value = blankPdfFilePath;

        Assert.That(designer.Result, Is.Null);
    }

    [Test]
    public void ReadFormDataResultTypeWithSampleAcroFieldsPdf()
    {
        var tester = new FunctionTester<Provider>();
        var designer = (FunctionDesigner)tester.CreateDesigner();
        designer.Properties[PropertyIds.ReadFormData].Value = true;
        designer.Properties[PropertyIds.FormDataSpecificationType].Value = FormDataSpecificationType.Infer;
        string inferPdfFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Read.Resources.InferFieldsAcro.pdf", this.inputFolderPath);
        designer.Properties[PropertyIds.SamplePdf].Value = inferPdfFilePath;

        var resultTypeProperties = designer.Result.GetProperties();
        resultTypeProperties.Single().AssertGenerated(OutputNames.FormData);
        var formDataTypeProperties = resultTypeProperties.Single().TypeReference.GetProperties();
        Assert.That(
            formDataTypeProperties.Select(p => p.Name), Is.EquivalentTo(new[] { "First_32Name", "Surname" }));
    }

    [Test]
    public void ReadFormDataResultTypeWithSampleAcroFieldsPdfAndPropertyMapping()
    {
        var tester = new FunctionTester<Provider>();
        var designer = (FunctionDesigner)tester.CreateDesigner();
        designer.Properties[PropertyIds.ReadFormData].Value = true;
        designer.Properties[PropertyIds.FormDataSpecificationType].Value = FormDataSpecificationType.Infer;
        string inferPdfFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Read.Resources.InferFieldsAcro.pdf", this.inputFolderPath);
        designer.Properties[PropertyIds.SamplePdf].Value = inferPdfFilePath;
        designer.Properties[PropertyIds.PropertyMappingOverrides].Value = new PropertyMapping
        {
            new PropertyMappingEntry("Surname", "LastName")
        }.ToString();

        var resultTypeProperties = designer.Result.GetProperties();
        resultTypeProperties.Single().AssertGenerated(OutputNames.FormData);
        var formDataTypeProperties = resultTypeProperties.Single().TypeReference.GetProperties();
        Assert.That(
            formDataTypeProperties.Select(p => p.Name), Is.EquivalentTo(new[] { "First_32Name", "LastName" }));
    }

    [Test]
    public void ReadFormDataResultTypeWithSampleXfaFieldsPdfAndPropertyMapping()
    {
        var tester = new FunctionTester<Provider>();
        var designer = (FunctionDesigner)tester.CreateDesigner();
        designer.Properties[PropertyIds.ReadFormData].Value = true;
        designer.Properties[PropertyIds.FormDataSpecificationType].Value = FormDataSpecificationType.Infer;
        string inferPdfFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Read.Resources.InferFieldsXFA.pdf", this.inputFolderPath);
        designer.Properties[PropertyIds.SamplePdf].Value = inferPdfFilePath;
        designer.Properties[PropertyIds.PropertyMappingOverrides].Value = new PropertyMapping
        {
            new PropertyMappingEntry("Surname", "LastName")
        }.ToString();

        var resultTypeProperties = designer.Result.GetProperties();
        resultTypeProperties.Single().AssertGenerated(OutputNames.FormData);
        var formDataTypeProperties = resultTypeProperties.Single().TypeReference.GetProperties();
        Assert.That(
            formDataTypeProperties.Select(p => p.Name), Is.EquivalentTo(new[] { "FullName", "LastName", "Email", "EmailMe" }));
    }

    [Test]
    public void ReadFormDataResultTypeWithList()
    {
        var tester = new FunctionTester<Provider>();
        var designer = (FunctionDesigner)tester.CreateDesigner();
        designer.Properties[PropertyIds.ReadFormData].Value = true;
        designer.Properties[PropertyIds.FormDataSpecificationType].Value = FormDataSpecificationType.List;

        var resultTypeProperties = designer.Result.GetProperties();
        resultTypeProperties.Single().AssertList(OutputNames.FormDataList, typeof(KeyValuePair<string, string>));
    }

    [Test]
    public void ReadSignatureResultType()
    {
        var tester = new FunctionTester<Provider>();
        var designer = (FunctionDesigner)tester.CreateDesigner();
        designer.Properties[PropertyIds.ReadSignature].Value = true;

        var properties = designer.Result.GetProperties();
        var signatureInfoProperty = properties.Single();
        signatureInfoProperty.AssertCompiled(OutputNames.Signatures, typeof(SignatureInfo));
    }

    [Test]
    public void ExecuteReadWithNoInputFileSpecified([Values(null, "")] string inputFilePath)
    {
        var propertyValues = new FunctionPropertyValues();
        var inputPdfPropertyGroup = new PdfFilePropertyGroup(Shared.Common.Constants.CategoryNames.Input);
        propertyValues.AddRuntime(inputPdfPropertyGroup.FilePathPropertyId, inputFilePath);
        propertyValues.AddDesignTime(inputPdfPropertyGroup.AuthenticationTypePropertyId, AuthenticationType.None);

        Assert.That(() => new FunctionTester<Provider>().Execute(propertyValues),
            Throws.Exception.TypeOf<ExecuteException>()
            .With.Property(nameof(ExecuteException.Message)).StartsWith("Value cannot be null. (Parameter 'filePath')\r\nSee Code and Parameter properties for more information."));
    }

    [Test]
    public void ExecuteReadWithNonExistentInputFileSpecified()
    {
        string nonExistentFilePath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
        Assert.That(File.Exists(nonExistentFilePath), Is.False);

        var propertyValues = new FunctionPropertyValues();
        var inputPdfPropertyGroup = new PdfFilePropertyGroup(Shared.Common.Constants.CategoryNames.Input);
        propertyValues.AddRuntime(inputPdfPropertyGroup.FilePathPropertyId, nonExistentFilePath);
        propertyValues.AddDesignTime(inputPdfPropertyGroup.AuthenticationTypePropertyId, AuthenticationType.None);

        Assert.That(() => new FunctionTester<Provider>().Execute(propertyValues),
            Throws.Exception.TypeOf<ExecuteException>()
            .With.Property(nameof(ExecuteException.Message)).StartsWith($"{nonExistentFilePath} not found as file or resource.\r\nSee Code and Parameter properties for more information."));
    }

    [Test]
    public void ExecuteReadWithNoOutput(
        [Values(
            FileAuthentication.None,
            FileAuthentication.Password,
            FileAuthentication.CertificateFile)] FileAuthentication inputFileAuthentication)
    {
        string inputFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Read.Resources.Blank.pdf", this.inputFolderPath);
        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, inputFileAuthentication, inputFilePath);
        propertyValues.AddDesignTime(PropertyIds.ReadText, false);
        propertyValues.AddDesignTime(PropertyIds.ReadFormData, false);
        propertyValues.AddDesignTime(PropertyIds.ReadSignature, false);

        var tester = new FunctionTester<Provider>();
        Assert.That(() => tester.Execute(propertyValues), Throws.Nothing);
    }

    [Test]
    public void ExecuteReadTextNoSplits()
    {
        string inputFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Read.Resources.Text.pdf", this.inputFolderPath);
        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, FileAuthentication.None, inputFilePath);
        propertyValues.AddDesignTime(PropertyIds.ReadText, true);
        propertyValues.AddDesignTime(PropertyIds.SplitText, TextSplit.Never);

        var tester = new FunctionTester<Provider>();
        var result = tester.Execute(propertyValues);

        Assert.That(result.Value.Text, Is.EqualTo("Text on page 1\r\nFooter text on page 1\r\nText on page 2\r\nText on page 3"));
    }

    [Test]
    public void ExecuteReadTextSplitPages()
    {
        string inputFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Read.Resources.Text.pdf", this.inputFolderPath);
        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, FileAuthentication.None, inputFilePath);
        propertyValues.AddDesignTime(PropertyIds.ReadText, true);
        propertyValues.AddDesignTime(PropertyIds.SplitText, TextSplit.Page);

        var tester = new FunctionTester<Provider>();
        var result = tester.Execute(propertyValues);

        Assert.That(result.Value.Text, Is.EqualTo(new[] { "Text on page 1\r\nFooter text on page 1", "Text on page 2", "Text on page 3" }));
    }

    [TestCase(TextExtractionStrategy.Location, " \nLeft Right \nL1 R2 ")]
    [TestCase(TextExtractionStrategy.Simple, " \nLeft \nL1 \nRight \nR2 ")]
    [TestCase(TextExtractionStrategy.TopToBottom, " \r\nLeft Right \r\nL1 R2 ")]
    public void ExecuteReadTextWithTextExtractionStrategy(TextExtractionStrategy extractionStrategy, string expectedValue)
    {
        string inputFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Read.Resources.TextBoxes.pdf", this.inputFolderPath);
        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, FileAuthentication.None, inputFilePath);
        propertyValues.AddDesignTime(PropertyIds.ReadText, true);
        propertyValues.AddDesignTime(PropertyIds.SplitText, TextSplit.Never);
        propertyValues.AddDesignTime(PropertyIds.ExtractionStrategy, extractionStrategy);

        var tester = new FunctionTester<Provider>();
        var result = tester.Execute(propertyValues);
        Assert.That(result.Value.Text, Is.EqualTo(expectedValue));
    }

    [Test]
    public void ExecuteReadFormDataWithListOutputAndDocumentWithNoForm()
    {
        string inputFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Read.Resources.NoForm.pdf", this.inputFolderPath);
        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, FileAuthentication.None, inputFilePath);
        propertyValues.AddDesignTime(PropertyIds.ReadFormData, true);
        propertyValues.AddDesignTime(PropertyIds.FormDataSpecificationType, FormDataSpecificationType.List);

        Assert.That(() => new FunctionTester<Provider>().Execute(propertyValues),
            Throws.Exception.TypeOf<ExecuteException>()
            .With.Property(nameof(ExecuteException.Message)).StartsWith("The input document does not contain a form.\r\nSee Code and Parameter properties for more information."));
    }

    [Test]
    public void ExecuteReadFormDataWithCustomTypeOutputAndDocumentWithNoForm()
    {
        string inputFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Read.Resources.NoForm.pdf", this.inputFolderPath);
        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, FileAuthentication.None, inputFilePath);
        propertyValues.AddDesignTime(PropertyIds.ReadFormData, true);
        propertyValues.AddDesignTime(PropertyIds.FormDataSpecificationType, FormDataSpecificationType.CustomType);
        propertyValues.AddDesignTime(PropertyIds.FormDataType, TypeReference.CreateGenerated());

        Assert.That(() => new FunctionTester<Provider>().Execute(propertyValues),
            Throws.Exception.TypeOf<ExecuteException>()
            .With.Property(nameof(ExecuteException.Message)).StartsWith("The input document does not contain a form.\r\nSee Code and Parameter properties for more information."));
    }

    [Test]
    public void ExecuteReadAcroFormDataWithCustomTypeOutput()
    {
        string inputFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Read.Resources.FormDataAcro.pdf", this.inputFolderPath);
        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, FileAuthentication.None, inputFilePath);
        propertyValues.AddDesignTime(PropertyIds.ReadFormData, true);

        var dataType = TypeReference.CreateGenerated(
            TypeReference.CreateTypeProperty("FirstName", typeof(string)),
            TypeReference.CreateTypeProperty("LastName", typeof(string)),
            TypeReference.CreateTypeProperty("Gender", typeof(string)),
            TypeReference.CreateTypeProperty("AcceptTCs", typeof(bool)));

        propertyValues.AddDesignTime(PropertyIds.FormDataSpecificationType, FormDataSpecificationType.CustomType);
        propertyValues.AddDesignTime(PropertyIds.FormDataType, dataType);
        propertyValues.AddDesignTime(PropertyIds.PropertyMappingOverrides, new PropertyMapping
        {
            new PropertyMappingEntry("First Name", "FirstName"),
            new PropertyMappingEntry("Surname", "LastName")
        }.ToString());

        var tester = new FunctionTester<Provider>();
        tester.CustomTypes.Add(dataType);
        var result = tester.Execute(propertyValues).Value;

        Assert.That(result.FormData.FirstName, Is.EqualTo("Jeremy"));
        Assert.That(result.FormData.LastName, Is.EqualTo("Woods"));
        Assert.That(result.FormData.Gender, Is.EqualTo("Male"));
        Assert.That(result.FormData.AcceptTCs, Is.True);
    }

    [Test]
    public void ExecuteReadXfaFormDataWithCustomTypeOutput()
    {
        string inputFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Read.Resources.FormDataXFA.pdf", this.inputFolderPath);
        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, FileAuthentication.None, inputFilePath);
        propertyValues.AddDesignTime(PropertyIds.ReadFormData, true);

        var dataType = TypeReference.CreateGenerated(
            TypeReference.CreateTypeProperty("FullName", typeof(string)),
            TypeReference.CreateTypeProperty("LastName", typeof(string)),
            TypeReference.CreateTypeProperty("Email", typeof(string)),
            TypeReference.CreateTypeProperty("EmailMe", typeof(bool)));

        propertyValues.AddDesignTime(PropertyIds.FormDataSpecificationType, FormDataSpecificationType.CustomType);
        propertyValues.AddDesignTime(PropertyIds.FormDataType, dataType);
        propertyValues.AddDesignTime(PropertyIds.PropertyMappingOverrides, new PropertyMapping
        {
            new PropertyMappingEntry("Surname", "LastName")
        }.ToString());

        var tester = new FunctionTester<Provider>();
        tester.CustomTypes.Add(dataType);
        var result = tester.Execute(propertyValues).Value;

        Assert.That(result.FormData.FullName, Is.EqualTo("John"));
        Assert.That(result.FormData.LastName, Is.EqualTo("Doe"));
        Assert.That(result.FormData.Email, Is.EqualTo("jdoe@digiata.com"));
        Assert.That(result.FormData.EmailMe, Is.True);
    }

    [Test]
    public void ExecuteReadAcroFormDataWithInferredOutput()
    {
        string inputFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Read.Resources.FormDataAcro.pdf", this.inputFolderPath);
        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, FileAuthentication.None, inputFilePath);
        propertyValues.AddDesignTime(PropertyIds.ReadFormData, true);

        propertyValues.AddDesignTime(PropertyIds.FormDataSpecificationType, FormDataSpecificationType.Infer);
        string inferFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Read.Resources.FormDataAcro.pdf", this.inputFolderPath);
        propertyValues.AddDesignTime(PropertyIds.SamplePdf, inferFilePath);
        propertyValues.AddDesignTime(PropertyIds.PropertyMappingOverrides, new PropertyMapping
        {
            new PropertyMappingEntry("Surname", "LastName")
        }.ToString());

        var tester = new FunctionTester<Provider>();
        var result = tester.Execute(propertyValues).Value;

        Assert.That(result.FormData.First_32Name, Is.EqualTo("Jeremy"));
        Assert.That(result.FormData.LastName, Is.EqualTo("Woods"));
        Assert.That(result.FormData.Gender, Is.EqualTo("Male"));
        Assert.That(result.FormData.AcceptTCs, Is.EqualTo("Yes"));
    }

    [Test]
    public void ExecuteReadXfaFormDataWithInferredOutput()
    {
        string inputFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Read.Resources.FormDataXFA.pdf", this.inputFolderPath);
        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, FileAuthentication.None, inputFilePath);
        propertyValues.AddDesignTime(PropertyIds.ReadFormData, true);

        propertyValues.AddDesignTime(PropertyIds.FormDataSpecificationType, FormDataSpecificationType.Infer);
        string inferFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Read.Resources.InferFieldsXFA.pdf", this.inputFolderPath);
        propertyValues.AddDesignTime(PropertyIds.SamplePdf, inferFilePath);
        propertyValues.AddDesignTime(PropertyIds.PropertyMappingOverrides, new PropertyMapping
        {
            new PropertyMappingEntry("Surname", "LastName")
        }.ToString());

        var tester = new FunctionTester<Provider>();
        var result = tester.Execute(propertyValues).Value;

        Assert.That(result.FormData.FullName, Is.EqualTo("John"));
        Assert.That(result.FormData.LastName, Is.EqualTo("Doe"));
        Assert.That(result.FormData.Email, Is.EqualTo("jdoe@digiata.com"));
        Assert.That(result.FormData.EmailMe, Is.EqualTo("1"));
    }

    [Test]
    public void ExecuteReadAcroFormDataWithListOutput()
    {
        string inputFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Read.Resources.FormDataAcro.pdf", this.inputFolderPath);
        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, FileAuthentication.None, inputFilePath);
        propertyValues.AddDesignTime(PropertyIds.ReadFormData, true);
        propertyValues.AddDesignTime(PropertyIds.FormDataSpecificationType, FormDataSpecificationType.List);

        var tester = new FunctionTester<Provider>();
        var result = tester.Execute(propertyValues).Value;

        List<KeyValuePair<string, string>> dataList = result.FormDataList;
        Assert.That(
            dataList.Select(entry => $"{entry.Key}: {entry.Value}"), Is.EquivalentTo(new[] { "First Name: Jeremy", "Surname: Woods", "Gender: Male", "AcceptTCs: Yes" }));
    }

    [Test]
    public void ExecuteReadXfaFormDataWithListOutput()
    {
        string inputFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Read.Resources.FormDataXFA.pdf", this.inputFolderPath);
        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, FileAuthentication.None, inputFilePath);
        propertyValues.AddDesignTime(PropertyIds.ReadFormData, true);
        propertyValues.AddDesignTime(PropertyIds.FormDataSpecificationType, FormDataSpecificationType.List);

        var tester = new FunctionTester<Provider>();
        var result = tester.Execute(propertyValues).Value;

        List<KeyValuePair<string, string>> dataList = result.FormDataList;
        Assert.That(
            dataList.Select(entry => $"{entry.Key}: {entry.Value}"), Is.EquivalentTo(new[] { "FullName: John", "Surname: Doe", "Email: jdoe@digiata.com", "EmailMe: 1" }));
    }

    [Test]
    public void ExecuteReadSignature()
    {
        var expectedSignature1 = new Signature(
            SignedBy: "John Smith",
            SignedAt: "Office location 1",
            SignedOn: new DateTime(2021, 10, 19, 13, 11, 2, DateTimeKind.Utc).ToLocalTime(),
            Reason: "I created the doc",
            Unmodified: true,
            SignedVersion: 1,
            IsLatestVersionSigned: false,
            Verified: true,
            VerificationMessage: string.Empty);
        var expectedSignature2 = new Signature(
            SignedBy: "Jane Doe",
            SignedAt: "Office location 2",
            SignedOn: new DateTime(2021, 10, 19, 13, 12, 12, DateTimeKind.Utc).ToLocalTime(),
            Reason: "I moderated the doc",
            Unmodified: true,
            SignedVersion: 1,
            IsLatestVersionSigned: false,
            Verified: false,
            VerificationMessage: "A certificate chain processed, but terminated in a root certificate which is not trusted by the trust provider.");

        using var store = new X509Store(StoreName.TrustedPeople, StoreLocation.CurrentUser);
        store.Open(OpenFlags.ReadWrite);
        store.Add(this.authenticationManager.Certificate);
        store.Close();

        string inputFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Read.Resources.Signature.pdf", this.inputFolderPath);
        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, FileAuthentication.None, inputFilePath);
        propertyValues.AddDesignTime(PropertyIds.ReadSignature, true);

        var tester = new FunctionTester<Provider>();
        var result = tester.Execute(propertyValues).Value;

        SignatureInfo signatureInfo = result.Signatures;
        Assert.That(signatureInfo.IsSigned, Is.True);
        Assert.That(signatureInfo.LatestSignature, Is.EqualTo(expectedSignature2));
        Assert.That(signatureInfo.AllSignatures, Is.EqualTo(new[] { expectedSignature1, expectedSignature2 }).AsCollection);

        store.Open(OpenFlags.ReadWrite);
        store.Remove(this.authenticationManager.Certificate);
        store.Close();
    }

    [Test]
    public void ExecuteReadSignatureWithUnsignedDocument()
    {
        string blankPdfFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Read.Resources.Blank.pdf", this.inputFolderPath);

        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, FileAuthentication.None, blankPdfFilePath);
        propertyValues.AddDesignTime(PropertyIds.ReadSignature, true);

        var tester = new FunctionTester<Provider>();
        var result = tester.Execute(propertyValues).Value;

        SignatureInfo signatureInfo = result.Signatures;
        Assert.That(signatureInfo.IsSigned, Is.False);
        Assert.That(signatureInfo.AllSignatures, Is.Empty);
        Assert.That(signatureInfo.LatestSignature, Is.Null);
    }

    private void ConfigureInputFileFunctionValues(FunctionPropertyValues propertyValues, FileAuthentication inputAuth, string inputFilePath)
    {
        ConfigureInputFileFunctionValues(
            propertyValues,
            inputAuth,
            inputFilePath,
            Shared.Common.Constants.CategoryNames.Input);
    }
}
