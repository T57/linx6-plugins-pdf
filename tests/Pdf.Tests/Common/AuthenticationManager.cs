﻿using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using iText.Bouncycastle.Crypto;
using iText.Bouncycastle.X509;
using iText.Kernel.Pdf;
using Org.BouncyCastle.Security;
using Twenty57.Linx.Plugins.Pdf.Tests.Helpers;

namespace Twenty57.Linx.Plugins.Pdf.Tests;

internal class AuthenticationManager : IDisposable
{
    public AuthenticationManager(string password = "secret")
    {
        Password = password;
        CertificateFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Common.Resources.John Smith.pfx", Path.GetTempPath());
        CertificateFilePassword = "secret";
        Certificate = new X509Certificate2(CertificateFilePath, CertificateFilePassword, X509KeyStorageFlags.Exportable);
    }

    public void Dispose()
    {
        if (File.Exists(CertificateFilePath))
        {
            File.Delete(CertificateFilePath);
        }
    }

    public string Password { get; }
    public string CertificateFilePath { get; }
    public string CertificateFilePassword { get; }
    public X509Certificate2 Certificate { get; }

    public void Protect(string filePath, FileAuthentication fileAuthentication)
    {
        const int FullPermissions =
            EncryptionConstants.ALLOW_ASSEMBLY
            | EncryptionConstants.ALLOW_COPY
            | EncryptionConstants.ALLOW_DEGRADED_PRINTING
            | EncryptionConstants.ALLOW_FILL_IN
            | EncryptionConstants.ALLOW_MODIFY_ANNOTATIONS
            | EncryptionConstants.ALLOW_MODIFY_CONTENTS
            | EncryptionConstants.ALLOW_PRINTING
            | EncryptionConstants.ALLOW_SCREENREADERS;

        CheckFileExists(filePath);

        if (fileAuthentication == FileAuthentication.None)
        {
            return;
        }

        string outputFilePath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
        using (var pdfReader = new PdfReader(filePath))
        using (var outputStream = new FileStream(outputFilePath, FileMode.CreateNew, FileAccess.Write, FileShare.None))
        {
            var writerProperties = new WriterProperties();

            switch (fileAuthentication)
            {
                case FileAuthentication.Password:
                    byte[] password = Encoding.UTF8.GetBytes(Password);
                    writerProperties.SetStandardEncryption(password, password, FullPermissions, EncryptionConstants.ENCRYPTION_AES_256);
                    break;
                case FileAuthentication.CertificateFile:
                    var certificates = new[] { new X509CertificateBC(DotNetUtilities.FromX509Certificate(new X509Certificate2(CertificateFilePath, CertificateFilePassword))) };
                    writerProperties.SetPublicKeyEncryption(certificates, [FullPermissions], EncryptionConstants.ENCRYPTION_AES_256);
                    break;
                default:
                    throw new Exception("Unhandled FileAuthentication type: " + fileAuthentication);
            }

            using var pdfWriter = new PdfWriter(outputStream, writerProperties);
            using var outputPdfDocument = new PdfDocument(pdfReader, pdfWriter);
        }

        File.Delete(filePath);
        File.Move(outputFilePath, filePath);
    }

    public PdfReader GetReader(string filePath, FileAuthentication fileAuthentication)
    {
        CheckFileExists(filePath);

        switch (fileAuthentication)
        {
            case FileAuthentication.None:
                return new PdfReader(filePath);
            case FileAuthentication.Password:
                return new PdfReader(filePath, new ReaderProperties().SetPassword(Encoding.UTF8.GetBytes(Password)));
            case FileAuthentication.CertificateFile:
                if (!Certificate.HasPrivateKey)
                {
                    throw new NotSupportedException("Certificate must have a private key.");
                }

                var bouncyCertificate = new X509CertificateBC(DotNetUtilities.FromX509Certificate(Certificate));
                var keyPair = DotNetUtilities.GetKeyPair(Certificate.GetRSAPrivateKey());
                var readerProperties = new ReaderProperties()
                    .SetPublicKeySecurityParams(bouncyCertificate, new PrivateKeyBC(keyPair.Private));
                return new PdfReader(filePath, readerProperties);
            default:
                throw new NotSupportedException("Invalid FileAuthentication specified.");
        }
    }

    private static void CheckFileExists(string filePath)
    {
        if (!File.Exists(filePath))
        {
            throw new FileNotFoundException($"File [{filePath}] does not exist.");
        }
    }
}
