﻿using System;
using System.IO;
using NUnit.Framework;
using Twenty57.Linx.Plugins.Pdf.Design.Common;
using Twenty57.Linx.Plugins.Pdf.Shared.Common;
using Twenty57.Linx.Plugins.Pdf.Shared.Common.Constants;

namespace Twenty57.Linx.Plugins.Pdf.Tests.Common;

internal abstract class TestPdfBase
{
    protected PdfFilePropertyGroup inputPdfPropertyGroup = new(CategoryNames.Input);
    protected AuthenticationManager authenticationManager;
    protected string inputFolderPath;

    [OneTimeSetUp]
    public void OneTimeSetupBase()
    {
        this.authenticationManager = new();
    }

    [OneTimeTearDown]
    public void OneTimeTearDownBase()
    {
        this.authenticationManager.Dispose();
    }

    [SetUp]
    public void SetUpBase()
    {
        this.inputFolderPath = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString("N"));
        Directory.CreateDirectory(this.inputFolderPath);
    }

    [TearDown]
    public void TearDownBase()
    {
        Directory.Delete(this.inputFolderPath, true);
    }

    protected void ConfigureInputFileFunctionValues(
        FunctionPropertyValues propertyValues,
        FileAuthentication inputFileAuthentication,
        string inputFilePath,
        string categoryName)
    {
        this.authenticationManager.Protect(inputFilePath, inputFileAuthentication);

        var inputPdfPropertyGroup = new PdfFilePropertyGroup(categoryName);
        propertyValues.AddRuntime(inputPdfPropertyGroup.FilePathPropertyId, inputFilePath);

        switch (inputFileAuthentication)
        {
            case FileAuthentication.None:
                propertyValues.AddDesignTime(inputPdfPropertyGroup.AuthenticationTypePropertyId, AuthenticationType.None);
                break;
            case FileAuthentication.Password:
                propertyValues.AddDesignTime(inputPdfPropertyGroup.AuthenticationTypePropertyId, AuthenticationType.Password);
                propertyValues.AddRuntime(inputPdfPropertyGroup.PasswordPropertyId, this.authenticationManager.Password);
                break;
            case FileAuthentication.CertificateFile:
                propertyValues.AddDesignTime(inputPdfPropertyGroup.AuthenticationTypePropertyId, AuthenticationType.Certificate);
                propertyValues.AddRuntime(inputPdfPropertyGroup.CertificateFilePathPropertyId, this.authenticationManager.CertificateFilePath);
                propertyValues.AddRuntime(inputPdfPropertyGroup.CertificateFilePasswordPropertyId, this.authenticationManager.Password);
                break;
            default:
                throw new NotSupportedException("Invalid FileAuthentication specified.");
        }
    }
}
