﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Xml.Linq;
using System.Xml.Serialization;
using iText.Forms;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas.Parser;
using iText.Signatures;
using NUnit.Framework;

namespace Twenty57.Linx.Plugins.Pdf.Tests;

internal class PdfComparer : IDisposable
{
    private readonly PdfDocument pdfDocument;

    private PdfComparer(string filePath, FileAuthentication fileAuthentication, AuthenticationManager authenticationManager)
    {
        this.pdfDocument = new PdfDocument(authenticationManager.GetReader(filePath, fileAuthentication));
    }

    public void Dispose()
    {
        ((IDisposable)this.pdfDocument).Dispose();
    }

    public static void AssertPageCount(string filePath, FileAuthentication fileAuthentication, AuthenticationManager authenticationManager,
        int expectedNumberOfPages)
    {
        using var comparer = new PdfComparer(filePath, fileAuthentication, authenticationManager);
        Assert.That(comparer.pdfDocument.GetNumberOfPages(), Is.EqualTo(expectedNumberOfPages));
    }

    public static void AssertText(
        string filePath,
        FileAuthentication fileAuthentication,
        AuthenticationManager authenticationManager,
        IEnumerable<string> expectedPageTexts)
    {
        using var comparer = new PdfComparer(filePath, fileAuthentication, authenticationManager);
        var pageTexts = Enumerable.Range(1, comparer.pdfDocument.GetNumberOfPages())
            .Select(pageNumber => PdfTextExtractor.GetTextFromPage(comparer.pdfDocument.GetPage(pageNumber)));

        Assert.That(pageTexts, Is.EqualTo(expectedPageTexts).AsCollection);
    }

    public static void AssertJavaScript(
        string filePath,
        FileAuthentication fileAuth,
        AuthenticationManager authenticationManager,
        IEnumerable<string> expectedJavaScript)
    {
        using var comparer = new PdfComparer(filePath, fileAuth, authenticationManager);
        var javaScriptCodes = comparer.pdfDocument.GetCatalog().GetNameTree(PdfName.JavaScript).GetNames()
            .Values.Cast<PdfDictionary>().Select(d => d.Get(PdfName.JS))
            .Cast<PdfString>().Select(s => s.GetValue());
        Assert.That(javaScriptCodes, Is.EquivalentTo(expectedJavaScript));
    }

    public static void AssertAcroFields(
        string filePath,
        FileAuthentication fileAuthentication,
        AuthenticationManager authenticationManager,
        params (string FieldName, string FieldValue)[] expectedFieldValues)
    {
        using var comparer = new PdfComparer(filePath, fileAuthentication, authenticationManager);
        var form = PdfAcroForm.GetAcroForm(comparer.pdfDocument, createIfNotExist: false);

        var fieldValues = form.GetAllFormFields()
            .Select(field => $"{field.Key}: [{field.Value.GetValueAsString()}]");
        Assert.That(
            fieldValues, Is.EquivalentTo(expectedFieldValues.Select(field => $"{field.FieldName}: [{field.FieldValue}]")));
    }

    public static void AssertXfaFormData<T>(
        string filePath,
        FileAuthentication fileAuthentication,
        AuthenticationManager authenticationManager,
        T expectedFormData)
    {
        using var comparer = new PdfComparer(filePath, fileAuthentication, authenticationManager);
        var form = PdfAcroForm.GetAcroForm(comparer.pdfDocument, createIfNotExist: false);
        var xfaForm = form.GetXfaForm();
        Assert.That(xfaForm?.IsXfaPresent(), Is.True, "The document does not contain an XFA form.");

        var formDataNode = xfaForm.GetDatasetsNode()!
            .Element(XName.Get("data", @"http://www.xfa.org/schema/xfa-data/1.0/"))!
            .Elements().Single();
        formDataNode.Name = XName.Get(typeof(T).Name, formDataNode.Name.NamespaceName);

        var xmlSerializer = new XmlSerializer(typeof(T));
        using var xmlReader = formDataNode.CreateReader();
        var formData = (T)xmlSerializer.Deserialize(xmlReader);

        Assert.That(JsonSerializer.Serialize(formData), Is.EqualTo(JsonSerializer.Serialize(expectedFormData)));
    }

    public static void AssertProtectionAllPermissions(string filePath, FileAuthentication fileAuthentication, AuthenticationManager authenticationManager, bool expectedDontEncryptMetadata = true)
    {
        AssertProtection(filePath, fileAuthentication, authenticationManager, expectedDontEncryptMetadata, true, true, true, true, true, true, true, true);
    }

    public static void AssertProtection(string filePath, FileAuthentication fileAuthentication, AuthenticationManager authenticationManager,
        bool expectedDontEncryptMetadata = true,
        bool expectedAllowAssembly = false,
        bool expectedAllowCopy = false,
        bool expectedAllowDegradedPrinting = false,
        bool expectedAllowFillIn = false,
        bool expectedAllowModifyAnnotations = false,
        bool expectedAllowModifyContents = false,
        bool expectedAllowPrinting = false,
        bool expectedAllowScreenReaders = false)
    {
        using var comparer = new PdfComparer(filePath, fileAuthentication, authenticationManager);
        var reader = comparer.pdfDocument.GetReader();

        Assert.That(reader.IsEncrypted(), Is.True);
        Assert.That((reader.GetCryptoMode() & EncryptionConstants.DO_NOT_ENCRYPT_METADATA) != 0, Is.EqualTo(expectedDontEncryptMetadata));

        if (expectedAllowAssembly && expectedAllowCopy && expectedAllowDegradedPrinting && expectedAllowFillIn &&
            expectedAllowModifyAnnotations && expectedAllowModifyContents && expectedAllowPrinting && expectedAllowScreenReaders)
        {
            Assert.That(reader.IsOpenedWithFullPermission(), Is.True);
        }
        else
        {
            int permissions = (int)reader.GetPermissions();
            Assert.That(PdfEncryptor.IsAssemblyAllowed(permissions), Is.EqualTo(expectedAllowAssembly));
            Assert.That(PdfEncryptor.IsCopyAllowed(permissions), Is.EqualTo(expectedAllowCopy));
            Assert.That(PdfEncryptor.IsDegradedPrintingAllowed(permissions), Is.EqualTo(expectedAllowDegradedPrinting));
            Assert.That(PdfEncryptor.IsFillInAllowed(permissions), Is.EqualTo(expectedAllowFillIn));
            Assert.That(PdfEncryptor.IsModifyAnnotationsAllowed(permissions), Is.EqualTo(expectedAllowModifyAnnotations));
            Assert.That(PdfEncryptor.IsModifyContentsAllowed(permissions), Is.EqualTo(expectedAllowModifyContents));
            Assert.That(PdfEncryptor.IsPrintingAllowed(permissions), Is.EqualTo(expectedAllowPrinting));
            Assert.That(PdfEncryptor.IsScreenReadersAllowed(permissions), Is.EqualTo(expectedAllowScreenReaders));
        }
    }

    public static void AssertPageSignature(string filePath, FileAuthentication fileAuth, AuthenticationManager authenticationManager,
        string expectedSignName, string expectedLocation, string expectedReason, bool expectedLockDocument,
        int expectedPage, int expectedLeftMillimeters, int expectedBottomMillimeters, int expectedWidthMillimeters, int expectedHeightMillimeters)
    {
        using var comparer = new PdfComparer(filePath, fileAuth, authenticationManager);
        string signatureName = comparer.GetSignatureName();
        comparer.AssertIsLocked(expectedLockDocument);
        comparer.AssertSignatureDetails(signatureName, expectedSignName, expectedLocation, expectedReason);
        comparer.AssertSignaturePosition(signatureName, expectedPage, expectedLeftMillimeters, expectedBottomMillimeters, expectedWidthMillimeters, expectedHeightMillimeters);
    }

    public static void AssertFieldSignature(string filePath, FileAuthentication fileAuth, AuthenticationManager authenticationManager,
        string expectedFieldName, string expectedSignName, string expectedLocation, string expectedReason, bool expectedLockDocument = false)
    {
        using var comparer = new PdfComparer(filePath, fileAuth, authenticationManager);
        string signatureName = comparer.GetSignatureName();
        Assert.That(signatureName, Is.EqualTo(expectedFieldName));
        comparer.AssertIsLocked(expectedLockDocument);
        comparer.AssertSignatureDetails(signatureName, expectedSignName, expectedLocation, expectedReason);
    }

    private string GetSignatureName()
    {
        var signatureNames = new SignatureUtil(this.pdfDocument).GetSignatureNames();
        if (!signatureNames.Any())
        {
            throw new Exception("No signatures found.");
        }

        if (signatureNames.Count > 1)
        {
            throw new NotSupportedException("Multiple signatures not supported.");
        }

        return signatureNames.First();
    }

    private void AssertIsLocked(bool expectedLockDocument)
    {
        Assert.That(IsDocumentLocked(), Is.EqualTo(expectedLockDocument));
    }

    private bool IsDocumentLocked()
    {
        // Adapted from https://stackoverflow.com/a/58418033/359765
        var signatureDictionary = new SignatureUtil(this.pdfDocument).GetSignatureDictionary(GetSignatureName());

        var referenceArray = signatureDictionary.GetAsArray(PdfName.Reference);
        if (referenceArray == null || referenceArray.Size() == 0)
        {
            return false;
        }

        foreach (PdfObject referenceArrayObject in referenceArray)
        {
            PdfObject referenceObject = referenceArrayObject;
            if (referenceObject.IsIndirectReference())
            {
                referenceObject = ((PdfIndirectReference)referenceObject).GetRefersTo(true);
            }

            if (!referenceObject.IsDictionary())
            {
                continue;
            }
            PdfDictionary reference = (PdfDictionary)referenceObject;
            PdfDictionary transformParams = reference.GetAsDictionary(PdfName.TransformParams);
            PdfNumber p = transformParams.GetAsNumber(PdfName.P);
            if (p?.IntValue() == 1)
            {
                return true;
            }
        }

        return false;
    }

    private void AssertSignatureDetails(string signatureName,
        string expectedSignName, string expectedLocation, string expectedReason)
    {
        var signatureData = new SignatureUtil(this.pdfDocument).ReadSignatureData(signatureName);
        var signingCertificate = signatureData.GetSigningCertificate();
        Assert.That(CertificateInfo.GetSubjectFields(signingCertificate).GetField("CN"), Is.EqualTo(expectedSignName));
        Assert.That(signatureData.GetLocation(), Is.EqualTo(expectedLocation));
        Assert.That(signatureData.GetReason(), Is.EqualTo(expectedReason));
        Assert.That(signatureData.GetSignDate(), Is.EqualTo(DateTime.Now).Within(1).Minutes);
    }

    private void AssertSignaturePosition(string signatureName,
        int expectedPage, int expectedLeftMillimeters, int expectedBottomMillimeters, int expectedWidthMillimeters, int expectedHeightMillimeters)
    {
        var form = PdfAcroForm.GetAcroForm(this.pdfDocument, createIfNotExist: false);
        var signatureField = form.GetField(signatureName);

        var signatureWidget = signatureField.GetWidgets().Single();
        Assert.That(this.pdfDocument.GetPageNumber(signatureWidget.GetPage()), Is.EqualTo(expectedPage));

        PdfArray rectangle = signatureWidget.GetRectangle();
        double left = ((PdfNumber)rectangle.Get(0)).GetValue();
        double bottom = ((PdfNumber)rectangle.Get(1)).GetValue();
        double right = ((PdfNumber)rectangle.Get(2)).GetValue();
        double top = ((PdfNumber)rectangle.Get(3)).GetValue();

        static float MillimetersToPoints(int millimeters) => millimeters * 72 / 25.4f;

        Assert.That(Math.Round(left), Is.EqualTo(Math.Round(MillimetersToPoints(expectedLeftMillimeters))),
            "Unexpected signature left position.");
        Assert.That(Math.Round(bottom), Is.EqualTo(Math.Round(MillimetersToPoints(expectedBottomMillimeters))),
            "Unexpected signature bottom position.");
        Assert.That(Math.Round(right - left), Is.EqualTo(Math.Round(MillimetersToPoints(expectedWidthMillimeters))),
            "Unexpected signature width.");
        Assert.That(Math.Round(top - bottom), Is.EqualTo(Math.Round(MillimetersToPoints(expectedHeightMillimeters))),
            "Unexpected signature height.");
    }
}
