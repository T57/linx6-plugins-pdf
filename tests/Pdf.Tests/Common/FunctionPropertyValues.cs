﻿using System.Collections.Generic;
using Twenty57.Linx.Sdk.TestKit;

namespace Twenty57.Linx.Plugins.Pdf.Tests.Common;

internal sealed class FunctionPropertyValues
{
    private readonly IList<PropertyValue> propertyValues = new List<PropertyValue>();
    private readonly IList<RuntimePropertyValue> runtimePropertyValues = new List<RuntimePropertyValue>();

    public IEnumerable<PropertyValue> PropertyValues => this.propertyValues;

    public IEnumerable<RuntimePropertyValue> RuntimePropertyValues => this.runtimePropertyValues;

    public void AddDesignTime(string propertyId, object value)
    {
        this.propertyValues.Add(new PropertyValue(propertyId, value));
    }

    public void AddRuntime(string propertyId, object value)
    {
        this.runtimePropertyValues.Add(new RuntimePropertyValue(propertyId, value));
    }
}
