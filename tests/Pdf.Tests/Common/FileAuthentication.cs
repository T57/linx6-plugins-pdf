﻿namespace Twenty57.Linx.Plugins.Pdf.Tests;

internal enum FileAuthentication
{
    None,
    Password,
    CertificateFile
};
