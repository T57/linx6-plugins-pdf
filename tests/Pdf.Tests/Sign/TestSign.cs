﻿using System;
using System.IO;
using NUnit.Framework;
using Twenty57.Linx.Plugins.Pdf.Design.Common;
using Twenty57.Linx.Plugins.Pdf.Shared.Sign;
using Twenty57.Linx.Plugins.Pdf.Shared.Sign.Constants;
using Twenty57.Linx.Plugins.Pdf.Sign;
using Twenty57.Linx.Plugins.Pdf.Tests.Common;
using Twenty57.Linx.Plugins.Pdf.Tests.Extensions;
using Twenty57.Linx.Plugins.Pdf.Tests.Helpers;
using Twenty57.Linx.Sdk.TestKit;

namespace Twenty57.Linx.Plugins.Pdf.Tests.Sign;

[TestFixture]
internal class TestSign : TestPdfBase
{
    private string outputFolderPath;

    private const string SignName = "John Smith";
    private const string SignReason = "Make it safe.";
    private const string SignLocation = "At the office.";

    [SetUp]
    public void SetUp()
    {
        this.outputFolderPath = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString("N"));
        Directory.CreateDirectory(this.outputFolderPath);
    }

    [TearDown]
    public void TearDown()
    {
        Directory.Delete(this.outputFolderPath, true);
    }

    [Test]
    [Combinatorial]
    public void SignWithNoVisibleSignature(
        [Values(
            FileAuthentication.None,
            FileAuthentication.Password,
            FileAuthentication.CertificateFile)] FileAuthentication inputFileAuthentication,
        [Values(
            "SignAcro.pdf",
            "SignXFA.pdf")] string fileName,
        [Values(false, true)] bool lockDocument)
    {
        string inputFilePath = ResourceHelpers.WriteResourceToFile($"Twenty57.Linx.Plugins.Pdf.Tests.Sign.Resources.{fileName}", this.inputFolderPath);
        string outputFilePath = Path.Combine(this.outputFolderPath, fileName);

        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, inputFileAuthentication, inputFilePath);
        propertyValues.AddRuntime(Shared.Common.Constants.PropertyIds.OutputFilePath, outputFilePath);
        ConfigureSignCertificateProperties(propertyValues, lockDocument);
        propertyValues.AddDesignTime(PropertyIds.SignaturePlacement, SignaturePlacement.Hidden);

        var tester = new FunctionTester<Provider>();
        tester.Execute(propertyValues);

        PdfComparer.AssertPageSignature(outputFilePath, inputFileAuthentication, this.authenticationManager, SignName, SignLocation, SignReason, lockDocument, 1, 0, 0, 0, 0);
    }

    [Test]
    public void SignAcroFormWithFieldSignature()
    {
        string inputFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Sign.Resources.SignAcro.pdf", this.inputFolderPath);
        string outputFilePath = Path.Combine(this.outputFolderPath, "SignAcro.pdf");
        string fieldName = "SignatureField";

        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, FileAuthentication.None, inputFilePath);
        propertyValues.AddRuntime(Shared.Common.Constants.PropertyIds.OutputFilePath, outputFilePath);
        ConfigureSignCertificateProperties(propertyValues);
        propertyValues.AddDesignTime(PropertyIds.SignaturePlacement, SignaturePlacement.FormField);
        propertyValues.AddRuntime(PropertyIds.FieldName, fieldName);
        propertyValues.AddRuntime(PropertyIds.BackgroundImage, ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Sign.Resources.Sign_Image.png", this.inputFolderPath));

        var tester = new FunctionTester<Provider>();
        tester.Execute(propertyValues);

        PdfComparer.AssertFieldSignature(outputFilePath, FileAuthentication.None, this.authenticationManager, fieldName, SignName, SignLocation, SignReason);
    }

    [Test]
    public void SignXfaFormWithFieldSignature()
    {
        string inputFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Sign.Resources.SignXFA.pdf", this.inputFolderPath);
        string outputFilePath = Path.Combine(this.outputFolderPath, "SignXFA.pdf");

        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, FileAuthentication.None, inputFilePath);
        propertyValues.AddRuntime(Shared.Common.Constants.PropertyIds.OutputFilePath, outputFilePath);
        ConfigureSignCertificateProperties(propertyValues);
        propertyValues.AddDesignTime(PropertyIds.SignaturePlacement, SignaturePlacement.FormField);
        propertyValues.AddRuntime(PropertyIds.FieldName, "SignatureField");
        propertyValues.AddRuntime(PropertyIds.BackgroundImage, null);

        var tester = new FunctionTester<Provider>();
        Assert.That(() => tester.Execute(propertyValues),
            Throws.Exception
            .With.Property(nameof(Exception.Message)).StartsWith("Field signatures are not supported for XFA documents.\r\nSee Code and Parameter properties for more information."));
    }

    [Test]
    public void SignWithFieldSignatureAndNonExistentField()
    {
        string inputFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Sign.Resources.SignAcro.pdf", this.inputFolderPath);
        string outputFilePath = Path.Combine(this.outputFolderPath, "SignAcro.pdf");
        string fieldName = "NonExistent";

        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, FileAuthentication.None, inputFilePath);
        propertyValues.AddRuntime(Shared.Common.Constants.PropertyIds.OutputFilePath, outputFilePath);
        ConfigureSignCertificateProperties(propertyValues);
        propertyValues.AddDesignTime(PropertyIds.SignaturePlacement, SignaturePlacement.FormField);
        propertyValues.AddRuntime(PropertyIds.FieldName, fieldName);
        propertyValues.AddRuntime(PropertyIds.BackgroundImage, null);

        var tester = new FunctionTester<Provider>();
        Assert.That(() => tester.Execute(propertyValues),
            Throws.Exception
            .With.Property(nameof(Exception.Message)).StartsWith("The form does not have a field named [NonExistent].\r\nSee Code and Parameter properties for more information."));
    }

    [Test]
    public void SignWithPageSignature([Values("SignAcro.pdf", "SignXFA.pdf")] string fileName)
    {
        string inputFilePath = ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Sign.Resources." + fileName, this.inputFolderPath);
        string outputFilePath = Path.Combine(this.outputFolderPath, fileName);
        int left = 45;
        int bottom = 50;
        int width = 109;
        int height = 79;
        int page = 1;

        var propertyValues = new FunctionPropertyValues();
        ConfigureInputFileFunctionValues(propertyValues, FileAuthentication.None, inputFilePath);
        propertyValues.AddRuntime(Shared.Common.Constants.PropertyIds.OutputFilePath, outputFilePath);
        ConfigureSignCertificateProperties(propertyValues);
        propertyValues.AddDesignTime(PropertyIds.SignaturePlacement, SignaturePlacement.OnPage);
        propertyValues.AddRuntime(PropertyIds.PositionX, left);
        propertyValues.AddRuntime(PropertyIds.PositionY, bottom);
        propertyValues.AddRuntime(PropertyIds.Width, width);
        propertyValues.AddRuntime(PropertyIds.Height, height);
        propertyValues.AddRuntime(PropertyIds.Page, page);
        propertyValues.AddRuntime(PropertyIds.BackgroundImage, ResourceHelpers.WriteResourceToFile("Twenty57.Linx.Plugins.Pdf.Tests.Sign.Resources.Sign_Image.png", this.inputFolderPath));

        var tester = new FunctionTester<Provider>();
        tester.Execute(propertyValues);

        PdfComparer.AssertPageSignature(outputFilePath, FileAuthentication.None, this.authenticationManager, SignName, SignLocation, SignReason, expectedLockDocument: false,
            page, left, bottom, width, height);
    }

    private void ConfigureInputFileFunctionValues(FunctionPropertyValues propertyValues, FileAuthentication inputAuth, string inputFilePath)
    {
        ConfigureInputFileFunctionValues(
            propertyValues,
            inputAuth,
            inputFilePath,
            Shared.Common.Constants.CategoryNames.Input);
    }

    private void ConfigureSignCertificateProperties(FunctionPropertyValues propertyValues, bool lockDocument = false)
    {
        var certificatePropertyGroup = new CertificatePropertyGroup(PropertyGroupIds.SigningCertificate, CategoryNames.SigningCertificate);
        propertyValues.AddRuntime(certificatePropertyGroup.FilePathPropertyId, this.authenticationManager.CertificateFilePath);
        propertyValues.AddRuntime(certificatePropertyGroup.FilePasswordPropertyId, this.authenticationManager.CertificateFilePassword);

        propertyValues.AddRuntime(PropertyIds.SignedAt, SignLocation);
        propertyValues.AddRuntime(PropertyIds.Reason, SignReason);
        propertyValues.AddDesignTime(PropertyIds.LockAfterSigning, lockDocument);
    }
}
