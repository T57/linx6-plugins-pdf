﻿using NUnit.Framework;
using Twenty57.Linx.Plugins.Pdf.Design.Common.Extensions;
using Twenty57.Linx.Sdk.TestKit;

namespace Twenty57.Linx.Plugins.Pdf.Tests;

[TestFixture]
internal class TestTypeReferenceExtensions
{
    [Test]
    public void GetAllPropertyNames()
    {
        var personTypeBuilder = TypeReference.CreateBuilder();
        personTypeBuilder.AddProperty("Name", typeof(string));
        personTypeBuilder.AddProperty("Car", TypeReference.CreateGenerated(
            TypeReference.CreateTypeProperty("Name", typeof(string)),
            TypeReference.CreateTypeProperty("Model", typeof(string))));
        personTypeBuilder.AddProperty("Children", personTypeBuilder, makeList: true);
        var personTypeReference = personTypeBuilder.CreateTypeReference();

        Assert.That(personTypeReference.GetAllPropertyNames(), Is.EquivalentTo(new[] { "Name", "Model" }));
    }
}
