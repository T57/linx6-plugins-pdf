﻿using System.Linq;
using Twenty57.Linx.Interfaces.Plugin.FunctionProvider;
using Twenty57.Linx.Plugins.Pdf.Tests.Common;
using Twenty57.Linx.Sdk.TestKit;

namespace Twenty57.Linx.Plugins.Pdf.Tests.Extensions;

internal static class FunctionTesterExtensions
{
    public static FunctionResult Execute<T>(this FunctionTester<T> functionTester, FunctionPropertyValues propertyValues)
        where T : IFunctionProvider
    {
        return functionTester.Execute(propertyValues.PropertyValues.ToArray(), propertyValues.RuntimePropertyValues.ToArray());
    }
}
