﻿using System;
using NUnit.Framework;
using Twenty57.Linx.Interfaces.Plugin.Types;
using Twenty57.Linx.Sdk.TestKit;

namespace Twenty57.Linx.Plugins.Pdf.Tests.Extensions;

internal static class TypePropertyExtensions
{
    public static void AssertCompiled(this ITypeProperty typeProperty, string expectedName, Type expectedType)
    {
        Assert.That(typeProperty.Name, Is.EqualTo(expectedName));
        Assert.That(typeProperty.TypeReference, Is.EqualTo(TypeReference.CreateCompiled(expectedType)));
    }

    public static void AssertGenerated(this ITypeProperty typeProperty, string expectedName)
    {
        Assert.That(typeProperty.Name, Is.EqualTo(expectedName));
        Assert.That(typeProperty.TypeReference.IsGenerated, Is.True);
    }

    public static void AssertList(this ITypeProperty typeProperty, string expectedName, Type expectedListType)
    {
        typeProperty.AssertList(expectedName, TypeReference.CreateCompiled(expectedListType));
    }

    public static void AssertList(this ITypeProperty typeProperty, string expectedName, ITypeReference expectedListTypeReference)
    {
        Assert.That(typeProperty.Name, Is.EqualTo(expectedName));
        Assert.That(typeProperty.TypeReference.IsList, Is.True);
        Assert.That(typeProperty.TypeReference.GetEnumerableContentType(), Is.EqualTo(expectedListTypeReference));
    }
}
